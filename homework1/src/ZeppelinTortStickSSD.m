function [sumRes, resJ] = ZeppelinTortStickSSD(x, Avox, bvals, qhat)
% Function for the Zeppelin with Tortuosity and Stick Model
%
% Input Params:
%  x = initial parameters
%  Avox = measurements from a single voxel from all the 33 image volumes
%  bvals = the b-value array
%  qhat = the gradient directions
% 
% Returns: 
%  sumRes = the sum of the square differences
%  S = the calculated signal
%

% Extract the parameters
S0 = x(1); % signal for b=0
diff = x(2); % initial diffusivity
f = x(3); % fraction of signal contributed by anisotropy
theta = x(4); % theta angle for polar coords
phi = x(5); % phi angle for polar coords
lambda1 = x(6);           % lambda1 > 0
lambda2 = (1-f)*lambda1;  

%% Synthesize the signals
% Fiber directions (cylinders have rho = 0, so we only get the first column
% of the rotation matrix)
fibDir = [cos(phi)*sin(theta) sin(phi)*sin(theta) cos(theta)];

% Gradient direction * fiber directions 
% Why sum?
fibDotGrad = sum(qhat .* repmat(fibDir, [length(qhat) 1])');


% The signal model: anisotropic part + isotropic part
% Why fibDotGrad.^2?
S = S0*( f  * exp(-bvals  * diff .* (fibDotGrad .^2)) + ...
     (1 - f)* exp(-bvals .* (lambda2 + (lambda1 - lambda2) * (fibDotGrad .^2))));

% Compute the sum of the square differences
sumRes = sum((Avox - S').^2);

end