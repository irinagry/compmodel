%function Q113()
% Question 1.1.3

%%% Load data
[dwis qhat bvals] = loadData();

% Initial Params
N = 100;

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
% WM  dwis(:,52,62,25);
% GM  dwis(:,66,26,25);
% CSF dwis(:,63,61,20);
Avox = dwis(:,63,61,20);
% figure, imshow(squeeze(dwis(1,:,:,25)), []);
% return
% TODO: ADD normally distributed random numbers to the initial values
% xini = 7.5e+05 + 2.5e+05.*randn(N,1); %7.5e+05;
% diffini = 3e-03 + 1e-03.*randn(N,1); %3e-03;
% fini = 2.5e-01 + 0.8e-01.*randn(N,1); %2.5e-01;
% thetaini = 0 + pi*randn(N,1); %0;
% phiini = 0 + pi*randn(N,1); %0;
 
xini = 1.1e+05 + 0.7e+05.*randn(N,1); %7.5e+05;
diffini = 3e-03 + 1.5e-03.*randn(N,1); %3e-03;
fini = 2.5e-01 + 1.25e-01.*randn(N,1); %2.5e-01;
thetaini = 0 + 2*pi*randn(N,1); %0;
phiini = 0 + 2*pi*randn(N,1); %0;

% xini = 7.5e+05*rand(N,1);
% diffini = 3e-03*rand(N,1);
% fini = 0.99*rand(N,1);
% thetaini = 2*pi*rand(N,1);
% phiini = 2*pi*rand(N,1);

% Define multiple starting points
initialPoints = [xini diffini fini thetaini phiini];
%return
finalPoints = zeros(1,5);

RESNORMMIN = 1e+30; % high value
RESNORMCounter = zeros(N,1);

tic
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'levenberg-marquardt',...
        'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');%, 'Display', 'iter');

    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        %startx = [sqrt(7.5e+05) sqrt(3e-03) (asin(sqrt(2.5e-01))) 0 0];
        startx = [sqrt(abs(initialPoints(i,1))) sqrt(abs(initialPoints(i,2))) ...
            invsin2(abs(initialPoints(i,3))) initialPoints(i,4) initialPoints(i,5)];
        
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fminunc('BallStickSSD2', startx, h, Avox, bvals, qhat);
        
        RESNORMCounter(i) = RESNORM;
    
    catch ME
        warning(ME.message);
        disp(ME.message);
        
        RESNORM = 1e+30; % high value
        RESNORMCounter(i) = RESNORM;
    end

    %parameter_hat2
    if RESNORM < RESNORMMIN
        % In parameter_hat I have the parameters at which my 
        % sum of square differences is minimum
        parameter_hat2 = [parameter_hat(1)^2 ...
            parameter_hat(2)^2 ...
            sin2(parameter_hat(3)) ...
            parameter_hat(4) ...
            parameter_hat(5)];
        RESNORMMIN = RESNORM;
        finalPoints = parameter_hat2;
    end
%     fprintf('Run %d', i);
end
toc
% What I got
disp('Q113\n');
RESNORMMIN
finalPoints


trials = numel(find(floor(RESNORMCounter) == floor(min(RESNORMCounter))));

% % Compare predictions with actual measurements:
% [predictedS] = BallStick(finalPoints, bvals, qhat);
% figure
% plot(Avox,'bs'); % measurements
% hold on
% plot(predictedS,'rx'); % predictions
% legend('Data','Model')
% title('Measured vs Predicted Values')
% xlabel('index') % x-axis label
% ylabel('S') % y-axis label
% saveas(gcf,'Q113.png');

%end
