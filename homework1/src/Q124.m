%function Q124()
% Question 1.2.4


% number of runs for the MCMC algorithm
T = 2000; 
Ith = 10;
Burn = T/10;

XMAX = 112;
YMAX = 112;

fibdirX = zeros(XMAX, YMAX, T);
fibdirY = zeros(XMAX, YMAX, T);
fibdirF = zeros(XMAX, YMAX, T);

%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
%Avox = dwis(:,62,57,25);
%imshow(squeeze(dwis(1,:,:,25)), []);
%return

savefile = 'Q114';
load(savefile, 'mS0', 'md', 'mf', 'mtheta', 'mphi', 'mResnorm', 'elapsedTime');
md(md > 3e-03) = 3e-03;

for xx = 1:XMAX
    for yy = 1:YMAX

        % Get parameters:
        parameter_hat = [mS0(xx,yy) md(xx,yy) mf(xx,yy) mtheta(xx,yy) mphi(xx,yy)];
        %parameter_hat

        %%%%% MCMC
        % Get your starting point scaled
        % parameter_hat
        x0 = scaleTransformParams(parameter_hat);
        %x0 = [7.5e+05 3e-03 2.5e-01 0 0];

        %% Gaussians for the scaled parameters
        sigma1 = abs(x0(1)/400); %0.5; %1;%8e-01;%0.07;
        sigma2 = abs(x0(2)/400); %5; %0.5; %1;%8e-01;
        sigma3 = abs(x0(3)/400); %5; %0.5; %1;%8e-01;
        sigma4 = abs(x0(4)/400); %1; %0.5;%pi/10; %1e+01;
        sigma5 = abs(x0(5)/400); %1; %0.5;%pi/10; %1e+01;
        sigma = [sigma1 sigma2 sigma3 sigma4 sigma5];


        % Vector of x values
        x = zeros(T,5);
        x(1,:) = x0(:);

        % % Standard Deviation for Gaussian
        % stdG = 6000; % do it with residuals thingy
        % % Compute Gaussian
        % gaussian = @(x,mux) (1/(stdG*sqrt(2*pi)))*exp(-1/2 * (x - mux)^2 * (1/(stdG^2)));
        % % Alpha function
        % alphaFunc = @(x,y) x/y;


        %% Do everything on the transformed parameters
        acceptanceRate = 0.0;
        alpha = zeros(T+Burn-1,1);
  
        tic
        for i = 2:(T+Burn)
            
            tmp = x(i-1,:) + sigma.*randn(1,5);

            for j = 1:Ith
               tmp = tmp + sigma.*randn(1,5);
            end

            xc = tmp;
            xp = x(i-1,:);

            [sumResc] = BallStickSSD22(xc, dwis(:,xx,yy,25), bvals, qhat);
            [sumResp] = BallStickSSD22(xp, dwis(:,xx,yy,25), bvals, qhat);

            alpha(i,1) = exp((sumResp - sumResc) / (2*(6000^2))); %sumResc/sumResp;

            if alpha(i,1) > rand
                x(i,:) = xc(:);
                acceptanceRate = acceptanceRate + 1;
            else
                x(i,:) = xp(:);
            end   
        end

        finalX = zeros(T,5);
        for i = 1:T
            finalX(i,:) = scaleTransformBackParams(x(i + Burn,:));
            theta = finalX(i,4);
            phi = finalX(i,5);
            fibdirF(xx,yy,i) = finalX(i,3);
            
            [fibdirX(xx,yy,i),fibdirY(xx,yy,i),Z] = sph2cart(theta, phi, 1);
%             % Maybe:
%             X = sin(theta)*cos(phi);
%             Y = sin(theta)*sin(phi);
%             quiver(X.*f+xx,Y.*f+yy)
%             drawnow
%             hold on
        end
        toc

        fprintf('Acceptance Rate: %f\n', (acceptanceRate / (T+Burn)));
        fprintf('Position [ %d , %d ]\n\n', xx, yy); 
        
    end
end


% savefile = 'Q124';
% save(savefile, 'fibdirX', 'fibdirY', 'fibdirF', 'T');

% savefile = 'Q124';
% load(savefile, 'fibdirX', 'fibdirY', 'fibdirF', 'T');
% 
% %%%% Plot fibdir
% figure
% for i = 1:5:T
%     quiver(fibdirX(:,:,i).*fibdirF(:,:,i), fibdirY(:,:,i).*fibdirF(:,:,i), '.', 'filled');
% %     drawnow
%     hold on
%     i
% end
% title('Uncertainty in the estimate of fiber orientation on a brain slice')
% drawnow
% saveas(gcf,'Q124fibdiruncert.png');
% 
%%%% Plot zoomed in
% figure
% for i = 1:10:T
%     quiver(fibdirX(40:80,40:80,i).*fibdirF(40:80,40:80,i), fibdirY(40:80,40:80,i).*fibdirF(40:80,40:80,i), '.', 'filled');
% %     drawnow
%     hold on
%     i
% end
% title('Uncertainty in the estimate of fiber orientation on a brain slice (zoomed in)')
% drawnow
% saveas(gcf,'Q124fibdiruncertZoom.png');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% 95
% 
% %%%%%%%%%%%%%%%%%
% %%%%%%%%% 95% 
% lb = 0.025*T;
% ub = T - lb;
% T95 = 0.95*T;
% 
% fibdirXlu = zeros(XMAX, YMAX, T95);
% fibdirYlu = zeros(XMAX, YMAX, T95);
% fibdirFlu = zeros(XMAX, YMAX, T95);
% 
% for i = 1:XMAX
%     for j = 1:YMAX
%         sortedFibdirX = sort(reshape(fibdirX(i,j,:),[T,1]));
%         sortedFibdirY = sort(reshape(fibdirY(i,j,:),[T,1]));
%         sortedFibdirF = sort(reshape(fibdirY(i,j,:),[T,1]));
%         
%         fibdirXlu(i,j,1) = sortedFibdirX(lb,1); fibdirXlu(i,j,2) = sortedFibdirX(ub,1);
%         fibdirYlu(i,j,1) = sortedFibdirY(lb,1); fibdirYlu(i,j,2) = sortedFibdirY(ub,1);
%         fibdirFlu(i,j,1) = sortedFibdirF(lb,1); fibdirFlu(i,j,2) = sortedFibdirF(ub,1);
%     end
% end
% 
% % discard 5% of values
% for i = 1:XMAX
%     for j = 1:YMAX
%         fibdirX(fibdirX(i,j,:) >= fibdirXlu(i,j,1)) = 0;
%         fibdirX(fibdirX(i,j,:) <  fibdirXlu(i,j,2)) = 0;
%         
%         fibdirY(fibdirY(i,j,:) >= fibdirYlu(i,j,1)) = 0;
%         fibdirY(fibdirY(i,j,:) <  fibdirYlu(i,j,2)) = 0;
%         
%         fibdirF(fibdirF(i,j,:) >= fibdirFlu(i,j,1)) = 0;
%         fibdirF(fibdirF(i,j,:) <  fibdirFlu(i,j,2)) = 0;
%     end
% end
% 
% %%%% Plot fibdir
% figure
% for i = 1:10:T
%     quiver(fibdirX(:,:,i).*fibdirF(:,:,i), fibdirY(:,:,i).*fibdirF(:,:,i), '.', 'filled');
% %     drawnow
%     hold on
%     i
% end
% title('Uncertainty in the estimate of fiber orientation on a brain slice on the 95% range')
% drawnow
% saveas(gcf,'Q124fibdiruncert95.png');
% 
% %%%% Plot fibdir
% figure
% for i = 1:10:T
%     quiver(fibdirX(40:80,40:80,i).*fibdirF(40:80,40:80,i), fibdirY(40:80,40:80,i).*fibdirF(40:80,40:80,i), '.', 'filled');
% %     drawnow
%     hold on
%     i
% end
% title('Uncertainty in the estimate of fiber orientation on a brain slice on the 95% range (zoomed in)')
% drawnow
% saveas(gcf,'Q124fibdiruncertZoom95.png');







%%%%%%%%%%%%%%%%%% Calculating good starting position
%         % Initial positions
%         iniN = 5;
%         xini = 1.5e+05 + 0.5e+05.*randn(iniN,1); 
%         diffini = 1e-03 + 0.25e-03.*randn(iniN,1); 
%         fini = 2.5e-01 + 1.2e-01.*randn(iniN,1); 
%         thetaini = 0 + pi*randn(iniN,1); 
%         phiini = 0 + pi*randn(iniN,1); 
% 
%         % Define a starting point for the non-linear fit
%         startx = [xini diffini fini thetaini phiini];
% 
%         lb = [0      ,0,0,-Inf,-Inf];
%         ub = [1.0e+10,3,1, Inf, Inf];
%         A = [];
%         b = [];
%         Aeq = [];
%         beq = [];
% 
%         % Define various options for the non-linear fitting algorithm
%         h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
%             'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');
%         evalFunc = @(x) BallStickSSD(x, dwis(:,xx,yy,25), bvals, qhat);
% 
%         RESNORMMIN = 1e+30; % high value
% 
%         parameter_hat = zeros(1,5); % Retain the best results
% 
%         for i = 1:iniN
% 
%             try
%                 [parameter_hat_local, RESNORM, EXITFLAG, OUTPUT] = ...
%                     fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);
%             catch ME
%                 disp(ME.message);
%                 RESNORM = 1e+30; % high value
%             end
% 
%             if RESNORM < RESNORMMIN
% 
%                 % In parameter_hat I have the parameters at which my 
%                 % sum of square differences is minimum
% 
%                 RESNORMMIN = RESNORM;
%                 parameter_hat = parameter_hat_local;
% 
%             end
% 
%         end




%%%%%%%%%%%
% figure 
% 
% subplot(2,5,1)
% plot(finalX(:,1)), title('S0')
% subplot(2,5,6)
% plot(cumsum(finalX(:,1))'./(1:length(finalX(:,1))))
% 
% subplot(2,5,2)
% plot(finalX(:,2)), title('d')
% subplot(2,5,7)
% plot(cumsum(finalX(:,2))'./(1:length(finalX(:,2))))
% 
% subplot(2,5,3)
% plot(finalX(:,3)), title('f')
% subplot(2,5,8)
% plot(cumsum(finalX(:,3))'./(1:length(finalX(:,3))))
% 
% subplot(2,5,4)
% plot(finalX(:,4)), title('theta')
% subplot(2,5,9)
% plot(cumsum(finalX(:,4))'./(1:length(finalX(:,4))))
% 
% subplot(2,5,5)
% plot(finalX(:,5)), title('phi')
% subplot(2,5,10)
% plot(cumsum(finalX(:,5))'./(1:length(finalX(:,5))))
% 
% % 
% 
% 
% figure 
% 
% subplot(1,5,1)
% histfit(finalX(:,1), T/10), title('S0')
% 
% subplot(1,5,2)
% histfit(finalX(:,2), T/10), title('d')
% 
% subplot(1,5,3)
% histfit(finalX(:,3), T/10), title('f')
% 
% subplot(1,5,4)
% histfit(finalX(:,4), T/10), title('theta')
% 
% subplot(1,5,5)
% histfit(finalX(:,5), T/10), title('phi')
% 


%end