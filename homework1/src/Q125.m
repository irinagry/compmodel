%function Q125()
% Question 1.2.5

% Trial 1 Tractography
% Load data from one slice (ex 4)

XMAX = 112;
YMAX = 112;

savefile = 'Q114';
load(savefile, 'mS0', 'md', 'mf', 'mtheta', 'mphi', 'mResnorm', 'elapsedTime');
md(md > 3e-03) = 3e-03;

savefile = 'Q124';
load(savefile, 'fibdirX', 'fibdirY', 'fibdirF', 'T');

eps = 0;%.00001;

[X,Y,Z] = sph2cart(mtheta, mphi, 1);
% X = sin(mtheta).*cos(mphi);
% Y = sin(mtheta).*sin(mphi);
% figure, quiver((X.*mf),(Y.*mf),'color',[0,0,1]), title('Fibre Direction Map');
% return
% U = X.*mf;
% V = Y.*mf;
% %(70,48)
% X = 0.1884
% Y = -0.0975
% 
% alpha = atan(Y/X)


% end

% Same sign:      \
% Different sign: /
% 
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tractography Constrained UP-DOWN

% Tractography map
tractX = zeros(XMAX, YMAX, T);
tractY = zeros(XMAX, YMAX, T);
pointsUD = zeros(T, 112, 2);
maxPoints = zeros(T,1);
% Seeding voxel
% xx = 24; yy = 47;
xx = 47; yy = 68;

% Tractography Constrained UP-DOWN 
for i = 1:T
    ok = 1; k = 1;
        
    xc = [xx yy];
    tractX(xx,yy,i) = X(xx,yy);
    tractY(xx,yy,i) = Y(xx,yy);

    % Get angles in degrees from the theta matrix
    mAngles = zeros(XMAX,YMAX);
    for ii = 1:XMAX
        for jj = 1:YMAX
            mAngles(ii,jj) = mod(atan(fibdirY(ii,jj,i)/fibdirX(ii,jj,i)),(2*pi)) * (180/pi);
        end
    end

    while(1)
       if ok == 0
           break; 
       end

       % Get everyone in quadrans 1 and 2
       if mAngles(xc(1), xc(2)) > 180
           angle = mAngles(xc(1), xc(2)) - 180;
       else
           angle = mAngles(xc(1), xc(2));
       end

       % Decision: 
       % if angle < 50        => x+1,y+1
       % if  50 < angle < 140 => x  ,y+1
       % if 140 < angle < 180 => x-1,y+1
       
       if (angle >= 20) && (angle < 70)
           tractX(xc(1), xc(2), i) = fibdirX(xc(1)+1,xc(2)+1,i);
           tractY(xc(1), xc(2), i) = fibdirY(xc(1)+1,xc(2)+1,i);
           xc(1) = xc(1) + 1;
           xc(2) = xc(2) + 1;
           pointsUD(i, k, 1) = xc(1);
           pointsUD(i, k, 2) = xc(2);
           if mf(xc(1),xc(2)) < eps
               break;
           end
       elseif (70 <= angle) && (angle < 110)
           tractX(xc(1), xc(2), i) = fibdirX(xc(1)+1,xc(2),i);
           tractY(xc(1), xc(2), i) = fibdirY(xc(1)+1,xc(2),i);
           xc(1) = xc(1) + 1;
           pointsUD(i, k, 1) = xc(1);
           pointsUD(i, k, 2) = xc(2);
           if mf(xc(1),xc(2)) < eps
               break;
           end
       elseif (angle >= 110) && (angle <= 160)
           tractX(xc(1), xc(2), i) = fibdirX(xc(1)+1,xc(2)-1,i);
           tractY(xc(1), xc(2), i) = fibdirY(xc(1)+1,xc(2)-1,i);
           xc(1) = xc(1) + 1;
           xc(2) = xc(2) - 1;
           pointsUD(i, k, 1) = xc(1);
           pointsUD(i, k, 2) = xc(2);
           if mf(xc(1),xc(2)) < eps
               break;
           end
       else
           break;
       end
       k = k + 1;
       maxPoints(i) = k;
 
       % Break when hitting brain
       if mS0(xc(1),xc(2)) < 50000
           ok = 0;
       end


    end
    
end
    
%%%% Plot zoomed in
figure
imagesc(mS0), colormap bone
hold on
quiver((X.*mf),(Y.*mf),'.','filled','color',[0.1,0,0.1]);
hold on
% for i = 1:T
%     plot(pointsUD(i,1:maxPoints(i)-1,2), pointsUD(i,1:maxPoints(i)-1,1));
%     hold on
%     i
% end

for i = 1:10:T
%     quiver(tractX(40:80,40:80,i).*fibdirF(40:80,40:80,i), tractY(40:80,40:80,i).*fibdirF(40:80,40:80,i), '.', 'filled');
    quiver(tractX(:,:,i).*fibdirF(:,:,i), tractY(:,:,i).*fibdirF(:,:,i), '.', 'filled','color',[1,1,0]);
%     drawnow
    hold on
    i
end
title('Tractography constrained to go in one direction (up-down)')
drawnow    
saveas(gcf,'Q125tractupdownquiv.png');   
%   


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tractography Constrained LEFT-RIGHT 
% Tractography map
tractX = zeros(XMAX, YMAX, T);
tractY = zeros(XMAX, YMAX, T);
pointsLR = zeros(T, 112, 2);
maxPoints = zeros(T,1);
% Seeding voxel
% xx = 24; yy = 47;
% xx = 48; yy = 45;
% xx = 43; yy = 38; 
% xx = 42; yy = 29;
xx = 67; yy = 43;
% Tractography Constrained LEFT-RIGHT 
for i = 1:T
    ok = 1; k = 1;
        
    xc = [xx yy];
    tractX(xx,yy,i) = X(xx,yy);
    tractY(xx,yy,i) = Y(xx,yy);

    % Get angles in degrees from the theta matrix
    mAngles = zeros(XMAX,YMAX);
    for ii = 1:XMAX
        for jj = 1:YMAX
            mAngles(ii,jj) = mod(atan(fibdirY(ii,jj,i)/fibdirX(ii,jj,i)),(2*pi)) * (180/pi);
        end
    end

    while(1)
       if ok == 0
           break; 
       end

       % Get everyone in quadran 1 and 4
%        if mAngles(xc(1), xc(2)) > 90 && mAngles(xc(1), xc(2)) <= 180
%            angle = mAngles(xc(1), xc(2)) + 180;
%        elseif mAngles(xc(1), xc(2)) > 180 && mAngles(xc(1), xc(2)) < 270
%            angle = mAngles(xc(1), xc(2)) - 180;
%        else
%            angle = mAngles(xc(1), xc(2));
%        end

       % Get everyone in quadrans 1 and 2
       if mAngles(xc(1), xc(2)) > 180
           angle = mAngles(xc(1), xc(2)) - 180;
       else
           angle = mAngles(xc(1), xc(2));
       end 

       % Decision: 
       % if angle < 50        => x+1,y-1
       % if  50 < angle < 140 => x+1,y
       % if 140 < angle < 180 => x+1,y+1

       if (angle >= 20) && (angle <= 90)
           tractX(xc(1), xc(2), i) = fibdirX(xc(1)+1,xc(2)+1,i);
           tractY(xc(1), xc(2), i) = fibdirY(xc(1)+1,xc(2)+1,i);
           xc(1) = xc(1) + 1;
           xc(2) = xc(2) + 1;
           pointsLR(i, k, 1) = xc(1);
           pointsLR(i, k, 2) = xc(2);
           if mf(xc(1),xc(2)) < eps
               break;
           end
       elseif (70 <= angle) && (angle < 110)
           tractX(xc(1), xc(2), i) = fibdirX(xc(1),xc(2)+1,i);
           tractY(xc(1), xc(2), i) = fibdirY(xc(1),xc(2)+1,i);
           xc(2) = xc(2) + 1;
           pointsLR(i, k, 1) = xc(1);
           pointsLR(i, k, 2) = xc(2);
           if mf(xc(1),xc(2)) < eps
               break;
           end
       elseif (angle >= 110) && (angle <= 180)
           tractX(xc(1), xc(2), i) = fibdirX(xc(1)-1,xc(2)+1,i);
           tractY(xc(1), xc(2), i) = fibdirY(xc(1)-1,xc(2)+1,i);
           xc(1) = xc(1) - 1;
           xc(2) = xc(2) + 1;
           pointsLR(i, k, 1) = xc(1);
           pointsLR(i, k, 2) = xc(2);
           if mf(xc(1),xc(2)) < eps
               break;
           end
       else
           break;
       end
       
       k = k + 1;
       maxPoints(i) = k;
       
       % Break when hitting brain
       if mS0(xc(1),xc(2)) < 50000
           ok = 0;
       end

        
    end
    
end
    
figure
imagesc(mS0), colormap bone
hold on
quiver((X.*mf),(Y.*mf),'.','filled','color',[.1,0,.1]);
hold on
% for i = 1:T
%     plot(pointsLR(i,1:maxPoints(i)-1,2), pointsLR(i,1:maxPoints(i)-1,1));
%     hold on
%     i
% end
for i = 1:10:T
%     quiver(tractX(40:80,40:80,i).*fibdirF(40:80,40:80,i), tractY(40:80,40:80,i).*fibdirF(40:80,40:80,i), '.', 'filled');
%     quiver(tractX(:,:,i).*fibdirF(:,:,i), tractY(:,:,i).*fibdirF(:,:,i), '.', 'filled','color',[1,1,0]);
    quiver(tractX(:,:,i).*fibdirF(:,:,i), tractY(:,:,i).*fibdirF(:,:,i), '.', 'filled','color',[1,1,0]);
%     drawnow
    hold on
    i
end
title('Tractography constrained to go in one direction (left-right)')
drawnow  
saveas(gcf,'Q125tractleftrightquiv.png');  














% if (angle >= 0) && (angle <= 36)
%    tractX(xc(1), xc(2), i) = fibdirX(xc(1),xc(2)+1,i);
%    tractY(xc(1), xc(2), i) = fibdirY(xc(1),xc(2)+1,i);
%    xc(1) = xc(1);
%    xc(2) = xc(2) + 1;
% elseif (angle > 36) && (angle <= 72)
%    tractX(xc(1), xc(2), i) = fibdirX(xc(1)+1,xc(2)+1,i);
%    tractY(xc(1), xc(2), i) = fibdirY(xc(1)+1,xc(2)+1,i);
%    xc(1) = xc(1) + 1;
%    xc(2) = xc(2) + 1;
% elseif (angle > 72) && (angle <= 108)
%    tractX(xc(1), xc(2), i) = fibdirX(xc(1)+1,xc(2),i);
%    tractY(xc(1), xc(2), i) = fibdirY(xc(1)+1,xc(2),i);
%    xc(1) = xc(1) + 1;
%    xc(2) = xc(2);
% elseif (angle > 108) && (angle <= 146)
%    tractX(xc(1), xc(2), i) = fibdirX(xc(1)-1,xc(2)+1,i);
%    tractY(xc(1), xc(2), i) = fibdirY(xc(1)-1,xc(2)+1,i);
%    xc(1) = xc(1) - 1;
%    xc(2) = xc(2) + 1;
% elseif (angle > 144) && (angle <= 180)
%    tractX(xc(1), xc(2), i) = fibdirX(xc(1)-1,xc(2)-1,i);
%    tractY(xc(1), xc(2), i) = fibdirY(xc(1)-1,xc(2)-1,i);
%    xc(1) = xc(1) - 1;
%    xc(2) = xc(2) - 1;
% end

% Decision: 
% if angle < 40        => x+1,y
% if 40 < angle < 50   => x+1,y+1
% if 50 < angle < 130  => x  ,y+1
% if 130 < angle < 140 => x-1,y+1
% if 140 < angle < 180 => x-1,y    
% if angle < 40
%    tractX(xc(1), xc(2)) = X(xc(1)+1,xc(2));
%    tractY(xc(1), xc(2)) = Y(xc(1)+1,xc(2));
%    xc(1) = xc(1) + 1;
% elseif (40 < angle) && (angle < 50)
%    tractX(xc(1), xc(2)) = X(xc(1)+1,xc(2)+1);
%    tractY(xc(1), xc(2)) = Y(xc(1)+1,xc(2)+1);
%    xc(1) = xc(1) + 1;
%    xc(2) = xc(2) + 1;
% elseif (50 < angle) && (angle < 130)
%    tractX(xc(1), xc(2)) = X(xc(1),xc(2)+1);
%    tractY(xc(1), xc(2)) = Y(xc(1),xc(2)+1);
%    xc(2) = xc(2) + 1;
% elseif (130 < angle) && (angle < 140)
%    tractX(xc(1), xc(2)) = X(xc(1)-1,xc(2)+1);
%    tractY(xc(1), xc(2)) = Y(xc(1)-1,xc(2)+1);
%    xc(1) = xc(1) - 1;
%    xc(2) = xc(2) + 1;
% else
%    tractX(xc(1), xc(2)) = X(xc(1)-1,xc(2));
%    tractY(xc(1), xc(2)) = Y(xc(1)-1,xc(2));
%    xc(1) = xc(1) - 1;
% end    
    
    
    