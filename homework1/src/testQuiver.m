function testQuiver()
    N = 100;
    [x,y] = meshgrid(-2:.2:2,-1:.15:1);
    %angles = zeros(N, N);
    theta = pi * rand(N,N);
    phi   = pi * rand(N,N);
    
    
    [X,Y,Z] = sph2cart(theta, phi, 1);
    %size(X)
    
    quiver(X,Y)
end