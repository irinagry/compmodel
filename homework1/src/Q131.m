%function Q131()
% Question 1.3.1

datafile = 'data/challengeOPEN.txt';
fid = fopen(datafile, 'r', 'b');

% Read in the header
A = fgetl(fid);

% Read in the data
A = fscanf(fid, '%f', [8, inf]);
fclose(fid);

% Create the protocol
Avox = A(1,:)';
qhat = A(2:4,:);
G = A(5,:);
delta = A(6,:);
smalldel = A(7,:);
TE = A(8,:);
GAMMA = 2.675987E8;
bvals = ((GAMMA*smalldel.*G).^2).*(delta-smalldel/3);
bvals = bvals.*1e-06;

% Apply DTI and get initial params
% [startx] = DT_SSD(Avox, bvals, qhat)
% return
% xini = 1.1321e+05;
% diffini = 0.001534;
% fini = 0.5754;
% thetaini = 76.4332;
% phiini = 47.0108;


% Number of Runs
N = 1000;

xini = 0 + 1.*randn(N,1);
diffini = 1.5e-3 + 1.5e-3.*randn(N,1);
fini = 0 + 1.*randn(N,1);
thetaini = pi + 2*pi*randn(N,1);
phiini = pi + 2*pi*randn(N,1); 

% for i = 1:N
%     while xini(i) < 0
%         xini(i) = 0 + 1.*randn(1,1);
%     end
%     
%     while diffini(i) < 0
%        diffini(i) = 1.5e-3 + 1.5e-3.*randn(1,1);
%     end
%     
%     while fini(i) < 0
%         fini(i) = 0 + 1.*randn(1,1);
%     end
% end

% Initial Points
startx = [xini diffini fini thetaini phiini];
lb = [0,0    ,0,-Inf,-Inf];
ub = [3,1e-01,1, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% BALL & STICK %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

RESNORMMIN = 1e+30; % high value
RESNORMCounter = zeros(N,1);
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    % Run the fitting
    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        
        evalFunc = @(x) BallStickSSD(x, Avox, bvals, qhat);
%         evalFunc(startx);
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

        % Remember the RESNORM PARAMETER
        RESNORMCounter(i) = RESNORM;
        
    catch ME
        warning(ME.message);
        disp(ME.message);

        RESNORM = 1e+30; % high value
        RESNORMCounter(i) = RESNORM;
        
    end

    if RESNORM < RESNORMMIN
        RESNORMMIN = RESNORM;
        finalPoints = parameter_hat;
    end
    
    i
    
end

% savefile = 'Q131';
% save(savefile, 'finalPoints');

disp('Q131\n');
RESNORMMIN
finalPoints
% Compare predictions with actual measurements:
[predictedS] = BallStick(finalPoints, bvals, qhat);
figure
title('Measured vs Predicted Values')
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
xlabel('index') % x-axis label
ylabel('S') % y-axis label
legend('Data','Model');
saveas(gcf,'Q131.png');




%end