%function Q134()
% Question 1.3.4


datafile = 'data/challengeOPEN.txt';
fid = fopen(datafile, 'r', 'b');

% Read in the header
A = fgetl(fid);

% Read in the data
A = fscanf(fid, '%f', [8, inf]);
fclose(fid);

% Create the protocol
Avox = A(1,:)';
qhat = A(2:4,:);
G = A(5,:);
delta = A(6,:);
smalldel = A(7,:);
TE = A(8,:);
GAMMA = 2.675987E8;
bvals = ((GAMMA*smalldel.*G).^2).*(delta-smalldel/3);
bvals = bvals.*1e-06;

sigmaData = 0.04;
K = length(Avox);

%%%%%%%
% Model 1: Ball and Stick
%%%%%%%

%%%%%%%
% Do MCMC:

% number of runs for the MCMC algorithm
T = 5000; 
Ith = 10;
Burn = T/10;


% Vector of x values
% get x0 from previous run
x0 = scaleTransformParams([0.979023407367306   0.001325322033862   0.481219959834656   7.828863048865617   9.448386399599237]);
x = zeros(T,5);
x(1,:) = x0(:);

%% Gaussians for the scaled parameters
sigma1 = abs(x0(1)/10); %0.5; %1;%8e-01;%0.07;   %S0
sigma2 = abs(x0(2)/10); %5; %0.5; %1;%8e-01;     %f
sigma3 = abs(x0(3)/10); %5; %0.5; %1;%8e-01;     %d
sigma4 = abs(x0(4)/10); %1; %0.5;%pi/10; %1e+01; %theta
sigma5 = abs(x0(5)/10); %1; %0.5;%pi/10; %1e+01; %phi
sigma = [sigma1 sigma2 sigma3 sigma4 sigma5];

%% Do everything on the transformed parameters
acceptanceRate = 0.0;
alpha = zeros(T+Burn-1,1);

for i = 2:(T+Burn)
%     i
    tmp = x(i-1,:) + sigma.*randn(1,5);
%     x0
%     tmp
    
    for j = 1:Ith
       tmp = tmp + sigma.*randn(1,5);
    end
    
    xc = tmp;
    xp = x(i-1,:);
    
%     x(i-1,:)
%     sigma.*randn(1,5)
%     xc
    
%     while (xc(1) < 0) || (xc(2) < 0) || (xc(2) > 3) || (xc(3) < 0) || (xc(3) > 1)
%         xc = x(i-1,:) + sigma.*randn(1,5);
%     end
    
    [sumResc] = BallStickSSD22(xc, Avox, bvals, qhat);
    [sumResp] = BallStickSSD22(xp, Avox, bvals, qhat);
    
    % Computing ALPHA
    alpha(i,1) = exp((sumResp - sumResc) / (2*(6000^2))); %sumResc/sumResp;
    
    if alpha(i,1) > rand
        x(i,:) = xc(:);
        %xc
        acceptanceRate = acceptanceRate + 1;
    else
        x(i,:) = xp(:);
    end
    i
end

finalX = zeros(T,5);
for i = 1:T
    finalX(i,:) = scaleTransformBackParams(x(i + Burn,:));
end

acceptanceRate / (T+Burn)


figure 

subplot(2,5,1)
plot(finalX(:,1)), title('S0')
subplot(2,5,6)
plot(cumsum(finalX(:,1))'./(1:length(finalX(:,1))))

subplot(2,5,2)
plot(finalX(:,2)), title('d')
subplot(2,5,7)
plot(cumsum(finalX(:,2))'./(1:length(finalX(:,2))))

subplot(2,5,3)
plot(finalX(:,3)), title('f')
subplot(2,5,8)
plot(cumsum(finalX(:,3))'./(1:length(finalX(:,3))))

subplot(2,5,4)
plot(finalX(:,4)), title('theta')
subplot(2,5,9)
plot(cumsum(finalX(:,4))'./(1:length(finalX(:,4))))

subplot(2,5,5)
plot(finalX(:,5)), title('phi')
subplot(2,5,10)
plot(cumsum(finalX(:,5))'./(1:length(finalX(:,5))))



figure 

subplot(1,5,1)
histfit(finalX(:,1), T/10), title('S0')

subplot(1,5,2)
histfit(finalX(:,2), T/10), title('d')

subplot(1,5,3)
histfit(finalX(:,3), T/10), title('f')

subplot(1,5,4)
histfit(finalX(:,4), T/10), title('theta')

subplot(1,5,5)
histfit(finalX(:,5), T/10), title('phi')











% end