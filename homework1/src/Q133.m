%function Q133()
% Question 1.3.3 
% this exercise can be found in functions:
% computeAIC() and
% computeBIC()

% Values from previous exercise
SSDBallStick = 3.61384559268164;
SSDZeppelinStick = 48.1914486137647;
SSDZeppelinTortStick = 15.7913182653776;

% Params
NBallStick = 5;
NZeppelinStick = 7;
NZeppelinTortStick = 6;
K = 1152; % number of data points
sigma = 0.04;

%%%% AIC %%%%% 2 N - 2 log L
AICBallStick = 2*NBallStick + (1/(sigma^2)) * SSDBallStick;
AICZeppelinStick = 2*NZeppelinStick + (1/(sigma^2)) * SSDZeppelinStick;
AICZeppelinTortStick = 2*NZeppelinTortStick + (1/(sigma^2)) * SSDZeppelinTortStick;

%%%% BIC %%%%% N log K - 2 log L
BICBallStick = NBallStick * log(K) + (1/(sigma^2)) * SSDBallStick;
BICZeppelinStick = NZeppelinStick * log(K) + (1/(sigma^2)) * SSDZeppelinStick;
BICZeppelinTortStick = NZeppelinTortStick * log(K) + (1/(sigma^2)) * SSDZeppelinTortStick;

% end