% function Q136()
% Question 1.3.6


%%% Load data
[dwis qhat bvals] = loadData();

% Initial Params
N = 5;
XMAX = 112;
YMAX = 112;

S = zeros(33,112,112);


%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ZEPPELIN & STICK %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0ZS = zeros(XMAX,YMAX);
mdZS = zeros(XMAX,YMAX);
mfZS = zeros(XMAX,YMAX);
mthetaZS = zeros(XMAX,YMAX);
mphiZS = zeros(XMAX,YMAX);
mlambda1ZS = zeros(XMAX,YMAX);
mlambda2ZS = zeros(XMAX,YMAX);
mResnormZS = zeros(XMAX,YMAX);

RESNORMCounterZS = zeros(N,XMAX,YMAX);

% Constrains
lb = [ 0    ,0    ,0,-Inf,-Inf,0,0];
ub = [10e+05,1e-02,1, Inf, Inf,1,1];
A = [];
b = [];
Aeq = [];
beq = [];

tic
for x = 1:XMAX
    for y = 1:YMAX
        
        Avox = dwis(:,x,y,25);
        
        % Function to call only with one parameter
        evalFunc = @(x) ZeppelinStickSSD(x, Avox, bvals, qhat);
        
        RESNORMMIN = 1e+30; % high value
        
        % Apply DTI to get good initial points
        try
            [startx] = DT_SSD(Avox, bvals, qhat);
        catch ME
            warning(ME.message);
%             disp(ME.message);
            startx = [1.5e+05 1e-03 2.5e-01 0 0];
        end
        
        % Add gaussians around initial points
        xini = startx(1) + (startx(1)/2).*randn(N,1); %7.5e+05;
        diffini = startx(2) + (startx(2)/2).*randn(N,1); %3e-03;
        fini = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
        thetaini = startx(4) + (startx(4)/2).*randn(N,1); %0;
        phiini = startx(5) + (startx(5)/2).*randn(N,1); %0;
        alphaini = 0.5 + 0.5.*randn(N,1);
        betaini  = 0.5 + 0.5.*randn(N,1);
        
        % Initial points matrix
        initialPoints = [xini diffini fini thetaini phiini alphaini betaini];
                
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off'); %'iter');

            % Run the fitting
            try
                              
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFunc, initialPoints(i,:), A, b, Aeq, beq, lb, ub, [], h);
                
                RESNORMCounterZS(i,x,y) = RESNORM;
                
            catch ME
                warning(ME.message);
%                 disp(ME.message);
                
                RESNORM = 1e+30; % high value
                RESNORMCounterZS(i,x,y) = RESNORM;
            end
            
            % Remember the best fit
            if RESNORM < RESNORMMIN
                
                RESNORMMIN = RESNORM;
                
                mS0ZS(x,y) = parameter_hat(1);
                mdZS(x,y) = parameter_hat(2);
                mfZS(x,y) = parameter_hat(3);
                mthetaZS(x,y) = parameter_hat(4);
                mphiZS(x,y) = parameter_hat(5);
                mlambda1ZS = parameter_hat(6);
                mlambda2ZS = parameter_hat(7);
                mResnormZS(x,y) = RESNORM;
                
            end
        end
        
    end
    x
end
toc
% 
% savefile = 'Q136_Zep';
% save(savefile, 'mS0ZS', 'mdZS', 'mfZS', 'mthetaZS', 'mphiZS', 'mlambda1ZS', 'mlambda2ZS', 'mResnormZS');
% 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ZEPPELIN & STICK WITH TORTUOSITY %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0ZTS = zeros(XMAX,YMAX);
mdZTS = zeros(XMAX,YMAX);
mfZTS = zeros(XMAX,YMAX);
mthetaZTS = zeros(XMAX,YMAX);
mphiZTS = zeros(XMAX,YMAX);
mlambda1ZTS = zeros(XMAX,YMAX);
mlambda2ZTS = zeros(XMAX,YMAX);
mResnormZTS = zeros(XMAX,YMAX);

RESNORMCounterZTS = zeros(N,XMAX,YMAX);

% Constrains
lb = [ 0    ,0    ,0,-Inf,-Inf,0];
ub = [10e+05,1e-02,1, Inf, Inf,1];
A = [];
b = [];
Aeq = [];
beq = [];


tic
for x = 1:XMAX
    for y = 1:YMAX
        
        Avox = dwis(:,x,y,25);
        
        % Function to call only with one parameter
        evalFunc = @(x) ZeppelinTortStickSSD(x, Avox, bvals, qhat);
        
        RESNORMMIN = 1e+30; % high value
        
        % Apply DTI to get good initial points
        try
            [startx] = DT_SSD(Avox, bvals, qhat);
        catch ME
            warning(ME.message);
%             disp(ME.message);
            startx = [1.5e+05 1e-03 2.5e-01 0 0];
        end
        
        % Add gaussians around initial points
        xini = startx(1) + (startx(1)/2).*randn(N,1); %7.5e+05;
        diffini = startx(2) + (startx(2)/2).*randn(N,1); %3e-03;
        fini = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
        thetaini = startx(4) + (startx(4)/2).*randn(N,1); %0;
        phiini = startx(5) + (startx(5)/2).*randn(N,1); %0;
        lambda1ini = 0.5 + 0.5.*randn(N,1);
        
        % Initial points matrix
        initialPoints = [xini diffini fini thetaini phiini lambda1ini];
                
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off'); %'iter');

            % Run the fitting
            try
                              
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFunc, initialPoints(i,:), A, b, Aeq, beq, lb, ub, [], h);
                
                RESNORMCounterZS(i,x,y) = RESNORM;
                
            catch ME
                warning(ME.message);
%                 disp(ME.message);
                
                RESNORM = 1e+30; % high value
                RESNORMCounterZS(i,x,y) = RESNORM;
            end
            
            % Remember the best fit
            if RESNORM < RESNORMMIN
                
                RESNORMMIN = RESNORM;
                
                mS0ZTS(x,y) = parameter_hat(1);
                mdZTS(x,y) = parameter_hat(2);
                mfZTS(x,y) = parameter_hat(3);
                mthetaZTS(x,y) = parameter_hat(4);
                mphiZTS(x,y) = parameter_hat(5);
                mlambda1ZTS = parameter_hat(6);
                mlambda2ZTS(i) = (1 - parameter_hat(3)) * parameter_hat(6);
                mResnormZTS(x,y) = RESNORM;
                
            end
        end
        
    end
    x
end
toc
% 
% savefile = 'Q136_ZepTort';
% save(savefile, 'mS0ZTS', 'mdZTS', 'mfZTS', 'mthetaZTS', 'mphiZTS', 'mlambda1ZTS', 'mlambda2ZTS', 'mResnormZTS');








savefile = 'Q136_Zep';
load(savefile, 'mS0ZS', 'mdZS', 'mfZS', 'mthetaZS', 'mphiZS', 'mlambda1ZS', 'mlambda2ZS', 'mResnormZS');
mdZS(mdZS > 3e-03) = 3e-03;

savefile = 'Q136_ZepTort';
load(savefile, 'mS0ZTS', 'mdZTS', 'mfZTS', 'mthetaZTS', 'mphiZTS', 'mlambda1ZTS', 'mlambda2ZTS', 'mResnormZTS');
mdZTS(mdZTS > 3e-03) = 3e-03;

figure,
subplot(2,2,1)
imshow(mS0ZS, []), title('S0 map (ZeppelinStick)'), colormap hot, colorbar;
subplot(2,2,2)
imshow(mdZS, []), title('d map (ZeppelinStick)'), colormap hot, colorbar;
subplot(2,2,3)
imshow(mfZS, []), title('f map (ZeppelinStick)'), colormap hot, colorbar;
subplot(2,2,4)
imshow(mResnormZS, []), title('RESNORM map (ZeppelinStick)'), colormap hot, colorbar;
saveas(gcf,'Q136mapsZS.png');

figure,
subplot(2,2,1)
imshow(mS0ZTS, []), title('S0 map (ZeppelinTortStick)'), colormap hot, colorbar;
subplot(2,2,2)
imshow(mdZTS, []), title('d map (ZeppelinTortStick)'), colormap hot, colorbar;
subplot(2,2,3)
imshow(mfZTS, []), title('f map (ZeppelinTortStick)'), colormap hot, colorbar;
subplot(2,2,4)
imshow(mResnormZTS, []), title('RESNORM map (ZeppelinTortStick)'), colormap hot, colorbar;
saveas(gcf,'Q136mapsZTS.png');




% end