% function Q1ParameterEstimation()
% Function to get best possible values with Ball&Stick on a slice

%%% Load data
[dwis qhat bvals] = loadData();

% Initial Params
N = 20;
XMAX = 112;
YMAX = 112;

S = zeros(33,112,112);

% Maps of:
mS0 = zeros(XMAX,YMAX);
md = zeros(XMAX,YMAX);
mf = zeros(XMAX,YMAX);
mResnorm = zeros(XMAX,YMAX);
mtheta = zeros(XMAX,YMAX);
mphi = zeros(XMAX,YMAX);

RESNORMCounter = zeros(N,XMAX,YMAX);

% Fmincon params
lb = [ 0    , 0   , 0, -Inf, -Inf];
ub = [10e+05, 1e-2, 1,  Inf,  Inf];
A = [];
b = [];
Aeq = [];
beq = [];

tic
for x = 1:XMAX
    for y = 1:YMAX
        
        Avox = dwis(:,x,y,25);
        
        % Function to call only with one parameter
        evalFunc = @(x) BallStickSSD(x, Avox, bvals, qhat);
        
        RESNORMMIN = 1e+30; % high value
        
        % Apply DTI to get good initial points
        try
            [startx] = DT_SSD(Avox, bvals, qhat);
        catch ME
            warning(ME.message);
%             disp(ME.message);
            startx = [1.5e+05 1e-03 2.5e-01 0 0];
        end
        
        % Add gaussians around initial points
        xini = startx(1) + (startx(1)/2).*randn(N,1); %7.5e+05;
        diffini = startx(2) + (startx(2)/2).*randn(N,1); %3e-03;
        fini = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
        thetaini = startx(4) + (startx(4)/2).*randn(N,1); %0;
        phiini = startx(5) + (startx(5)/2).*randn(N,1); %0;
        
        % Initial points matrix
        initialPoints = [xini diffini fini thetaini phiini];
                
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off'); %'iter');

            % Run the fitting
            try
                              
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFunc, initialPoints(i,:), A, b, Aeq, beq, lb, ub, [], h);
                
                RESNORMCounter(i,x,y) = RESNORM;
                
            catch ME
                warning(ME.message);
%                 disp(ME.message);
                
                RESNORM = 1e+30; % high value
                RESNORMCounter(i,x,y) = RESNORM;
            end
            
            % Remember the best fit
            if RESNORM < RESNORMMIN
                
                RESNORMMIN = RESNORM;
                
                mS0(x,y) = parameter_hat(1);
                md(x,y) = parameter_hat(2);
                mf(x,y) = parameter_hat(3);
                mtheta(x,y) = parameter_hat(4);
                mphi(x,y) = parameter_hat(5);
                mResnorm(x,y) = RESNORM;
                
            end
        end
        
    end
%     x
end
toc


savefile = 'Q1_ParameterEstimation';
save(savefile, 'mS0', 'md', 'mf', 'mtheta', 'mphi', 'mResnorm');

% load(savefile, 'mS0', 'md', 'mf', 'mtheta', 'mphi', 'mResnorm');

% figure, imshow(mS0, []), colormap parula, title('S0 map');
% % imwrite(mS0./max(max(mS0)), 'qQ114S0.png');
% figure, imshow(md, []), colormap parula, title('d map');
% % imwrite(md./max(max(md)), 'qQ114d.png');
% figure, imshow(mf, []), colormap parula, title('f map');
% % imwrite(mf./max(max(mf)), 'qQ114f.png');
% figure, imshow(mResnorm, []), colormap parula, title('RESNORM map');
% % imwrite(mResnorm./max(max(mResnorm)), 'qQ114Resnorm.png');
% % Avox = reshape(dwis(2,:,:,25),[112,112]);
[X,Y,Z] = sph2cart(mtheta, mphi, 1);
% X = -1.*X;
% Y = -1.*Y;
figure, imagesc(mS0), colormap bone
hold on
quiver((X.*mf),(Y.*mf),'color',[1,0,0]), title('Fibre Direction map')
% % saveas(gcf,'qQ114fibdir.png');








% end