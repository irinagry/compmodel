% function Q142()
% % Exercise Q1.4.2.

datafile = 'data/challengeOPEN.txt';
fid = fopen(datafile, 'r', 'b');

% Read in the header
A = fgetl(fid);

% Read in the data
A = fscanf(fid, '%f', [8, inf]);
fclose(fid);

% Create the protocol
Avox = A(1,:)';
qhat = A(2:4,:);
G = A(5,:);
delta = A(6,:);
smalldel = A(7,:);
TE = A(8,:);
GAMMA = 2.675987E8;
bvals = ((GAMMA*smalldel.*G).^2).*(delta-smalldel/3);
bvals = bvals.*1e-06;

sigma = 0.04;
K = length(Avox);

% Number of Runs
N = 5;

% Global Params
xini = 0 + 1.*randn(N,1);
diffini = 1.5e-3 + 1.5e-3.*randn(N,1);
fini = 0 + 1.*randn(N,1);
thetaini = pi + 2*pi*randn(N,1);
phiini = pi + 2*pi*randn(N,1); 

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% BALL & STICK %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

% Initial Points
startx = [xini diffini fini thetaini phiini];
%startx = [0 0 0 0 0];

% Constrains
lb = [0,0,0,-Inf,-Inf];
ub = [3,1,1, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMCounterBS = zeros(N,1);
finalPointsBS = zeros(24, 5);

% For each shell
for s = 1:24
    
    RESNORMMINBS = 1e+30; % high value
    
    for i = 1:N
        % Define various options for the non-linear fitting algorithm
        h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                     'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

        % Run the fitting
        try
            % Define a starting point for the non-linear fit
            % Apply inverse transformations

            evalFunc = @(x) BallStickSSD(x, Avox((1 + (s-1)*48) : (48 + (s-1)*48), 1) , ...
                                            bvals(1, (1 + (s-1)*48) : (48 + (s-1)*48)), ...
                                            qhat(:,(1 + (s-1)*48) : (48 + (s-1)*48))  );
    %         evalFunc(startx);
            % Run the fitting
            [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

            % Remember the RESNORM PARAMETER
            RESNORMCounterBS(i) = RESNORM;

        catch ME
            warning(ME.message);
            disp(ME.message);

            RESNORM = 1e+30; % high value
            RESNORMCounterBS(i) = RESNORM;

        end

        if RESNORM < RESNORMMINBS
            RESNORMMINBS = RESNORM;
            finalPointsBS(s,:) = parameter_hat;
        end

    end
    
    fprintf('Shell %d\n', s);
end


%%%%%%%%%%%%%%%%%%% FISHER MATRICES
fisherMatrices = zeros(5,5,24);
fisherMatricesT2 = zeros(5,5,24);

aoptimality   = zeros(24,1);
aoptimalityT2 = zeros(24,1);

% Calculate fisher matrix for each shell
for s = 1:24
    
    fisherMatrices(:,:,s) = fisherMatrix(...
                            finalPointsBS(s,:)                        , ...
                            Avox((1 + (s-1)*48) : (48 + (s-1)*48), 1) , ...
                            bvals(1, (1 + (s-1)*48) : (48 + (s-1)*48)), ...
                            qhat(:,(1 + (s-1)*48) : (48 + (s-1)*48))  , ...
                            sigma);

end

% Compute A-Optimality Criteria
for s = 1:24
    
    aoptimality(s) = trace(pinv(fisherMatrices(1:3,1:3,s)));

end

%%%%%%%%%%%%%%%% SCALING
% Calculate fisher matrix for each shell after scaling
T2 = 0.09;
finalPointsBSScaled = finalPointsBS;
for s = 1:24
    
    finalPointsBSScaled(s,1) = finalPointsBSScaled(s,1) / exp(-TE(5 + (s-1)*48)/T2);
    
    fisherMatricesT2(:,:,s) = fisherMatrix(...
                            finalPointsBSScaled(s,:)                  , ...
                            Avox((1 + (s-1)*48) : (48 + (s-1)*48), 1) , ...
                            bvals(1, (1 + (s-1)*48) : (48 + (s-1)*48)), ...
                            qhat(:,(1 + (s-1)*48) : (48 + (s-1)*48))  , ...
                            sigma);

end

% Compute A-Optimality Criteria after scaling
for s = 1:24
    
    aoptimalityT2(s) = trace(pinv(fisherMatricesT2(1:3,1:3,s)));

end


% Best shell no scaling
[Y, bestShell] = min(aoptimality);
% Best shell after scaling
[Y, bestShellScaling]  = min(aoptimalityT2);
[Y, worstShellScaling] = max(aoptimalityT2);
randomShell = floor(1+rand*24);

while randomShell == bestShellScaling || randomShell == worstShellScaling
    randomShell = floor(1+rand*24);
end




%%%% RUN PARAMETRIC BOOTSTRAP ON THESE 2

%% BEST SHELL
predicted_best = finalPointsBSScaled(bestShellScaling, :);
[predictedS_best] = BallStick(predicted_best, ...
                              bvals(1, (1 + (bestShellScaling-1)*48) : (48 + (bestShellScaling-1)*48)), ...
                              qhat(:,(1 + (bestShellScaling-1)*48) : (48 + (bestShellScaling-1)*48)));

% Parametric Bootstrap
T = 100;

% Add noise 
K = 48; N = 5;
residuals = sum((Avox((1 + (bestShellScaling-1)*48) : (48 + (bestShellScaling-1)*48), 1) - predictedS_best').^2);
noise = (1/(K - N)) * residuals;
noiseModel = sqrt(noise).*randn(T,48);

Sperturbed = repmat(predictedS_best,[T,1]) + noiseModel;

% Values to be maped
mS0 = zeros(1,T);
mDiff = zeros(1,T);
mF = zeros(1,T);

iniN = 5;

tic
for i = 1:T

    evalFunc2 = @(x) BallStickSSD(x, Sperturbed(i,:)', ...
                                     bvals(1, (1 + (bestShellScaling-1)*48) : (48 + (bestShellScaling-1)*48)), ...
                                     qhat(:,(1 + (bestShellScaling-1)*48) : (48 + (bestShellScaling-1)*48)));
    
    RESNORMMIN = 1e+30; % high value
    
    for j = 1:iniN
        
        try
            [parameter_hat_boot, RESNORM, EXITFLAG, OUTPUT] = ...
                fmincon(evalFunc2, startx(j,:), A, b, Aeq, beq, lb, ub, [], h);
        catch ME
            disp(ME.message);
            RESNORM = 1e+30; % high value
        end
        
        if RESNORM < RESNORMMIN
            
            % In parameter_hat I have the parameters at which my 
            % sum of square differences is minimum

            RESNORMMIN = RESNORM;
            mS0(i) = parameter_hat_boot(1);
            mDiff(i) = parameter_hat_boot(2);
            mF(i) = parameter_hat_boot(3);
            
        end
        
    end
end
toc

% In parameter_hat I have the parameters at which my 
% sum of square differences is minimum

% figure 
% 
% subplot(1,3,1)
% hist(mS0, T/2), title('S0')
% 
% subplot(1,3,2)
% hist(mDiff, T/2), title('diffusivity')
% 
% subplot(1,3,3)
% hist(mF, T/2), title('fraction of anisotropic and isotropic')

stdS0best = std(mS0);
stdDbest  = std(mDiff);
stdFbest  = std(mF);
best = stdS0best^2 + stdDbest^2 + stdFbest^2;



%%%%%%%%%%%% WORST SHELL
predicted_worst = finalPointsBSScaled(worstShellScaling, :);
[predictedS_worst] = BallStick(predicted_worst, ...
                              bvals(1, (1 + (worstShellScaling-1)*48) : (48 + (worstShellScaling-1)*48)), ...
                              qhat(:,(1 + (worstShellScaling-1)*48) : (48 + (worstShellScaling-1)*48)));

% Parametric Bootstrap
T = 100;

% Add noise 
K = 48; N = 5;
residuals = sum((Avox((1 + (worstShellScaling-1)*48) : (48 + (worstShellScaling-1)*48), 1) - predictedS_worst').^2);
noise = (1/(K - N)) * residuals;
noiseModel = sqrt(noise).*randn(T,48);

Sperturbed = repmat(predictedS_worst,[T,1]) + noiseModel;

% Values to be maped
mS0 = zeros(1,T);
mDiff = zeros(1,T);
mF = zeros(1,T);

iniN = 5;

tic
for i = 1:T

    evalFunc2 = @(x) BallStickSSD(x, Sperturbed(i,:)', ...
                                     bvals(1, (1 + (worstShellScaling-1)*48) : (48 + (worstShellScaling-1)*48)), ...
                                     qhat(:,(1 + (worstShellScaling-1)*48) : (48 + (worstShellScaling-1)*48)));
    
    RESNORMMIN = 1e+30; % high value
    
    for j = 1:iniN
        
        try
            [parameter_hat_boot, RESNORM, EXITFLAG, OUTPUT] = ...
                fmincon(evalFunc2, startx(j,:), A, b, Aeq, beq, lb, ub, [], h);
        catch ME
            disp(ME.message);
            RESNORM = 1e+30; % high value
        end
        
        if RESNORM < RESNORMMIN
            
            % In parameter_hat I have the parameters at which my 
            % sum of square differences is minimum

            RESNORMMIN = RESNORM;
            mS0(i) = parameter_hat_boot(1);
            mDiff(i) = parameter_hat_boot(2);
            mF(i) = parameter_hat_boot(3);
            
        end
        
    end
end
toc

% In parameter_hat I have the parameters at which my 
% sum of square differences is minimum

% figure 
% 
% subplot(1,3,1)
% hist(mS0, T/2), title('S0')
% 
% subplot(1,3,2)
% hist(mDiff, T/2), title('diffusivity')
% 
% subplot(1,3,3)
% hist(mF, T/2), title('fraction of anisotropic and isotropic')
% 
stdS0worst = std(mS0);
stdDworst = std(mDiff);
stdFworst = std(mF);
worst = stdS0worst^2 + stdDworst^2 + stdFworst^2;










% 
% figure, 
% for i = 1:24
%     plot(finalPointsBS(i,:),'s');
%     hold on
% end
% 
% 
% figure, 
% for i = 1:24
%     plot(finalPointsBSScaled(i,:),'s');
%     hold on
% end





% end