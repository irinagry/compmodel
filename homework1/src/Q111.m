%function Q111()
% Question 1.1.1

%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
Avox = dwis(:,52,62,25);
imshow(squeeze(dwis(1,:,:,25)), []);
return
% Define a starting point for the non-linear fit
startx = [7.5e+05 3e-03 2.5e-01 0 0];

% Define various options for the non-linear fitting algorithm
h = optimset('MaxFunEvals', 20000, 'Algorithm', 'levenberg-marquardt',...
    'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

% Run the fitting
[parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
    fminunc('BallStickSSD', startx, h, Avox, bvals, qhat)

% In parameter_hat I have the parameters at which my 
% sum of square differences is minimum
parameter_hat
RESNORM
% Compare predictions with actual measurements:
[predictedS] = BallStick(parameter_hat, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
legend('Data','Model')
title('Measured vs Predicted Values')
xlabel('index') % x-axis label
ylabel('S') % y-axis label
saveas(gcf,'Q111.png');

% Is the final value of RESNORM above the kind of value 
% for the sum of square differences we expect?
% Yes

% STD for noise is between 5000-6000
% What would you expect a typical value of RESNORM to be?
% TODO: How do I incorporate STD?

% Are the params sensible?
% No, some are physically impossible



% end