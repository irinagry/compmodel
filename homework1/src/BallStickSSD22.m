function [sumRes, resJ] = BallStickSSD22(x, Avox, bvals, qhat)
% Function for the Ball and Stick Model for Q122
%
% Input Params:
%  x = initial parameters
%  Avox = measurements from a single voxel from all the 33 image volumes
%  bvals = the b-value array
%  qhat = the gradient directions
% 
% Returns: 
%  sumRes = the sum of the square differences
%  S = the calculated signal
%

finalX = scaleTransformBackParams(x);

% Extract the parameters and do transformations on them
% signal for b=0 has to be positive
S0 = finalX(1); 
% initial diffusivity has to be positive
diff = finalX(2); 
% fraction of signal contributed by anisotropy has to be [0,1]
f = finalX(3);
theta = finalX(4); % theta angle for polar coords
phi = finalX(5); % phi angle for polar coords
% finalX
%% Synthesize the signals
% Fiber directions (cylinders have rho = 0, so we only get the first column
% of the rotation matrix)
fibDir = [cos(phi)*sin(theta) sin(phi)*sin(theta) cos(theta)];

% Gradient direction * fiber directions 
% Why sum?
fibDotGrad = sum(qhat .* repmat(fibDir, [length(qhat) 1])');

% The signal model: anisotropic part + isotropic part
% Why fibDotGrad.^2?
S = S0*(f*exp(-bvals * diff .* (fibDotGrad .^2)) + (1-f)*exp(-bvals*diff));

% Compute the sum of the square differences
sumRes = sum((Avox - S').^2);

end