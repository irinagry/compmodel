function test()
% Function to test different things

% x = fminunc(fun,x0) starts at the point x0 
% and attempts to find a local minimum x 
% of the function described in fun. 
% The point x0 can be a scalar, vector, or matrix.

% fun = @(x)3*x(1)^2 + 2*x(1)*x(2) + x(2)^2 - 4*x(1) + 5*x(2);
% 
% x0 = [1,1];
% [x,fval] = fminunc(fun, x0);

options = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton');
problem.options = options;
problem.x0 = [-1,2];
problem.objective = @testFminuncRosenbrockwithgrad;
problem.solver = 'fminunc';

[x,fval,exitflag,output] = fminunc(problem)

end