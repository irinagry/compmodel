% function Q136ModelSelection()
% Question 1.3.6 - model selection criterion

XMAX = 112; YMAX = 112;
K = 33;
sigma = 6000;

%%%%% Ball & Stick Model
% Load Data
savefile = 'Q114';
load(savefile, 'mResnorm');
% Compute AIC and BIC for the whole slice
aicBSM = zeros(XMAX, YMAX);
bicBSM = zeros(XMAX, YMAX);
NBallStick = 5;

for i = 1:XMAX
    for j = 1:YMAX
        
        aicBSM(i,j) = computeCorrectedAIC(NBallStick, K, sigma, mResnorm(i,j));
        bicBSM(i,j) = computeBIC(NBallStick, K, sigma, mResnorm(i,j));
        
    end
end

%%%%% Zeppelin & Stick Model
% Load Data
savefile = 'Q136_Zep';
load(savefile, 'mResnormZS');
% Compute AIC and BIC for the whole slice
aicZSM = zeros(XMAX, YMAX);
bicZSM = zeros(XMAX, YMAX);
NZeppelinStick = 7;

for i = 1:XMAX
    for j = 1:YMAX
        
        aicZSM(i,j) = computeCorrectedAIC(NZeppelinStick, K, sigma, mResnormZS(i,j));
        bicZSM(i,j) = computeBIC(NZeppelinStick, K, sigma, mResnormZS(i,j));
        
    end
end

%%%%% Zeppelin Tort & Stick Model
savefile = 'Q136_ZepTort';
load(savefile, 'mResnormZTS');
% Compute AIC and BIC for the whole slice
aicZTSM = zeros(XMAX, YMAX);
bicZTSM = zeros(XMAX, YMAX);
NZeppelinTortStick = 6;

for i = 1:XMAX
    for j = 1:YMAX
        
        aicZTSM(i,j) = computeCorrectedAIC(NZeppelinTortStick, K, sigma, mResnormZTS(i,j));
        bicZTSM(i,j) = computeBIC(NZeppelinTortStick, K, sigma, mResnormZTS(i,j));
        
    end
end

%%%%% Ball & 2 Sticks Model
savefile = 'Q135_wholeslice_b2s';
load(savefile, 'mResnormB2S');
% Compute AIC and BIC for the whole slice
aicB2SM = zeros(XMAX, YMAX);
bicB2SM = zeros(XMAX, YMAX);
NBall2Stick = 8;

for i = 1:XMAX
    for j = 1:YMAX
        
        aicB2SM(i,j) = computeCorrectedAIC(NBall2Stick, K, sigma, mResnormB2S(i,j));
        bicB2SM(i,j) = computeBIC(NBall2Stick, K, sigma, mResnormB2S(i,j));
        
    end
end

%%%%% Zeppelin & 2 Sticks Model
savefile = 'Q135_wholeslice_z2s';
load(savefile, 'mResnormZ2S');
% Compute AIC and BIC for the whole slice
aicZ2SM = zeros(XMAX, YMAX);
bicZ2SM = zeros(XMAX, YMAX);
NZeppelin2Stick = 12;

for i = 1:XMAX
    for j = 1:YMAX
        
        aicZ2SM(i,j) = computeCorrectedAIC(NZeppelin2Stick, K, sigma, mResnormZ2S(i,j));
        bicZ2SM(i,j) = computeBIC(NZeppelin2Stick, K, sigma, mResnormZ2S(i,j));
        
    end
end


%%%% PLOT

aicMap = zeros(XMAX, YMAX);
bicMap = zeros(XMAX, YMAX);
for i = 1:XMAX
    for j = 1:YMAX
   
        comparisonAIC = [aicBSM(i,j) ; aicZSM(i,j) ; aicZTSM(i,j) ; aicZ2SM(i,j) ];
        [xa posA] = min(comparisonAIC);
        aicMap(i,j) = posA;
        
        comparisonBIC = [bicBSM(i,j) ; bicZSM(i,j) ; bicZTSM(i,j) ; bicZ2SM(i,j)];
        [xb posB] = min(comparisonBIC);
        bicMap(i,j) = posB;
       
    end
end


figure, 
subplot(2,1,1)
imshow(aicMap,[]), colormap hot, title('AIC (corrected)'), colorbar

subplot(2,1,2)
imshow(bicMap,[]), colormap hot, title('BIC'), colorbar
legend('black(1) Ball&Stick', 'red(2)Zeppelin&Stick', 'yellow(3)Zeppelin&StickTort', '4(white)Zeppelin&2Sticks');
saveas(gcf,'Q136ModelSelectionZ2S.png');

%%%% PLOT

aicMap = zeros(XMAX, YMAX);
bicMap = zeros(XMAX, YMAX);
for i = 1:XMAX
    for j = 1:YMAX
   
        comparisonAIC = [aicBSM(i,j) ; aicZSM(i,j) ; aicZTSM(i,j) ; aicB2SM(i,j) ];
        [xa posA] = min(comparisonAIC);
        aicMap(i,j) = posA;
        
        comparisonBIC = [bicBSM(i,j) ; bicZSM(i,j) ; bicZTSM(i,j) ; bicB2SM(i,j)];
        [xb posB] = min(comparisonBIC);
        bicMap(i,j) = posB;
       
    end
end


figure,
subplot(2,1,1)
imshow(aicMap,[]), colormap hot, title('AIC (corrected)'), colorbar

subplot(2,1,2)
imshow(bicMap,[]), colormap hot, title('BIC'), colorbar
legend('black(1) Ball&Stick', 'red(2)Zeppelin&Stick', 'yellow(3)Zeppelin&StickTort', '4(white)Ball&2Sticks')
saveas(gcf,'Q136ModelSelectionB2S.png');









% end