function [S] = Zeppelin2Stick(x, bvals, qhat)
% Function for the Zeppelin and 2 Sticks Model
%
% Input Params:
%  x = initial parameters
%  Avox = measurements from a single voxel from all the 33 image volumes
%  bvals = the b-value array
%  qhat = the gradient directions
% 
% Returns: 
%  sumRes = the sum of the square differences
%  S = the calculated signal
%

% Extract the parameters
S0 = x(1); % signal for b=0
diff = x(2); % initial diffusivity
f1 = x(3); % stick 1
f2 = x(4); % stick 2
theta1 = x(5); % theta angle for stick 1 
phi1 = x(6); % phi angle for stick 1 
theta2 = x(7); % theta angle for stick 2
phi2 = x(8); % phi angle for stick 2
theta = x(9);  % theta angle for zeppelin
phi   = x(10); % phi angle for zeppelin
alpha = x(11); % lambda1 - lambda2 > 0
beta =  x(12); % lambda2           > 0

%% Synthesize the signals
% Fiber directions (cylinders have rho = 0, so we only get the first column
% of the rotation matrix)
fibDir  = [cos(phi )*sin(theta ) sin(phi )*sin(theta ) cos(theta )];
fibDir1 = [cos(phi1)*sin(theta1) sin(phi1)*sin(theta1) cos(theta1)];
fibDir2 = [cos(phi2)*sin(theta2) sin(phi2)*sin(theta2) cos(theta2)];

% Gradient direction * fiber directions 
fibDotGrad  = sum(qhat .* repmat(fibDir , [length(qhat) 1])');
fibDotGrad1 = sum(qhat .* repmat(fibDir1, [length(qhat) 1])');
fibDotGrad2 = sum(qhat .* repmat(fibDir2, [length(qhat) 1])');

% alpha = lambda1 - lambda2;
% beta  = lambda2;

% The signal model: anisotropic part + isotropic part
% Why fibDotGrad.^2?
S = S0 * ( f1 * exp(-bvals * diff .* (fibDotGrad1.^2)) + ...
           f2 * exp(-bvals * diff .* (fibDotGrad2.^2))  + ...
      (1 - f1 - f2) * exp(-bvals .* (beta + alpha * (fibDotGrad .^2))));


end