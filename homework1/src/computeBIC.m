function bic = computeBIC(N, K, sigma, SSD)
% Function that computes BIC

    %%% BIC %%%%% N log K + 1/sigma^2 L
    bic = N * log(K) + (1/(sigma^2)) * SSD;
    
end