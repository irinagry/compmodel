%function Q132()
% Question 1.3.2

datafile = 'data/challengeOPEN.txt';
fid = fopen(datafile, 'r', 'b');

% Read in the header
A = fgetl(fid);

% Read in the data
A = fscanf(fid, '%f', [8, inf]);
fclose(fid);

% Create the protocol
Avox = A(1,:)';
qhat = A(2:4,:);
G = A(5,:);
delta = A(6,:);
smalldel = A(7,:);
TE = A(8,:);
GAMMA = 2.675987E8;
bvals = ((GAMMA*smalldel.*G).^2).*(delta-smalldel/3);
bvals = bvals.*1e-06;

sigma = 0.04;
K = length(Avox);

% Apply DTI and get initial params
% [startx] = DT_SSD(Avox, bvals, qhat)
% return
% xini = 1.1321e+05;
% diffini = 0.001534;
% fini = 0.5754;
% thetaini = 76.4332;
% phiini = 47.0108;


% Number of Runs
N = 100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% DIFFUSION TENSOR MODEL %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Final Points
[finalPointsDTM, predictedDTM, sumresDTM] = DT_SSD(Avox, bvals, qhat);

%%%%% AIC&BIC
NDTM = 7;
aicDTM = computeAIC(NDTM, sigma, sumresDTM);
bicDTM = computeBIC(NDTM, K, sigma, sumresDTM);

fprintf('Diffusion Tensor Model %f\n', sumresDTM);
% Compare predictions with actual measurements:
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedDTM,'rx'); % predictions
legend('Data','Model');

titlePlotDTM = sprintf('Measured vs Predicted Values Diffusion Tensor Model\nAIC = %4.4f\nBIC = %4.4f', aicDTM, bicDTM);

title(titlePlotDTM)

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% BALL & STICK %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

% Global Params
xini = 0 + 1.*randn(N,1);
diffini = 1.5e-3 + 1.5e-3.*randn(N,1);
fini = 0 + 1.*randn(N,1);
thetaini = pi + 2*pi*randn(N,1);
phiini = pi + 2*pi*randn(N,1); 

% Initial Points
startx = [xini diffini fini thetaini phiini];

% Constrains
lb = [0,0    ,0,-Inf,-Inf];
ub = [3,1e-01,1, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMMINBS = 1e+30; % high value
RESNORMCounterBS = zeros(N,1);
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    % Run the fitting
    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        
        evalFunc = @(x) BallStickSSD(x, Avox, bvals, qhat);
%         evalFunc(startx);
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

        % Remember the RESNORM PARAMETER
        RESNORMCounterBS(i) = RESNORM;
        
    catch ME
        warning(ME.message);
        disp(ME.message);

        RESNORM = 1e+30; % high value
        RESNORMCounterBS(i) = RESNORM;
        
    end

    if RESNORM < RESNORMMINBS
        RESNORMMINBS = RESNORM;
        finalPointsBS = parameter_hat;
    end
    
    disp('Ball&Stick');
    i
    
end

%%%%% AIC&BIC
NBallStick = 5;
aicBS = computeAIC(NBallStick, sigma, RESNORMMINBS);
bicBS = computeBIC(NBallStick, K, sigma, RESNORMMINBS);
finalPointsBS
return
fprintf('Ball and Stick %f\n', RESNORMMINBS);
% Compare predictions with actual measurements:
[predictedBS] = BallStick(finalPointsBS, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedBS,'rx'); % predictions
legend('Data','Model');

titlePlotBS = sprintf('Measured vs Predicted Values Ball & Stick\nAIC = %4.4f\nBIC = %4.4f', aicBS, bicBS);

title(titlePlotBS)


%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ZEPPELIN & STICK %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0ZS = zeros(N,1);
mdZS = zeros(N,1);
mfZS = zeros(N,1);
mthetaZS = zeros(N,1);
mphiZS = zeros(N,1);
mlambda1ZS = zeros(N,1);
mlambda2ZS = zeros(N,1);

% Initial Points
alphaini = 0.5 + 0.5.*randn(N,1);
betaini  = 0.5 + 0.5.*randn(N,1);
startx = [xini diffini fini thetaini phiini alphaini betaini];

% Constrains
lb = [0,0    ,0,-Inf,-Inf,0,0];
ub = [3,1e-02,1, Inf, Inf,1,1];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMMINZ = 1e+30; % high value
RESNORMCounterZS = zeros(N,1);
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    % Run the fitting
    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        
        evalFunc = @(x) ZeppelinStickSSD(x, Avox, bvals, qhat);
%         evalFunc(startx);
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

        % Remember the RESNORM PARAMETER
        RESNORMCounterZS(i) = RESNORM;
        
        % Remember the found params
        mS0ZS(i) = parameter_hat(1);
        mdZS(i) = parameter_hat(2);
        mfZS(i) = parameter_hat(3);
        mthetaZS(i) = parameter_hat(4);
        mphiZS(i) = parameter_hat(5);
        mlambda1ZS(i) = parameter_hat(6)+parameter_hat(7);
        mlambda2ZS(i) = parameter_hat(7);
        
    catch ME
        warning(ME.message);
        disp(ME.message);

        RESNORM = 1e+30; % high value
        RESNORMCounterZS(i) = RESNORM;
        
    end

    if RESNORM < RESNORMMINZ
        RESNORMMINZ = RESNORM;
        finalPointsZS = parameter_hat;
    end

    disp('Zeppelin');
    i
    
end

%%%%% AIC&BIC
NZeppelinStick = 7;
aicZ = computeAIC(NZeppelinStick, sigma, RESNORMMINZ);
bicZ = computeBIC(NZeppelinStick, K, sigma, RESNORMMINZ);

fprintf('Zeppelin and Stick %f\n', RESNORMMINZ);
% Display lambda 1 and lambda 2
% figure, plot(mlambda1ZS,'rs'),hold on, plot(mlambda2ZS,'bo')
% Compare predictions with actual measurements:
[predictedZ] = ZeppelinStick(finalPointsZS, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedZ,'rx'); % predictions
legend('Data','Model');

titlePlotZ = sprintf('Measured vs Predicted Values Zeppelin & Stick\nAIC = %4.4f\nBIC = %4.4f', aicZ, bicZ);

title(titlePlotZ)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ZEPPELIN & STICK WITH TORTUOSITY %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0ZTS = zeros(N,1);
mdZTS = zeros(N,1);
mfZTS = zeros(N,1);
mthetaZTS = zeros(N,1);
mphiZTS = zeros(N,1);
mlambda1ZTS = zeros(N,1);
mlambda2ZTS = zeros(N,1);

% Initial Points
lambda1ini = 0.5 + 0.5.*randn(N,1);

startx = [xini diffini fini thetaini phiini lambda1ini];

% Constrains
lb = [0,0    ,0,-Inf,-Inf,0];
ub = [3,1e-02,1, Inf, Inf,1];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMMINZT = 1e+30; % high value
RESNORMCounterZTS = zeros(N,1);
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    % Run the fitting
    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        
        evalFunc = @(x) ZeppelinTortStickSSD(x, Avox, bvals, qhat);
%         evalFunc(startx);
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

        % Remember the RESNORM PARAMETER
        RESNORMCounterZTS(i) = RESNORM;
        
        % Remember the found params
        mS0ZTS(i) = parameter_hat(1);
        mdZTS(i) = parameter_hat(2);
        mfZTS(i) = parameter_hat(3);
        mthetaZTS(i) = parameter_hat(4);
        mphiZTS(i) = parameter_hat(5);
        mlambda1ZTS(i) = parameter_hat(6);
        mlambda2ZTS(i) = (1 - parameter_hat(3)) * parameter_hat(6);
        
    catch ME
        warning(ME.message);
        disp(ME.message);

        RESNORM = 1e+30; % high value
        RESNORMCounterZTS(i) = RESNORM;
        
    end

    if RESNORM < RESNORMMINZT
        RESNORMMINZT = RESNORM;
        finalPointsZTS = parameter_hat;
    end
    
    disp('ZeppelinTortuosity');
    i
    
end

%%%%% AIC&BIC
NZeppelinTortStick = 6;
aicZT = computeAIC(NZeppelinTortStick, sigma, RESNORMMINZT);
bicZT = computeBIC(NZeppelinTortStick, K, sigma, RESNORMMINZT);

fprintf('Zeppelin Tort and Stick %f\n', RESNORMMINZT);
% Compare predictions with actual measurements:
[predictedZT] = ZeppelinTortStick(finalPointsZTS, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedZT,'rx'); % predictions
legend('Data','Model');

titlePlotZT = sprintf('Measured vs Predicted Values for Zeppelin Tortuosity & Stick\nAIC = %4.4f\nBIC = %4.4f', aicZT, bicZT);

title(titlePlotZT)



%%%%%%%%%%%%% Plotting all of them
figure

subplot(2,2,1)
plot(Avox,'bs'); % measurements
hold on
plot(predictedDTM,'rx'); % predictions
legend('Data','Model');
xlabel('index') % x-axis label
ylabel('S') % y-axis label
title(titlePlotDTM)

subplot(2,2,2)
plot(Avox,'bs'); % measurements
hold on
plot(predictedBS,'rx'); % predictions
legend('Data','Model');
xlabel('index') % x-axis label
ylabel('S') % y-axis label
title(titlePlotBS)

subplot(2,2,3)
plot(Avox,'bs'); % measurements
hold on
plot(predictedZ,'rx'); % predictions
legend('Data','Model');
xlabel('index') % x-axis label
ylabel('S') % y-axis label
title(titlePlotZ)

subplot(2,2,4)
plot(Avox,'bs'); % measurements
hold on
plot(predictedZT,'rx'); % predictions
legend('Data','Model');
xlabel('index') % x-axis label
ylabel('S') % y-axis label
title(titlePlotZT)

% saveas(gcf,'Q132.png');




%end





