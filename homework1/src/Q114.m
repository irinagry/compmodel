%function Q114()
% Question 1.1.4

%%% Load data
[dwis qhat bvals] = loadData();

% Initial Params
N = 5;
XMAX = 112;
YMAX = 112;

%%% To start with, let's extract the set of 33 measurements
% from all voxels:

S = zeros(33,112,112);
% figure, imshow(squeeze(Avox(1,:,:)), [])
% return

% TODO: ADD normally distributed random numbers to the initial values
% xini = 7.5e+05 + 2.5e+05.*randn(N,1); %7.5e+05;
% diffini = 3e-03 + 1e-03.*randn(N,1); %3e-03;
% fini = 2.5e-01 + 0.8e-01.*randn(N,1); %2.5e-01;
% thetaini = 0 + pi*rand(N,1); %0;
% phiini = 0 + pi*rand(N,1); %0;
% 
% xini = 1.132129323942616*1.0e+05; %7.5e+05*rand(N,1);
% diffini = 0.000000015341345*1.0e+05; %3e-03*rand(N,1);
% fini = 0.000005754285190*1.0e+05; %0.99*rand(N,1);
% thetaini = 0.000764332772467*1.0e+05; %2*pi*rand(N,1);
% phiini = 0.000470108408902*1.0e+05; %2*pi*rand(N,1);

% Define a starting point for the non-linear fit
startx = [1.1e+05 3e-03 2.5e-01 0 0];

xini = startx(1) + (startx(1)/2).*randn(N,1); %7.5e+05;
diffini = startx(2) + (startx(2)/2).*randn(N,1); %3e-03;
fini = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
thetaini = 0 + pi*randn(N,1); %0;
phiini = 0 + pi*randn(N,1); %0;

% Define multiple starting points
initialPoints = [xini diffini fini thetaini phiini];
%return

% Maps of:
mS0 = zeros(XMAX,YMAX);
md = zeros(XMAX,YMAX);
mf = zeros(XMAX,YMAX);
mResnorm = zeros(XMAX,YMAX);
mtheta = zeros(XMAX,YMAX);
mphi = zeros(XMAX,YMAX);

RESNORMCounter = zeros(N,XMAX,YMAX);

tic
for x = 1:XMAX
    for y = 1:YMAX
        
        Avox = dwis(:,x,y,25);
        finalPoints = zeros(1,5);
        RESNORMMIN = 1e+30; % high value
        
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'levenberg-marquardt',...
                'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off'); %'iter');

            % Run the fitting
            try
                % Define a starting point for the non-linear fit
                % Apply inverse transformations
                startx = [sqrt(abs(initialPoints(i,1))) sqrt(abs(initialPoints(i,2))) ...
                    invsin2(abs(initialPoints(i,3))) initialPoints(i,4) initialPoints(i,5)];
                
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fminunc('BallStickSSD2', startx, h, Avox, bvals, qhat);
                
                RESNORMCounter(i,x,y) = RESNORM;
            catch ME
                warning(ME.message);
                disp(ME.message);
                
                RESNORM = 1e+30; % high value
                RESNORMCounter(i,x,y) = RESNORM;
            end
            %size(Avox(:,x,y))
            %return
            %parameter_hat2
            if RESNORM < RESNORMMIN
                % In parameter_hat I have the parameters at which my 
                % sum of square differences is minimum
                parameter_hat2 = [parameter_hat(1)^2 ...
                    parameter_hat(2)^2 ...
                    sin2(parameter_hat(3)) ...
                    parameter_hat(4) ...
                    parameter_hat(5)];
                RESNORMMIN = RESNORM;
                finalPoints = parameter_hat2;
         
                mS0(x,y) = parameter_hat2(1);
                md(x,y) = parameter_hat2(2);
                mf(x,y) = parameter_hat2(3);
                mtheta(x,y) = parameter_hat2(4);
                mphi(x,y) = parameter_hat2(5);
                mResnorm(x,y) = RESNORM;
                
            end
        end
        
        %disp('(x,y)');
        %x
        %y
        %S(:,x,y) = BallStick(finalPoints, bvals, qhat);
    end
%     x
end
elapsedTime = toc

% size(S)
% What I got
disp('Q114\n');

% savefile = 'Q114';
% save(savefile, 'mS0', 'md', 'mf', 'mtheta', 'mphi', 'mResnorm', 'elapsedTime');
% 
% savefile = 'Q114';
% load(savefile, 'mS0', 'md', 'mf', 'mtheta', 'mphi', 'mResnorm', 'elapsedTime');
% md(md > 3e-03) = 3e-03;
% elapsedTime

% figure, imshow(mS0, []), title('S0 map');
% % imwrite(mS0./max(max(mS0)), 'Q114mapS0.png');
% figure, imshow(md, []), title('d map');
% % imwrite(md./max(max(md)), 'Q114mapd.png');
% figure, imshow(mf, []), title('f map');
% % imwrite(mf./max(max(mf)), 'Q114mapf.png');
% figure, imshow(mResnorm, []), title('RESNORM map');
% % imwrite(mResnorm./max(max(mResnorm)), 'Q114mapR.png');
% [X,Y,Z] = sph2cart(mtheta, mphi, 1);
% figure, quiver(X.*mf,Y.*mf), title('Fibre Direction map');
% saveas(gcf,'Q114fibdir.png');
% 
% figure,
% subplot(2,2,1)
% imshow(mS0, []), title('S0 map'), colormap hot, colorbar;
% subplot(2,2,2)
% imshow(md, []), title('d map'), colormap hot, colorbar;
% subplot(2,2,3)
% imshow(mf, []), title('f map'), colormap hot, colorbar;
% subplot(2,2,4)
% imshow(mResnorm, []), title('RESNORM map'), colormap hot, colorbar;
% saveas(gcf,'Q114maps.png');

% [X,Y,Z] = sph2cart(mtheta, mphi, 1);
% figure, imagesc(mS0), colormap bone
% hold on
% quiver((X.*mf),(Y.*mf),'.', 'filled','color',[1,0,0]), title('Fibre Direction Map')
% saveas(gcf,'Q114fibdir.png');

%end
