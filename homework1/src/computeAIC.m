function aic = computeAIC(N, sigma, SSD)
% Function that computes AIC

    %%% AIC %%%%% 2 N - 2 log L
    aic = 2*N + (1/(sigma^2)) * SSD;
    
end