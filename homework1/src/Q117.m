%function Q117()
% Question 1.1.7

%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
% WM  dwis(:,52,62,25);
% GM  dwis(:,66,26,25);
% CSF dwis(:,63,61,20);
Avox = dwis(:,63,61,20);

% Initial Params
N = 1;
xini = 1.1e+05 + (1.1e+05/6).*randn(N,1); %7.5e+05;
diffini = 3e-03 + (3e-03/6).*randn(N,1); %3e-03;
fini = 2.5e-01 + (2.5e-01/6).*randn(N,1); %2.5e-01;
thetaini = 0 + 2*pi*randn(N,1); %0;
phiini = 0 + 2*pi*randn(N,1); %0;

% Define multiple starting points
initialPoints = [xini diffini fini thetaini phiini];
finalPoints = zeros(1,5);

RESNORMMIN = 1e+30; % high value
RESNORMCounter = zeros(N,1);

% Fmincon parameters
evalFunc1 = @(x) BallStickSSD(x, Avox, bvals, qhat);
evalFunc2 = @(x) BallStickSSD7(x, Avox, bvals, qhat);
lb = [ 0    , 0   , 0, -Inf, -Inf];
ub = [10e+05, 1e-2, 1,  Inf,  Inf];
A = [];
b = [];
Aeq = [];
beq = [];

% Q116 -> doar fmincon
tic
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    try
        % Define a starting point for the non-linear fit
        startx = initialPoints(i,:);        
        
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc1, startx, A, b, Aeq, beq, lb, ub, [], h);
        
        RESNORMCounter(i) = RESNORM;
    
    catch ME
        warning(ME.message);
        disp(ME.message);
        
        RESNORM = 1e+30; % high value
        RESNORMCounter(i) = RESNORM;
    end

    if RESNORM < RESNORMMIN
        RESNORMMIN = RESNORM;
        finalPoints = parameter_hat;
    end
end
toc

% Q117 -> cu derivative on
RESNORMMIN = 1e+30; % high value
tic
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off', ...
                 'DerivativeCheck', 'off', 'GradObj', 'on');%, 'DerivativeCheck', 'on');

    try
        % Define a starting point for the non-linear fit
        startx = initialPoints(i,:);        
        
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc2, startx, A, b, Aeq, beq, lb, ub, [], h);
        
        RESNORMCounter(i) = RESNORM;
    
    catch ME
        warning(ME.message);
        disp(ME.message);
        
        RESNORM = 1e+30; % high value
        RESNORMCounter(i) = RESNORM;
    end

    if RESNORM < RESNORMMIN
        RESNORMMIN = RESNORM;
        finalPoints = parameter_hat;
    end
end
toc
% What I got
disp('Q117\n');
RESNORMMIN
finalPoints

trials = numel(find(floor(RESNORMCounter) == floor(min(RESNORMCounter))))

% Compare predictions with actual measurements:
[predictedS] = BallStick(parameter_hat, bvals, qhat);
figure
title('Measured vs Predicted Values')
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
legend('Data','Model');


%end