% function Q135()
% Question 1.3.5

datafile = 'data/challengeOPEN.txt';
fid = fopen(datafile, 'r', 'b');

% Read in the header
A = fgetl(fid);

% Read in the data
A = fscanf(fid, '%f', [8, inf]);
fclose(fid);

% Create the protocol
Avox = A(1,:)';
qhat = A(2:4,:);
G = A(5,:);
delta = A(6,:);
smalldel = A(7,:);
TE = A(8,:);
GAMMA = 2.675987E8;
bvals = ((GAMMA*smalldel.*G).^2).*(delta-smalldel/3);
bvals = bvals.*1e-06;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% BALL & 2 STICKS %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Number of Runs
N = 100;
sigma = 0.04;
K = length(Avox);

% Global Params
xini = 0 + 1.*randn(N,1);
diffini = 1.5e-10 + 1.5e-5.*randn(N,1);
fini1 = 0 + 1.*randn(N,1);
fini2 = 0 + 1.*randn(N,1);
thetaini1 = pi + 2*pi*randn(N,1);
phiini1 = pi + 2*pi*randn(N,1); 
thetaini2 = pi + 2*pi*randn(N,1);
phiini2 = pi + 2*pi*randn(N,1); 

% Initial Points
startx = [xini diffini fini1 fini2 thetaini1 phiini1 thetaini2 phiini2];

% Constrains
lb = [0,0,    0,0,-Inf,-Inf,-Inf,-Inf];
ub = [3,1e-02,1,1, Inf, Inf, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMMINB2S = 1e+30; % high value
RESNORMCounterBS = zeros(N,1);

for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    % Run the fitting
    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        
        evalFunc = @(x) BallTwoStickSSD(x, Avox, bvals, qhat);
%         evalFunc(startx);
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

        % Remember the RESNORM PARAMETER
        RESNORMCounterBS(i) = RESNORM;
        
    catch ME
        warning(ME.message);
        disp(ME.message);

        RESNORM = 1e+30; % high value
        RESNORMCounterBS(i) = RESNORM;
        
    end

    if RESNORM < RESNORMMINB2S
        RESNORMMINB2S = RESNORM;
        finalPointsBS = parameter_hat;
    end
    
    disp('Ball&TwoSticks');
    i
    
end

fprintf('Ball and Two Sticks %f\n', RESNORMMINB2S);

%%%%% AIC&BIC
NB2S = 8;
aicB2S = computeAIC(NB2S, sigma, RESNORMMINB2S);
bicB2S = computeBIC(NB2S, K, sigma, RESNORMMINB2S);

% Compare predictions with actual measurements:
[predictedS] = BallTwoStick(finalPointsBS, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
legend('Data','Model');
titlePlotB2S = sprintf('Measured vs Predicted Values Ball&2Sticks Model\nAIC = %4.4f\nBIC = %4.4f', aicB2S, bicB2S);
title(titlePlotB2S)





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ZEPPELIN & 2 STICKS %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0Z2S = zeros(N,1); % S0
mdZ2S = zeros(N,1);  % d
mf1Z2S = zeros(N,1); % f1
mf2Z2S = zeros(N,1); % f2
mtheta1Z2S = zeros(N,1); % theta1
mphi1Z2S = zeros(N,1);   % phi1
mtheta2Z2S = zeros(N,1); % theta2
mphi2Z2S = zeros(N,1);   % phi2
mthetaZ2S = zeros(N,1);  % theta zep
mphiZ2S = zeros(N,1);  % phi zep
malphaZ2S = zeros(N,1);
mbetaZ2S = zeros(N,1);

% Initial Points
thetaini1 = pi + 2*pi*randn(N,1);
phiini1   = pi + 2*pi*randn(N,1);
thetaini2 = pi + 2*pi*randn(N,1);
phiini2   = pi + 2*pi*randn(N,1);
thetaini = pi + 2*pi*randn(N,1);
phiini   = pi + 2*pi*randn(N,1);
alphaini = 0.5 + 0.5.*randn(N,1);
betaini  = 0.5 + 0.5.*randn(N,1);

startx = [xini diffini fini1 fini2 thetaini1 phiini1 thetaini2 phiini2 thetaini phiini alphaini betaini];

% Constrains
lb = [0,0    ,0,0,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,0,0];
ub = [3,1e-02,1,1, Inf, Inf, Inf, Inf, Inf, Inf,1,1];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMMINZ2S = 1e+30; % high value
RESNORMCounterZ2S = zeros(N,1);
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    % Run the fitting
    try
        % Define a starting point for the non-linear fit
        % Apply inverse transformations
        
        evalFunc = @(x) Zeppelin2StickSSD(x, Avox, bvals, qhat);
%         evalFunc(startx);
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);

        % Remember the RESNORM PARAMETER
        RESNORMCounterZ2S(i) = RESNORM;
        
        % Remember the found params
        mS0Z2S = parameter_hat(1);
        mdZ2S = parameter_hat(2);
        mf1Z2S = parameter_hat(3);
        mf2Z2S = parameter_hat(4);
        mtheta1Z2S = parameter_hat(5);
        mphi1Z2S = parameter_hat(6);
        mtheta2Z2S = parameter_hat(7);
        mphi2Z2S = parameter_hat(8);
        mthetaZ2S = parameter_hat(9);
        mphiZ2S = parameter_hat(10);
        malphaZ2S = parameter_hat(11);
        mbetaZ2S = parameter_hat(12);
        
    catch ME
        warning(ME.message);
        disp(ME.message);

        RESNORM = 1e+30; % high value
        RESNORMCounterZ2S(i) = RESNORM;
        
    end

    if RESNORM < RESNORMMINZ2S
        RESNORMMINZ2S = RESNORM;
        finalPointsZ2S = parameter_hat;
    end

    disp('Zeppelin&2Stick');
    i
    
end

%%%%% AIC&BIC
NZeppelin2Stick = 12;
aicZ2S = computeAIC(NZeppelin2Stick, sigma, RESNORMMINZ2S);
bicZ2S = computeBIC(NZeppelin2Stick, K, sigma, RESNORMMINZ2S);

fprintf('Zeppelin and 2 Stick %f\n', RESNORMMINZ2S);
% Display lambda 1 and lambda 2
% figure, plot(mlambda1ZS,'rs'),hold on, plot(mlambda2ZS,'bo')
% Compare predictions with actual measurements:
[predictedZ2S] = Zeppelin2Stick(finalPointsZ2S, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedZ2S,'rx'); % predictions
legend('Data','Model');

titlePlotZ2S = sprintf('Measured vs Predicted Values Zeppelin&2Sticks\nAIC = %4.4f\nBIC = %4.4f', aicZ2S, bicZ2S);

title(titlePlotZ2S)


%%%%%%%%%%%%% Plotting all of them
figure

subplot(1,2,1)
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
legend('Data','Model');
xlabel('index') % x-axis label
ylabel('S') % y-axis label
title(titlePlotB2S)

subplot(1,2,2)
plot(Avox,'bs'); % measurements
hold on
plot(predictedZ2S,'rx'); % predictions
legend('Data','Model');
xlabel('index') % x-axis label
ylabel('S') % y-axis label
title(titlePlotZ2S)

saveas(gcf,'Q135.png');














% end