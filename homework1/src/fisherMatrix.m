function fisher = fisherMatrix(x, Avox, bvals, qhat, sigma)
% Function that computes the Fisher matrix


% signal for b=0 has to be positive
S0 = x(1); 
% initial diffusivity has to be positive
diff = x(2); 
% fraction of signal contributed by anisotropy has to be [0,1]
f = x(3);
theta = x(4); % theta angle for polar coords
phi = x(5); % phi angle for polar coords
    
%% Synthesize the signals
% Fiber directions (cylinders have rho = 0, so we only get the first column
% of the rotation matrix)
fibDir           = [ cos(phi)*sin(theta) sin(phi)*sin(theta)  cos(theta)] ;
fibDirDerivTheta = [ cos(phi)*cos(theta) sin(phi)*cos(theta) -sin(theta)] ;
fibDirDerivPhi   = [-sin(phi)*sin(theta) cos(phi)*sin(theta)       0    ] ;

% Gradient direction * fiber directions 
% Why sum?
fibDotGrad      = sum(qhat .* repmat(fibDir,           [length(qhat) 1])');
fibDotGradTheta = sum(qhat .* repmat(fibDirDerivTheta, [length(qhat) 1])');
fibDotGradPhi   = sum(qhat .* repmat(fibDirDerivPhi,   [length(qhat) 1])');

SderivS0 = f   * exp(-bvals * diff .* (fibDotGrad .^2)) + ...
         (1-f) * exp(-bvals * diff);
   
Sderivdiff = S0 * ...
    (  f   * exp(-bvals * diff .* (fibDotGrad .^2)) .* (-bvals .* (fibDotGrad .^2)) + ...
     (1-f) * exp(-bvals*diff)                       .* (-bvals));
 
Sderivf = S0 * ( ...
                 exp(-bvals * diff .* (fibDotGrad .^2)) ...
               - exp(-bvals * diff));
    
Sderivtheta = S0 * ( ...
                     f * exp(-bvals * diff .* (fibDotGrad.^2)) ...
                      .* ((-2 * bvals * diff .* fibDotGrad) .* fibDotGradTheta));

Sderivphi = S0 * ( ...
                     f * exp(-bvals * diff .* (fibDotGrad.^2)) ...
                      .* ((-2 * bvals * diff .* fibDotGrad) .* fibDotGradPhi));
                  
derivatives = [SderivS0 ; Sderivdiff ; Sderivf ; Sderivtheta ; Sderivphi];
giveMeDeriv = @(x) derivatives(x,:);
                  
%%%%%%%%%%% FISHER MATRIX
fisher = zeros(5,5);

for i = 1:5
    for j = 1:5
        fisher(i,j) = 1/(sigma^2) * sum(giveMeDeriv(i) .* giveMeDeriv(j));
    end
end


end