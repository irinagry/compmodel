%function Q141()
% Question 1.4.1

% Load dataset
datafile = 'data/challengeOPEN.txt';
fid = fopen(datafile, 'r', 'b');

% Read in the header
A = fgetl(fid);

% Read in the data
A = fscanf(fid, '%f', [8, inf]);
fclose(fid);

% Create the protocol
Avox = A(1,:)';
qhat = A(2:4,:);
G = A(5,:);
delta = A(6,:);
smalldel = A(7,:);
TE = A(8,:);
GAMMA = 2.675987E8;
bvals = ((GAMMA*smalldel.*G).^2).*(delta-smalldel/3);
bvals = bvals.*1e-06;

% Load data from Ball and Stick
savefile = 'Q131';
load(savefile, 'finalPoints');

% Parameters
S0 = finalPoints(1);
diff = finalPoints(2);
f = finalPoints(3);
theta = finalPoints(4);
phi = finalPoints(5);

% Fiber directions (cylinders have rho = 0, so we only get the first column
% of the rotation matrix)
fibDir           = [ cos(phi)*sin(theta) sin(phi)*sin(theta)  cos(theta)] ;
fibDirDerivTheta = [ cos(phi)*cos(theta) sin(phi)*cos(theta) -sin(theta)] ;
fibDirDerivPhi   = [-sin(phi)*sin(theta) cos(phi)*sin(theta)       0    ] ;

% Gradient direction * fiber directions 
fibDotGrad      = sum(qhat .* repmat(fibDir,           [length(qhat) 1])');
fibDotGradTheta = sum(qhat .* repmat(fibDirDerivTheta, [length(qhat) 1])');
fibDotGradPhi   = sum(qhat .* repmat(fibDirDerivPhi,   [length(qhat) 1])');

%%%%%%%%%%%%%%%%%%%%%%%%%%
% The derivatives
SderivS0 =  f   * exp(-bvals * diff .* (fibDotGrad .^2)) + ...
          (1-f) * exp(-bvals * diff);
   
Sderivdiff = S0 * ...
    (  f   * exp(-bvals * diff .* (fibDotGrad .^2)) .* (-bvals .* (fibDotGrad .^2)) + ...
     (1-f) * exp(-bvals*diff)                       .* (-bvals));
 
Sderivf = S0 * ( ...
                 exp(-bvals * diff .* (fibDotGrad .^2)) ...
               - exp(-bvals * diff));
    
Sderivtheta = S0 * ( ...
                     f * exp(-bvals * diff .* (fibDotGrad.^2)) ...
                      .* ((-2 * bvals * diff .* fibDotGrad) .* fibDotGradTheta));

Sderivphi = S0 * ( ...
                     f * exp(-bvals * diff .* (fibDotGrad.^2)) ...
                      .* ((-2 * bvals * diff .* fibDotGrad) .* fibDotGradPhi));

derivatives = [SderivS0 ; Sderivdiff ; Sderivf ; Sderivtheta ; Sderivphi];
giveMeDeriv = @(x) derivatives(x,:);
                  
%%%%%%%%%%% FISHER MATRIX
fisher = zeros(5,5);
sigma = 0.04;

for i = 1:5
    for j = 1:5
        fisher(i,j) = 1/(sigma^2) * sum(giveMeDeriv(i) .* giveMeDeriv(j));
    end
end

fisher








% end