function y = invsin2(x)
    y = asin(sqrt(x));
end