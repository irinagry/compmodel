function aic = computeCorrectedAIC(N, K, sigma, SSD)
% Function that computes AIC

    %%% AIC %%%%% 2 N - 2 log L
    aic = computeAIC(N, sigma, SSD) + (2 * N * (N + 1)) / (K - N - 1);
    
end