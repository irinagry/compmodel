%function Q116()
% Question 1.1.6

%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
% WM  dwis(:,52,62,25);
% GM  dwis(:,66,26,25);
% CSF dwis(:,63,61,20);
Avox = dwis(:,52,62,25);

% Initial Params
N = 100;
xini = 1.1e+05 + 0.7e+05.*randn(N,1); %7.5e+05;
diffini = 3e-03 + 1.5e-03.*randn(N,1); %3e-03;
fini = 2.5e-01 + 1.25e-01.*randn(N,1); %2.5e-01;
thetaini = 0 + 2*pi*randn(N,1); %0;
phiini = 0 + 2*pi*randn(N,1); %0;

% Define multiple starting points
initialPoints = [xini diffini fini thetaini phiini];
% initialPoints = [1.1e+05 3e-03 2.5e-01 0 0];
finalPoints = zeros(1,5);

RESNORMMIN = 1e+30; % high value
RESNORMCounter = zeros(N,1);

% Fmincon parameters
evalFunc = @(x) BallStickSSD(x, Avox, bvals, qhat);
lb = [ 0    , 0   , 0, -Inf, -Inf];
ub = [10e+05, 1e-2, 1,  Inf,  Inf];
A = [];
b = [];
Aeq = [];
beq = [];

tic
for i = 1:N
    % Define various options for the non-linear fitting algorithm
    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

    try
        % Define a starting point for the non-linear fit
        startx = initialPoints(i,:);        
        
        % Run the fitting
        [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);
        
        RESNORMCounter(i) = RESNORM;
    
    catch ME
        warning(ME.message);
        disp(ME.message);
        
        RESNORM = 1e+30; % high value
        RESNORMCounter(i) = RESNORM;
    end

    if RESNORM < RESNORMMIN
        finalPoints = parameter_hat;
    end
end
toc
% What I got
disp('Q116\n');
RESNORMMIN
finalPoints

% Compare predictions with actual measurements:
[predictedS] = BallStick(parameter_hat, bvals, qhat);
figure
title('Measured vs Predicted Values')
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
legend('Data','Model');


%end