function [S] = TensorStick(x, bvals, qhat)
% Function for the Tensor and Stick Model
%
% Input Params:
%  x = initial parameters
%  Avox = measurements from a single voxel from all the 33 image volumes
%  bvals = the b-value array
%  qhat = the gradient directions
% 
% Returns: 
%  sumRes = the sum of the square differences
%  S = the calculated signal
%

% Extract the parameters
S0 = x(1);   % signal for b=0
diff = x(2); % initial diffusivity

f = x(3);     % stick 
theta = x(4); % theta angle for stick 
phi   = x(5); % phi angle for stick 

lambda1 = x(6); % tensor eig 1
theta1 = x(7);  % tensor eig 1 theta angle 
phi1 = x(8);    % tensor eig 1 phi   angle 

lambda2 = x(9);  % tensor eig 2
theta2  = x(10); % tensor eig 2 theta angle 

lambda3 = x(11); % tensor eig 3

%% Synthesize the signals
% Fiber directions (cylinders have rho = 0, so we only get the first column
% of the rotation matrix)
fibDir  = [cos(phi )*sin(theta ) sin(phi )*sin(theta ) cos(theta )]; % stick

fibDir1 = [cos(phi1)*sin(theta1) sin(phi1)*sin(theta1) cos(theta1)]; 
% TODO: get phi2 out of fibDir
fibDir2 = [cos(phi2)*sin(theta2) sin(phi2)*sin(theta2) cos(theta2)];
fibDir3 = [cos(phi3)*sin(theta3) sin(phi3)*sin(theta3) cos(theta3)];

% Gradient direction * fiber directions 
fibDotGrad  = sum(qhat .* repmat(fibDir , [length(qhat) 1])');
fibDotGrad1 = sum(qhat .* repmat(fibDir1, [length(qhat) 1])');
fibDotGrad2 = sum(qhat .* repmat(fibDir2, [length(qhat) 1])');

% alpha = lambda1 - lambda2;
% beta  = lambda2;

% The signal model: anisotropic part + isotropic part
% Why fibDotGrad.^2?
S = S0 * ( f1 * exp(-bvals * diff .* (fibDotGrad1.^2)) + ...
           f2 * exp(-bvals * diff .* (fibDotGrad2.^2))  + ...
      (1 - f1 - f2) * exp(-bvals .* (beta + alpha * (fibDotGrad .^2))));


end