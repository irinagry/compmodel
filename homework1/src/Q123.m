% function Q123()
% % Exercise Q1.2.3

%%%%%%%%%%% Laplace Method


%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
% WM  dwis(:,52,62,25);
% GM  dwis(:,66,26,25);
% CSF dwis(:,63,61,20);
Avox = dwis(:,63,61,20);

%imshow(squeeze(dwis(1,:,:,25)), []);
%return
% Define a starting point for the non-linear fit
startx = [1.5e+05 1e-03 1.5e-01 0 0];
lb = [0      ,0,0,-Inf,-Inf];
ub = [1.0e+10,3,1, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];

% Define various options for the non-linear fitting algorithm
h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
    'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');
evalFunc = @(x) BallStickSSD(x, Avox, bvals, qhat);
%evalFunc(startx)

% Run the fitting and get HESSIAN
[parameter_hat, RESNORM, EXITFLAG, OUTPUT, LAMBDA, GRAD, HESSIAN] = ...
    fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);

S0 = parameter_hat(1);
d = parameter_hat(2);
f = parameter_hat(3);
theta = parameter_hat(4);
phi = parameter_hat(5);

sigma = 6000;
HESSIAN = HESSIAN./(2*sigma^2);
invHESSIAN = pinv(HESSIAN);

stdS0 = sqrt(invHESSIAN(1,1));
stdd = sqrt(invHESSIAN(2,2));
stdf = sqrt(invHESSIAN(3,3));
stdtheta = sqrt(invHESSIAN(4,4));
stdphi = sqrt(invHESSIAN(5,5));



figure,

subplot(1,3,1)
plot([S0-4*stdS0:stdS0/100:S0+4*stdS0], normpdf([S0-4*stdS0:stdS0/100:S0+4*stdS0],S0,stdS0));
hold on
% 2 sigma
scatter(linspace(S0-2*stdS0 , S0+2*stdS0, 50), repmat(3*1e+03, [1 50]), ...
                        '.', 'MarkerEdgeColor', [1 0 0]);
titleS0 = sprintf('S0\n2 sigma: [%f, %f]',S0-2*stdS0,S0+2*stdS0);
title(titleS0)

subplot(1,3,2)
plot([d-4*stdd:stdd/100:d+4*stdd], normpdf([d-4*stdd:stdd/100:d+4*stdd],d,stdd));
hold on
% 2 sigma
scatter(linspace(d-2*stdd , d+2*stdd, 50), repmat(4000, [1 50]), ...
                        '.', 'MarkerEdgeColor', [1 0 0]);
titled = sprintf('d\n2 sigma: [%f, %f]',d-2*stdd,d+2*stdd);
title(titled)

subplot(1,3,3)
plot([f-4*stdf:stdf/100:f+4*stdf], normpdf([f-4*stdf:stdf/100:f+4*stdf],f,stdf));
hold on
% 2 sigma
scatter(linspace(f-2*stdf , f+2*stdf, 50), repmat(10, [1 50]), ...
                        '.', 'MarkerEdgeColor', [1 0 0]);
titlef = sprintf('f\n2 sigma: [%f, %f]',f-2*stdf,f+2*stdf);
title(titlef)


legend('normpdf','2sigma','Location','eastoutside')

% saveas(gcf,'Q123Laplace.png');



%%%%%%%%%%%%%%%% Classical BOOTSTRAP


% Classical Bootstrap
T = 100;

perturbationModel = randi([1,33],33,T);
Avoxperturbed = zeros(33,T);

for i = 1:T
    Avoxperturbed(:,i) = Avox(perturbationModel(:,i));
end

% Values to be maped
mS0 = zeros(1,T);
mDiff = zeros(1,T);
mF = zeros(1,T);

% Initial positions
iniN = 5;
xini = 1.1e+05 + 0.7e+05.*randn(iniN,1); %7.5e+05;
diffini = 3e-03 + 1.5e-03.*randn(iniN,1); %3e-03;
fini = 2.5e-01 + 1.25e-01.*randn(iniN,1); %2.5e-01;
thetaini = 0 + 2*pi*randn(iniN,1); %0;
phiini = 0 + 2*pi*randn(iniN,1); %0;

% Define multiple starting points
initialPoints = [xini diffini fini thetaini phiini];

tic
for i = 1:T

    evalFunc2 = @(x) BallStickSSD(x, Avoxperturbed(:,i), bvals, qhat);
    
    RESNORMMIN = 1e+30; % high value
    
    for j = 1:iniN
        
        try
            [parameter_hat_boot, RESNORM, EXITFLAG, OUTPUT] = ...
                fmincon(evalFunc2, initialPoints(j,:), A, b, Aeq, beq, lb, ub, [], h);
        catch ME
            disp(ME.message);
            RESNORM = 1e+30; % high value
        end
        
        if RESNORM < RESNORMMIN
            
            % In parameter_hat I have the parameters at which my 
            % sum of square differences is minimum

            RESNORMMIN = RESNORM;
            mS0(i) = parameter_hat_boot(1);
            mDiff(i) = parameter_hat_boot(2);
            mF(i) = parameter_hat_boot(3);
            
        end
        
    end
    i
end
toc

% In parameter_hat I have the parameters at which my 
% sum of square differences is minimum

mS0sorted = sort(mS0);
mDiffsorted = sort(mDiff);
mFsorted = sort(mF);

%%%%%%%%%% RANGES
% S0
% 2 sigma range:
S02sigmaL=mean(mS0)-2*std(mS0);
S02sigmaU=mean(mS0)+2*std(mS0);
% 95% range
S095L=mS0sorted(floor(length(mS0)*0.025));
S095U=mS0sorted(floor(length(mS0)*0.975));


% f
% 2 sigma range:
f2sigmaL=mean(mF)-2*std(mF);
f2sigmaU=mean(mF)+2*std(mF);
% 95% range
f95L=mFsorted(floor(length(mF)*0.025));
f95U=mFsorted(floor(length(mF)*0.975));


% d
% 2 sigma range:
d2sigmaL=mean(mDiff)-2*std(mDiff);
d2sigmaU=mean(mDiff)+2*std(mDiff);
% 95% range
d95L=mDiffsorted(floor(length(mDiff)*0.025));
d95U=mDiffsorted(floor(length(mDiff)*0.975));

%%%%%%% FIGURES

figure, 
%% subplot 1
subplot(1,3,1)
histfit(mS0, T/2)%, ylim([0 8])
hold on
% 2 sigma
scatter(linspace(mean(mS0)-2*std(mS0) , mean(mS0)+2*std(mS0),T/2), repmat(5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [1 0 0])
hold on
% 95%
scatter(linspace(mS0sorted(floor(length(mS0)*0.025)) , mS0sorted(floor(length(mS0)*0.975)), T/2), repmat(5.5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [0 1 0])
% legend('hist','gaussfit','2sigma','95%')
titleS0 = sprintf('S0\n2 sigma: [%f, %f]\n95%%: [%f, %f]',S02sigmaL,S02sigmaU,S095L,S095U);
title(titleS0)

% subplot 2
subplot(1,3,2)
histfit(mDiff, T/2)%, ylim([0 7])
hold on
% 2 sigma
scatter(linspace(mean(mDiff)-2*std(mDiff) , mean(mDiff)+2*std(mDiff),T/2), repmat(5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [1 0 0])
hold on
% 95%
scatter(linspace(mDiffsorted(floor(length(mDiff)*0.025)) , mDiffsorted(floor(length(mDiff)*0.975)), T/2), repmat(5.5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [0 1 0])
% legend('hist','gaussfit','2sigma','95%','Location','eastoutside')
titled = sprintf('d\n2 sigma: [%f, %f]\n95%%: [%f, %f]',d2sigmaL,d2sigmaU,d95L,d95U);
title(titled)

% subplot 3
subplot(1,3,3)
histfit(mF, T/2)%, ylim([0 10])
hold on
% 2 sigma
scatter(linspace(mean(mF)-2*std(mF) , mean(mF)+2*std(mF),T/2), repmat(5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [1 0 0])
hold on
% 95%
scatter(linspace(mFsorted(floor(length(mF)*0.025)) , mFsorted(floor(length(mF)*0.975)), T/2), repmat(5.5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [0 1 0])
% legend('hist','gaussfit','2sigma','95%')
titlef = sprintf('f\n2 sigma: [%f, %f]\n95%%: [%f, %f]',f2sigmaL,f2sigmaU,f95L,f95U);
title(titlef)

legend('hist','gaussfit','2sigma','95%','Location','eastoutside')

% saveas(gcf,'Q123classicalbootstrapGM.png');
























% end