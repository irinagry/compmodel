function [parameter_hat, S, sumRes] = DT_SSD(Avox, bvals, qhat)
% Function to compute SSD for DTI

% Number of rows for the Design Matrix
R = length(Avox);

% Design matrix
% ( 1 , -Bk Qkx^2 , -2 Bk QkxQky, 
%     -2 Bk QkxQkz, -Bk Qky^2, 
%     -2 Bk QkyQkz, -Bk Qkz^2 )
G = [ones(1,R) ; ...
    -bvals.*qhat(1,:).^2            ; -2.*bvals.*qhat(1,:).*qhat(2,:) ; ...
    -2.*bvals.*qhat(1,:).*qhat(3,:) ; -bvals.*qhat(2,:).^2            ; ...
    -2.*bvals.*qhat(2,:).*qhat(3,:) ; -bvals.*qhat(3,:).^2];

% Compute x (the unknown model parameters
x = pinv(G)' * log(Avox);

% The unknown parameters
logS0 = x(1);
Dxx = x(2);
Dxy = x(3);
Dxz = x(4);
Dyy = x(5);
Dyz = x(6);
Dzz = x(7);

% The diffusion tensor matrix
D = [Dxx Dxy Dxz ; Dxy Dyy Dyz ; Dxz Dyz Dzz];

% Compute eigenvalues
E = eig(D);
[Evect,Eval] = eig(D);

% Get principal eigenvector:
[Y,I] = max(E);
princEigenVector = Evect(:,I);

% Parameters needed
% S0 is computed from logS0
S0 = exp(logS0);
% Diffusivity computed from MD = Tr(D)/3
d = (Dxx + Dyy + Dzz) / 3;
% Fractional anisotropy compute from FA = ..
f = 3/2 * sqrt((sum((E - repmat(d,3,1)).^2) / sum(E.^2)) );
% f = sqrt(0.5) * sqrt(sum((E - repmat(mean(E),3,1)).^2) / (sum(E.^2)));
% Angles: theta and phi computing primary eigenvector orientation
[theta,phi,r] = cart2sph(princEigenVector(1), princEigenVector(2), princEigenVector(3));


parameter_hat = [S0 d f theta phi];

% The abs is a cheat to add numerical stability
% S = S0*exp(-abs(sum(qhat.*(D*qhat)).*bvals));
% 
% sumRes = sum((Avox - S').^2);
S = exp(G'*x);
sumRes = sum((Avox - S).^2);

end