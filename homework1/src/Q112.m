function Q112()
% Question 1.1.2

%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
Avox = dwis(:,52,62,25);

% Define a starting point for the non-linear fit
%startx = [7.5e+05 3e-03 2.5e-01 0 0];
% Apply inverse transformations
startx = [sqrt(1.1e+05) sqrt(3e-03) (asin(sqrt(2.5e-01))) 0 0];
% startx = [sqrt(1.5e+05) sqrt(3e-03) invsin2(2.5e-01) 0 0];

% Define various options for the non-linear fitting algorithm
h = optimset('MaxFunEvals', 20000, 'Algorithm', 'levenberg-marquardt',...
    'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

% Run the fitting
tic
[parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
    fminunc('BallStickSSD2', startx, h, Avox, bvals, qhat)
toc

% In parameter_hat I have the parameters at which my 
% sum of square differences is minimum
parameter_hat2 = [parameter_hat(1)^2 ...
    parameter_hat(2)^2 ...
    sin2(parameter_hat(3)) ...
    parameter_hat(4) ...
    parameter_hat(5)];
parameter_hat2
RESNORM

% disp('Q112');
% parameter_hat2

% Compare predictions with actual measurements:
[predictedS] = BallStick(parameter_hat2, bvals, qhat);
figure
plot(Avox,'bs'); % measurements
hold on
plot(predictedS,'rx'); % predictions
legend('Data','Model')
title('Measured vs Predicted Values')
xlabel('index') % x-axis label
ylabel('S') % y-axis label
% saveas(gcf,'Q112sm.png');

end
