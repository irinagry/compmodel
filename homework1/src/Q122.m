%function Q122()
% Question 1.2.2

%%% Load data
[dwis qhat bvals] = loadData();

%%% To start with, let's extract the set of 33 measurements
% from one single voxel; we'll take the one near the centre of the image:
% WM  dwis(:,52,62,25);
% GM  dwis(:,66,26,25);
% CSF dwis(:,63,61,20);
Avox = dwis(:,52,62,25);
%imshow(squeeze(dwis(1,:,:,25)), []);
%return
% Define a starting point for the non-linear fit

% Initial positions
iniN = 5;
xini = 1.5e+05 + 0.5e+05.*randn(iniN,1); 
diffini = 1e-03 + 0.25e-03.*randn(iniN,1); 
fini = 2.5e-01 + 1.2e-01.*randn(iniN,1); 
thetaini = 0 + pi*randn(iniN,1); 
phiini = 0 + pi*randn(iniN,1); 

startx = [xini diffini fini thetaini phiini];

lb = [0      ,0,0,-Inf,-Inf];
ub = [1.0e+10,3,1, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];

% Define various options for the non-linear fitting algorithm
h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
    'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');
evalFunc = @(x) BallStickSSD(x, Avox, bvals, qhat);
    
RESNORMMIN = 1e+30; % high value

parameter_hat = zeros(1,5); % Retain the best results

for i = 1:iniN

    try
        [parameter_hat_local, RESNORM, EXITFLAG, OUTPUT] = ...
            fmincon(evalFunc, startx(i,:), A, b, Aeq, beq, lb, ub, [], h);
    catch ME
        disp(ME.message);
        RESNORM = 1e+30; % high value
    end

    if RESNORM < RESNORMMIN

        % In parameter_hat I have the parameters at which my 
        % sum of square differences is minimum

        RESNORMMIN = RESNORM;
        parameter_hat = parameter_hat_local;

    end

end
%parameter_hat

%%%%% MCMC
% Get your starting point scaled
% parameter_hat
x0 = scaleTransformParams(parameter_hat);
%x0 = [7.5e+05 3e-03 2.5e-01 0 0];

%% Gaussians for the scaled parameters
sigma1 = abs(x0(1)/400); %0.5; %1;%8e-01;%0.07;   %S0
sigma2 = abs(x0(2)/400); %5; %0.5; %1;%8e-01;     %f
sigma3 = abs(x0(3)/400); %5; %0.5; %1;%8e-01;     %d
sigma4 = abs(x0(4)/400); %1; %0.5;%pi/10; %1e+01; %theta
sigma5 = abs(x0(5)/400); %1; %0.5;%pi/10; %1e+01; %phi
sigma = [sigma1 sigma2 sigma3 sigma4 sigma5];


% number of runs for the MCMC algorithm
T = 100000; 
Ith = 10;
Burn = T/10;

% Vector of x values
x = zeros(T,5);
x(1,:) = x0(:);

% % Standard Deviation for Gaussian
% stdG = 6000; % do it with residuals thingy
% % Compute Gaussian
% gaussian = @(x,mux) (1/(stdG*sqrt(2*pi)))*exp(-1/2 * (x - mux)^2 * (1/(stdG^2)));
% % Alpha function
% alphaFunc = @(x,y) x/y;


%% Do everything on the transformed parameters
acceptanceRate = 0.0;
alpha = zeros(T+Burn-1,1);

for i = 2:(T+Burn)
%     i
    tmp = x(i-1,:) + sigma.*randn(1,5);
%     x0
%     tmp
    
    for j = 1:Ith
       tmp = tmp + sigma.*randn(1,5);
    end
    
    xc = tmp;
    xp = x(i-1,:);
    
%     x(i-1,:)
%     sigma.*randn(1,5)
%     xc
    
%     while (xc(1) < 0) || (xc(2) < 0) || (xc(2) > 3) || (xc(3) < 0) || (xc(3) > 1)
%         xc = x(i-1,:) + sigma.*randn(1,5);
%     end
    
    [sumResc] = BallStickSSD22(xc, Avox, bvals, qhat);
    [sumResp] = BallStickSSD22(xp, Avox, bvals, qhat);
    
    % Computing ALPHA
    alpha(i,1) = exp((sumResp - sumResc) / (2*(6000^2))); %sumResc/sumResp;
    
    if alpha(i,1) > rand
        x(i,:) = xc(:);
        %xc
        acceptanceRate = acceptanceRate + 1;
    else
        x(i,:) = xp(:);
    end
    
end

finalX = zeros(T,5);
for i = 1:T
    finalX(i,:) = scaleTransformBackParams(x(i + Burn,:));
end

acceptanceRate / (T+Burn)
return
figure 

subplot(2,5,1)
plot(finalX(:,1)), title('S0')
subplot(2,5,6)
plot(cumsum(finalX(:,1))'./(1:length(finalX(:,1))))

subplot(2,5,2)
plot(finalX(:,2)), title('d')
subplot(2,5,7)
plot(cumsum(finalX(:,2))'./(1:length(finalX(:,2))))

subplot(2,5,3)
plot(finalX(:,3)), title('f')
subplot(2,5,8)
plot(cumsum(finalX(:,3))'./(1:length(finalX(:,3))))

subplot(2,5,4)
plot(finalX(:,4)), title('theta')
subplot(2,5,9)
plot(cumsum(finalX(:,4))'./(1:length(finalX(:,4))))

subplot(2,5,5)
plot(finalX(:,5)), title('phi')
subplot(2,5,10)
plot(cumsum(finalX(:,5))'./(1:length(finalX(:,5))))

saveas(gcf,'Q122MCMCconvCSF.png');
% 


figure 

subplot(1,5,1)
histfit(finalX(:,1), T/10), title('S0')

subplot(1,5,2)
histfit(finalX(:,2), T/10), title('d')

subplot(1,5,3)
histfit(finalX(:,3), T/10), title('f')

subplot(1,5,4)
histfit(finalX(:,4), T/10), title('theta')

subplot(1,5,5)
histfit(finalX(:,5), T/10), title('phi')

saveas(gcf,'Q122MCMCallCSF.png');

%%%%%%%%%%%%%%%%%%%%% FIGURES
mS0 = finalX(:,1);
mDiff = finalX(:,2);
mF = finalX(:,3);

mS0sorted = sort(mS0);
mDiffsorted = sort(mDiff);
mFsorted = sort(mF);

%%%%%%%%%% RANGES
% S0
% 2 sigma range:
S02sigmaL=mean(mS0)-2*std(mS0);
S02sigmaU=mean(mS0)+2*std(mS0);
% 95% range
S095L=mS0sorted(floor(length(mS0)*0.025));
S095U=mS0sorted(floor(length(mS0)*0.975));


% f
% 2 sigma range:
f2sigmaL=mean(mF)-2*std(mF);
f2sigmaU=mean(mF)+2*std(mF);
% 95% range
f95L=mFsorted(floor(length(mF)*0.025));
f95U=mFsorted(floor(length(mF)*0.975));


% d
% 2 sigma range:
d2sigmaL=mean(mDiff)-2*std(mDiff);
d2sigmaU=mean(mDiff)+2*std(mDiff);
% 95% range
d95L=mDiffsorted(floor(length(mDiff)*0.025));
d95U=mDiffsorted(floor(length(mDiff)*0.975));

%%%%%%% FIGURES

figure, 
%% subplot 1
subplot(1,3,1)
histfit(mS0, T/2)%, ylim([0 8])
hold on
% 2 sigma
scatter(linspace(mean(mS0)-2*std(mS0) , mean(mS0)+2*std(mS0),T/2), repmat(15,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [1 0 0])
hold on
% 95%
scatter(linspace(mS0sorted(floor(length(mS0)*0.025)) , mS0sorted(floor(length(mS0)*0.975)), T/2), repmat(15.5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [0 1 0])
% legend('hist','gaussfit','2sigma','95%')
titleS0 = sprintf('S0\n2 sigma: [%f, %f]\n95%%: [%f, %f]',S02sigmaL,S02sigmaU,S095L,S095U);
title(titleS0)

% subplot 2
subplot(1,3,2)
histfit(mDiff, T/2)%, ylim([0 7])
hold on
% 2 sigma
scatter(linspace(mean(mDiff)-2*std(mDiff) , mean(mDiff)+2*std(mDiff),T/2), repmat(15,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [1 0 0])
hold on
% 95%
scatter(linspace(mDiffsorted(floor(length(mDiff)*0.025)) , mDiffsorted(floor(length(mDiff)*0.975)), T/2), repmat(15.5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [0 1 0])
% legend('hist','gaussfit','2sigma','95%','Location','eastoutside')
titled = sprintf('d\n2 sigma: [%f, %f]\n95%%: [%f, %f]',d2sigmaL,d2sigmaU,d95L,d95U);
title(titled)

% subplot 3
subplot(1,3,3)
histfit(mF, T/2)%, ylim([0 10])
hold on
% 2 sigma
scatter(linspace(mean(mF)-2*std(mF) , mean(mF)+2*std(mF),T/2), repmat(15,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [1 0 0])
hold on
% 95%
scatter(linspace(mFsorted(floor(length(mF)*0.025)) , mFsorted(floor(length(mF)*0.975)), T/2), repmat(15.5,[1 T/2]), ...
                        '.', 'MarkerEdgeColor', [0 1 0])
% legend('hist','gaussfit','2sigma','95%')
titlef = sprintf('f\n2 sigma: [%f, %f]\n95%%: [%f, %f]',f2sigmaL,f2sigmaU,f95L,f95U);
title(titlef)

legend('hist','gaussfit','2sigma','95%','Location','eastoutside')

saveas(gcf,'Q122MCMCCSF.png');



%end