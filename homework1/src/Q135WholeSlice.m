% function Q135WholeSlice()
% Question 1.3.5

%%% Load data
[dwis qhat bvals] = loadData();

% Initial Params
N = 5;
XMAX = 112;
YMAX = 112;

S = zeros(33,112,112);


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% BALL & 2 STICKS %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0B2S = zeros(XMAX,YMAX);
mdB2S = zeros(XMAX,YMAX);
mf1B2S = zeros(XMAX,YMAX);
mf2B2S = zeros(XMAX,YMAX);
mtheta1B2S = zeros(XMAX,YMAX);
mphi1B2S = zeros(XMAX,YMAX);
mtheta2B2S = zeros(XMAX,YMAX);
mphi2B2S = zeros(XMAX,YMAX);
mResnormB2S = zeros(XMAX,YMAX);

RESNORMCounterBS = zeros(N,XMAX,YMAX);

% Constraints
lb = [0     ,0    ,0,0,-Inf,-Inf,-Inf,-Inf];
ub = [10e+05,1e-02,1,1, Inf, Inf, Inf, Inf];
A = [];
b = [];
Aeq = [];
beq = [];
% 
% tic
% for x = 1:XMAX
%     for y = 1:YMAX
%         
%         Avox = dwis(:,x,y,25);
%         
%         % Function to call only with one parameter
%         evalFunc = @(x) BallTwoStickSSD(x, Avox, bvals, qhat);
%         
%         RESNORMMIN = 1e+30; % high value
%         
%         % Apply DTI to get good initial points
%         try
%             [startx] = DT_SSD(Avox, bvals, qhat);
%         catch ME
%             warning(ME.message);
% %             disp(ME.message);
%             startx = [1.5e+05 1e-03 2.5e-01 0 0];
%         end
%         
%         % Add gaussians around initial points
%         xini = startx(1) + (startx(1)/2).*randn(N,1); %7.5e+05;
%         diffini = startx(2) + (startx(2)/2).*randn(N,1); %3e-03;
%         fini1 = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
%         fini2 = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
%         thetaini1 = startx(4) + (startx(4)/2).*randn(N,1); %0;
%         phiini1 = startx(5) + (startx(5)/2).*randn(N,1); %0;
%         thetaini2 = startx(4) + (startx(4)/2).*randn(N,1); %0;
%         phiini2 = startx(5) + (startx(5)/2).*randn(N,1); %0;
%                 
%         % Initial Points
%         initialPoints = [xini diffini fini1 fini2 thetaini1 phiini1 thetaini2 phiini2];
%                 
%         for i = 1:N
%             % Define various options for the non-linear fitting algorithm
%             h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
%                 'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off'); %'iter');
% 
%             % Run the fitting
%             try
%                               
%                 [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
%                     fmincon(evalFunc, initialPoints(i,:), A, b, Aeq, beq, lb, ub, [], h);
%                 
%                 RESNORMCounterBS(i,x,y) = RESNORM;
%                 
%             catch ME
%                 warning(ME.message);
% %                 disp(ME.message);
%                 
%                 RESNORM = 1e+30; % high value
%                 RESNORMCounterBS(i,x,y) = RESNORM;
%             end
%             
%             % Remember the best fit
%             if RESNORM < RESNORMMIN
%                 
%                 RESNORMMIN = RESNORM;
%                 
%                 mS0B2S(x,y) = parameter_hat(1);
%                 mdB2S(x,y) = parameter_hat(2);
%                 mf1B2S(x,y) = parameter_hat(3);
%                 mf2B2S(x,y) = parameter_hat(4);
%                 mtheta1B2S(x,y) = parameter_hat(5);
%                 mphi1B2S(x,y) = parameter_hat(6);
%                 mtheta2B2S(x,y) = parameter_hat(7);
%                 mphi2B2S(x,y) = parameter_hat(8);
%                 mResnormB2S(x,y) = RESNORM;
%                 
%             end
%         end
%         
%     end
%     disp('Ball&TwoSticks');
%     x
% end
% toc
% % 
% % savefile = 'Q135_wholeslice_b2s';
% % save(savefile, 'mS0B2S', 'mdB2S', 'mf1B2S', 'mf2B2S', 'mtheta1B2S', 'mphi1B2S', 'mtheta2B2S', 'mphi2B2S', 'mResnormB2S');
% 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ZEPPELIN & 2 STICKS %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maps of parameters:
mS0Z2S = zeros(XMAX,YMAX); % S0
mdZ2S = zeros(XMAX,YMAX); % d
mf1Z2S = zeros(XMAX,YMAX); % f1
mf2Z2S = zeros(XMAX,YMAX); % f2
mtheta1Z2S = zeros(XMAX,YMAX); % theta1
mphi1Z2S = zeros(XMAX,YMAX);   % phi1
mtheta2Z2S = zeros(XMAX,YMAX); % theta2
mphi2Z2S = zeros(XMAX,YMAX);  % phi2
mthetaZ2S = zeros(XMAX,YMAX);  % theta zep
mphiZ2S = zeros(XMAX,YMAX);  % phi zep
malphaZ2S = zeros(XMAX,YMAX);
mbetaZ2S = zeros(XMAX,YMAX);
mResnormZ2S = zeros(XMAX,YMAX);

% Constrains
lb = [0     ,0    ,0,0,-Inf,-Inf,-Inf,-Inf,-Inf,-Inf,0,0];
ub = [10e+05,1e-02,1,1, Inf, Inf, Inf, Inf, Inf, Inf,1,1];
A = [];
b = [];
Aeq = [];
beq = [];

RESNORMMINZ2S = 1e+30; % high value
RESNORMCounterZ2S = zeros(N,XMAX,YMAX);

N = 1;
tic
for x = 1:XMAX
    for y = 1:YMAX
        
        Avox = dwis(:,x,y,25);
        
        % Function to call only with one parameter
        evalFunc = @(x) Zeppelin2StickSSD(x, Avox, bvals, qhat);
        
        RESNORMMIN = 1e+30; % high value
        
        % Apply DTI to get good initial points
        try
            [startx] = DT_SSD(Avox, bvals, qhat);
        catch ME
            warning(ME.message);
%             disp(ME.message);
            startx = [1.5e+05 1e-03 2.5e-01 0 0];
        end
        
        % Initial Points
        xini = startx(1) + (startx(1)/2).*randn(N,1);
        diffini = startx(2) + (startx(2)/2).*randn(N,1); %3e-03;
        fini1 = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
        fini2 = startx(3) + (startx(3)/2).*randn(N,1); %2.5e-01;
        thetaini1 = startx(4) + (startx(4)/2).*randn(N,1); %0;
        phiini1 = startx(5) + (startx(5)/2).*randn(N,1); %0;
        thetaini2 = startx(4) + (startx(4)/2).*randn(N,1); %0;
        phiini2 = startx(5) + (startx(5)/2).*randn(N,1); %0;
        thetaini = startx(4) + (startx(4)/2).*randn(N,1); %0;
        phiini = startx(5) + (startx(5)/2).*randn(N,1); %0;
        alphaini = 0.5 + 0.5.*randn(N,1);
        betaini  = 0.5 + 0.5.*randn(N,1);

%         initialPoints = [xini diffini fini1 fini2 thetaini1 phiini1 thetaini2 phiini2 thetaini phiini alphaini betaini];
        initialPoints = [1.5e+05 1e-03 2.5e-01 2.5e-01 0 0 0 0 0 0 0.5 0.5];
        
                
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off'); %'iter');

            % Run the fitting
            try
                              
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFunc, initialPoints(i,:), A, b, Aeq, beq, lb, ub, [], h);
                
                RESNORMCounterZ2S(i,x,y) = RESNORM;
                
            catch ME
                warning(ME.message);
%                 disp(ME.message);
                
                RESNORM = 1e+30; % high value
                RESNORMCounterZ2S(i,x,y) = RESNORM;
            end
            
            % Remember the best fit
            if RESNORM < RESNORMMIN
                
                RESNORMMIN = RESNORM;
                
                % Remember the found params
                mS0Z2S(x,y) = parameter_hat(1);
                mdZ2S(x,y) = parameter_hat(2);
                mf1Z2S(x,y) = parameter_hat(3);
                mf2Z2S(x,y) = parameter_hat(4);
                mtheta1Z2S(x,y) = parameter_hat(5);
                mphi1Z2S(x,y) = parameter_hat(6);
                mtheta2Z2S(x,y) = parameter_hat(7);
                mphi2Z2S(x,y) = parameter_hat(8);
                mthetaZ2S(x,y) = parameter_hat(9);
                mphiZ2S(x,y) = parameter_hat(10);
                malphaZ2S(x,y) = parameter_hat(11);
                mbetaZ2S(x,y) = parameter_hat(12);
                
                mResnormZ2S(x,y) = RESNORM;
                
            end
        end
        
    end
    x
end
toc


% savefile = 'Q135_wholeslice_z2s';
% save(savefile, 'mS0Z2S', 'mdZ2S', 'mf1Z2S', 'mf2Z2S', 'mtheta1Z2S', 'mphi1Z2S', 'mtheta2Z2S', 'mphi2Z2S', 'mthetaZ2S', 'mphiZ2S', 'malphaZ2S', 'mbetaZ2S', 'mResnormZ2S');


% figure,
% subplot(2,2,1)
% imshow(mS0B2S, []), title('S0 map (Ball2Stick)'), colormap hot, colorbar;
% subplot(2,2,2)
% imshow(mdB2S, []), title('d map (Ball2Stick)'), colormap hot, colorbar;
% subplot(2,2,3)
% imshow(mf1B2S, []), title('f map (Ball2Stick)'), colormap hot, colorbar;
% subplot(2,2,4)
% imshow(mResnormB2S, []), title('RESNORM map (Ball2Stick)'), colormap hot, colorbar;
% saveas(gcf,'Q136mapsB2S.png');

% figure,
% subplot(2,2,1)
% imshow(mS0Z2S, []), title('S0 map (Zeppelin2Stick)'), colormap hot, colorbar;
% subplot(2,2,2)
% mdZ2S(mdZ2S > 3e-03) = 3e-03;
% imshow(mdZ2S, []), title('d map (Zeppelin2Stick)'), colormap hot, colorbar;
% subplot(2,2,3)
% imshow(mf1Z2S + mf2Z2S, []), title('f map (Zeppelin2Stick)'), colormap hot, colorbar;
% subplot(2,2,4)
% imshow(mResnormZ2S, []), title('RESNORM map (Zeppelin2Stick)'), colormap hot, colorbar;
% saveas(gcf,'Q136mapsZ2S.png');





% end