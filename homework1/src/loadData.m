function [dwis qhat bvals] = loadData()
% Function that loads data
%
% Returns: 
%  dwis = diffusion weighted data
%  qhat = gradient directions
%  bvals = the b-values array
%

%%% Open data file
fid = fopen('../data/dwi.Bfloat', 'r', 'b');
dwis = fread(fid, 'float');
fclose(fid);

%%% Reshape the data array to reflect the image dimensions:
% each of the 33 image volumes has 50 slices with 112x112 voxels.
% The voxel dimensions are 2x2x2 mm^3.

dwis = reshape(dwis, 33, 112, 112, 50);
size(dwis);

%%% Display a single slice
% Middle slice of the first image volume, which has b=0
%figure, imshow(squeeze(dwis(1,:,:,25)), []);
% Middle slice of the second image volume, which has b=1000
%figure, imshow(squeeze(dwis(2,:,:,25)), []);
    
%%% Load gradient directions
qhat = load('../data/grad_dirs.txt')';
% The b-value array:
bvals = 1000*sum(qhat.*qhat);

end