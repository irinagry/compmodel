% function worksheet1()

%%%%%%%%%%%%%%%%%%%%%%%%%% TASK 1

%%%% TASK 1.1 - plot variable space and subject space
data = [5 3 ; 7 2 ; 4 6];
figure
% Plot variable space
subplot(2,2,1)
plot([repmat(0,[3,1]) data(:,1)]', [repmat(0,[3,1]) data(:,2)]'), grid on, title('Variable Space')
xlim([0,10]), ylim([0,10])
% Plot subject space
subplot(2,2,2)
plot3([[0 0] ; data(1,:)], [[0 0] ; data(2,:)], [[0 0] ; data(3,:)]), grid on, title('Subject Space')
xlabel('Person 1'), ylabel('Person 2'), zlabel('Person 3')
xlim([0,10]), ylim([0,10]), zlim([0,10])


%%%% TASK 1.2 - mean scores for each part of the exam
mean1 = mean(data(:,1));
mean2 = mean(data(:,2));
dataDemeaned(:,1) = data(:,1) - repmat(mean1 , [3,1]);
dataDemeaned(:,2) = data(:,2) - repmat(mean2 , [3,1]);


%%%% TASK 1.3 - plot variable space and subject space
% Plot variable space
subplot(2,2,3)
plot([repmat(0,[3,1]) dataDemeaned(:,1)]', [repmat(0,[3,1]) dataDemeaned(:,2)]'), 
grid on, title('Variable Space (demeaned)')
%xlim([0,10]), ylim([0,10])
% Plot subject space
subplot(2,2,4)
plot3([[0 0] ; dataDemeaned(1,:)], [[0 0] ; dataDemeaned(2,:)], [[0 0] ; dataDemeaned(3,:)]), 
grid on, title('Subject Space (demeaned)')
xlabel('Person 1'), ylabel('Person 2'), zlabel('Person 3')
% xlim([0,10]), ylim([0,10]), zlim([0,10])


%%%% TASK 1.4 - compute length of vectors
len1 = norm(dataDemeaned(:,1),2);
len2 = norm(dataDemeaned(:,2),2);
sd1 = std(dataDemeaned(:,1));
sd2 = std(dataDemeaned(:,2));
fprintf('Length of the vector %f and sdev %f Sx*sqrt(N-1) = %f\n', len1, sd1, sqrt(2)*sd1);
fprintf('Length of the vector %f and sdev %f Sx*sqrt(N-1) = %f\n', len2, sd2, sqrt(2)*sd2);


%%%% TASK 1.5 - cosine of the angle between the vectors
cosine = dot(dataDemeaned(:,1), dataDemeaned(:,2)) / (norm(dataDemeaned(:,1)) * norm(dataDemeaned(:,2)));
corCoef = corrcoef( dataDemeaned(:,1), dataDemeaned(:,2));
fprintf('acos(cos(x,y)) = %f, corrcoef(x,y) = %f \n', ...
                 acos(cosine)*180/pi, ...
                 acos(corCoef(1,2))*180/pi );

% |x|*|y| = x y cos 
% cos = (x'*y)/(norm(x)*norm(y))



%%%%%%%%%%%%%%%%%%%%%%%%%% TASK 2
X = [10 5 1 6 7 3 4 5 1 8];
Y = [ 2 4 4 2 4 5 4 5 6 4];

%%%% TASK 2.1 - compute means, stddev, correlations
meanX = mean(X); stdX = std(X); 
meanY = mean(Y); stdY = std(Y);
corrXY = dot(X,Y) / (norm(X) * norm(Y));
figure
subplot(2,1,1), scatter(X,Y), grid on, xlim([-10,10]), ylim([-10,10]);


%%%% TASK 2.2
Xdemean = X - meanX; stdXdemean  = std(Xdemean);
Ydemean = Y - meanY; stdYdemean  = std(Ydemean);
subplot(2,1,2), scatter(Xdemean,Ydemean), grid on, xlim([-10,10]), ylim([-10,10]);


%%%% TASK 2.3
lenX = norm(Xdemean); lenY = norm(Ydemean);
corCoefMatrix = corrcoef(Xdemean, Ydemean);
XYsimilarity = acos(corCoefMatrix(2,1))*180/pi;


%%%% TASK 2.4
beta = [repmat(1, [length(X),1])  Xdemean'] \ Ydemean';
Ypredicted = [repmat(1, [length(X),1])  Xdemean']*beta


%%%% TASK 2.5
figure, plot(Xdemean, Ydemean, 'ro', Xdemean, Ypredicted, 'b-'), title('Regression Model'), grid on
errors = Ydemean' - Ypredicted;
hold on
errorbar(Xdemean, Ypredicted, errors);


%%%% TASK 2.6













% end