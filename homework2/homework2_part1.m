%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GRIGORESCU IRINA CMBI HOMEWORK 2 %
%              PART 1              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Part 1 - a
% Simulate sampled data from 2 groups with different means but the same
% variance
sample_size = 25;
sample1 = 1.0 + 0.25*randn(sample_size,1);
sample2 = 1.5 + 0.25*randn(sample_size,1);

e_gt = [sample1 - 1.0 ; sample2 - 1.5];

mean1 = mean(sample1);
mean2 = mean(sample2);
std1  = std(sample1);
std2  = std(sample2);

title1 = sprintf('Group 1: mean = %f std = %f \n', mean1, std1);
title2 = sprintf('Group 2: mean = %f std = %f \n', mean2, std2);

% figure
% subplot(2,1,1), histfit(sample1,100), title(title1), xlabel('value'), ylabel('frequency'); 
% subplot(2,1,2), histfit(sample2,100), title(title2), xlabel('value'), ylabel('frequency');

%% Part 1 - b
% T-statistic on the samples
[Hy,P,CI,STATS] = ttest2(sample1, sample2);
fprintf('T-statistic on the samples: H = %.5f, P = %.5f, TSTAT = %.5f\n', Hy, P, STATS.tstat);
% T-statistic on the demeaned samples
%sampledem1 = sample1 - mean1;
%sampledem2 = sample2 - mean2;
%[Htemp,Ptemp,CItemp,STATStemp] = ttest2(sampledem1, sampledem2);
%fprintf('T-statistic on the demeaned samples: H = %.5f, P = %.5f\n', Htemp, Ptemp);

%% Part 1 - c
Y = [sample1 ; sample2];
X = [ones(size(sample1))  , zeros(size(sample1)) ; ...
     zeros(size(sample1)) , ones(size(sample1))] ;

% i.   Dimension of the column space
CX = rank(X);

% ii.  Projection matrix
Px = X*(pinv(X'*X))*X';
trace(Px); % dimension of C(X) space

% iii. Deetermine Yhat
Yhat = Px*Y;

% iv.  Compute Rx
Rx = eye(length(Px)) - Px;
trace(Rx); % trace(Rx) = 48 = dim(Y) - dim(X)
corrcoef(Rx,Px);  % Rx orthogonal to Px

% v.   Determine ehat
ehat = Rx*Y;
rank(Rx); % 48 (== dimension of Cxp (error space))

% vi.  Angle between ehat and Yhat
correY = acos( dot(ehat,Yhat) / (norm(ehat) * norm(Yhat)) ) *180/pi; % 90 degrees
% corrcoef(ehat,Yhat) % 0 correlation between the two 

% vii. Determine bhat 
bhat = X \ Yhat;
% bhat = X'*pinv(X*X')*Y; % 

% viii.Estimate the variance of the stochastic component ehat
varehat = (ehat'*ehat) / (length(Y) - rank(X));
stdehat = sqrt(varehat); % similar to the standard deviation we used for the samples

% ix.  Covariance matrix for the estimated model parameters 
% (beta1 and beta2 are not correlated)
Sbetahat = varehat * pinv(X'*X); % they are not correlated 

% x.   Lambda = [1 -1]
lambda = [1 -1]';
H = pinv(X') * lambda; % (Poline)

% xi.  Use X0 to compute the additional error as a result of placing the
% constraint, then estimate the f-statistic
Ph = H*(pinv(H'*H))*H'; % projection onto H space
X0 = ones(50,1);        % reduced model
rank(X0);               % rank(reduced model) == 1 and rank(X = initial model) == 2
%Px == (Px0 + Ph)       % it is true

Px0 = Px - Ph;          % projection onto the space of the reduced model
Ychat  = Px0 * Yhat;    % Y predicted constrained 
rank(X);              % == 2
rank(Ychat);          % == 1
echat = Y - Ychat;   % error predicted constrained = Y - Yhat
% ehat
eadd = echat - ehat; % additional error 
% the additional error is also: [repmat(mean1,[25,1]) ; repmat(mean2,[25,1])] - bhat0

% f-statistic
v1 = trace(Px - Px0); %  1
v2 = trace(Rx);       % 48   % (eye(length(Px)) - Px);
fstat = ( (Y'*(eye(length(Px0)) - Px0)*Y - Y'*(eye(length(Px))-Px)*Y)/v1) / ...
          ((Y'*(eye(length(Px))-Px)*Y)/v2);

% xii. t-statistic
tstatc = (lambda' * bhat) / sqrt(lambda' * Sbetahat * lambda);
fprintf('Tstat calculated with formula %.5f and from matlab function %.5f\n', tstatc, STATS.tstat);  % it is

bhat0 = X0'*pinv(X0*X0')*Ychat;
size(Ychat);

% xiii on paper

% xiv
proj_gt = Px * e_gt;

% xv
proj_gt2 = Rx * e_gt;

%% Part 1 - d) Compute the t-statistic with a different model
Yd = [sample1 ; sample2];
Xd = [ones(size(sample1))  , zeros(size(sample1)) ; ...
     zeros(size(sample1)) , ones(size(sample1))] ;
[Hd,Pd,CId,STATSd] = ttest2(sample1, sample2);

% i)  Write down the design matrix X and determine the dimension of its column space dim(X)
Xd = [ ones(length(sample1) + length(sample2), 1) , Xd ];
CXd = rank(Xd);

% ii)  Projections Matrices
Pxd = Xd*(pinv(Xd'*Xd))*Xd'; % the same
trace(Pxd); % dimension of C(X) space the same as c)

Rxd = eye(length(Pxd)) - Pxd; 
trace(Rxd); % trace(Rx) = 48 = dim(Y) - dim(X)
corrcoef(Rxd,Pxd);  % Rx orthogonal to Px - demonstrates that Rx is also a perpendicular projection operator

Yhatd = Pxd*Yd;
ehatd = Rxd*Yd;
bhatd = Xd'*pinv(Xd*Xd')*Yd;
varehatd = (ehatd'*ehatd) / (length(Yd) - rank(Xd));
Sbetahatd = varehatd * pinv(Xd'*Xd);

% iii) Determine the appropriate contrast vector λ and the reduced
%model corresponding to the constraint λ'β = 0
lambdad = [0 1 -1]';

Hd = pinv(Xd') * lambdad;
Phd = Hd*(pinv(Hd'*Hd))*Hd'; % projection onto H space
X0d = ones(50,2);            % reduced model
rank(X0d);                   % rank(reduced model) == 1 and rank(X = initial model) == 2

% iv) t-statistic
Px0d = Pxd - Phd;        % projection onto the space of the reduced model
Ychatd  = Px0d * Yhatd;  % Y predicted constrained 
echatd = Yd - Ychatd;    % error predicted constrained = Y - Yhat

tstatcd = (lambdad' * bhatd) / sqrt(lambdad' * Sbetahatd * lambdad);
fprintf('Tstat calculated with formula %.5f and from matlab function %.5f\n', tstatcd, STATSd.tstat);  % it is the same as above


%% Part 1 - e) Compute the t-statistic with yet another model
Ye = [sample1 ; sample2];
Xe = [ones(size(sample1)) ; zeros(size(sample1))];

[He,Pe,CIe,STATSe] = ttest2(sample1, sample2);

% i)  Write down the design matrix X and determine the dimension of its column space dim(X)
Xe = [ ones(length(sample1) + length(sample2), 1) , Xe ];
CXe = rank(Xe);

Pxe = Xe*(pinv(Xe'*Xe))*Xe';
trace(Pxe); % dimension of C(X) space

Rxe = eye(length(Pxe)) - Pxe; 
trace(Rxe); % trace(Rx) = 48 = dim(Y) - dim(X)
corrcoef(Rxe,Pxe);  % Rx orthogonal to Px - demonstrates that Rx is also a perpendicular projection operator

Yhate = Pxe*Ye;
ehate = Rxe*Ye;
bhate = Xe'*pinv(Xe*Xe')*Ye;
varehate = (ehate'*ehate) / (length(Ye) - rank(Xe));
Sbetahate = varehate * pinv(Xe'*Xe);

% ii) Determine the appropriate contrast vector
lambdae = [0 1]';
He = pinv(Xe') * lambdae;
Phe = He*(pinv(He'*He))*He';  % projection onto H space
X0e = ones(50,1);             % reduced model
rank(X0e);                    % rank(reduced model) == 1 and rank(X = initial model) == 2

% iii) Determine the t-statistic.
Px0e   = Pxe - Phe;        % projection onto the space of the reduced model
Ychate = Px0e * Yhate;     % Y predicted constrained 
echate = Ye - Ychate;      % error predicted constrained = Y - Yhat

tstatce = (lambdae' * bhate) / sqrt(lambdae' * Sbetahate * lambdae);
fprintf('Tstat calculated with formula %.5f and from matlab function %.5f\n', tstatce, STATSe.tstat);  % it is

%% Part 1 - f) 
% No

%% Part 2 - Paired t-test for comparing the means from repeat measurements from the
%same group of subjects.

%% a) Treat the simulated data from above as the repeat observations from the same group of subjects
[H2,P2,CI2,STATS2] = ttest(sample1, sample2);
fprintf('\n\nPart 2: T-statistic on the samples: H = %.5f, P = %.5f, TSTAT = %.5f\n', H2, P2, STATS2.tstat);

% b) Compute the same statistic with the GLM model given in the coursework
Y = [sample1 ; sample2];

% i) Determine the design matrix X and its rank.
X = [ones(size(sample1)) ; zeros(size(sample1))];
X = [ ones(length(sample1) + length(sample2), 1) , X ];

Si = [eye(sample_size) ; eye(sample_size)];
X = [X , Si];

CX = rank(X); % CX == 26

Px = X*(pinv(X'*X))*X';
trace(Px); % dimension of C(X) space

Rx = eye(length(Px)) - Px; 
trace(Rx);        % trace(Rx) = 24 = dim(Y) - rank(X)
corrcoef(Rx,Px);  % Rx orthogonal to Px - demonstrates that Rx is also a perpendicular projection operator

Yhat = Px*Y;
ehat = Rx*Y;
bhat = X'*pinv(X*X')*Y;
varehat = (ehat'*ehat) / (length(Y) - rank(X));
Sbetahat = varehat * pinv(X'*X);

% ii) Write down the appropriate contrast vector 
lambda = [0 1 zeros(1,25)]';

H = pinv(X') * lambda;
Ph = H*(pinv(H'*H))*H';  % projection onto H space
X0 = ones(length(sample1) + length(sample2), 1);
X0 = [X0, Si];           % reduced model
rank(X0);                % rank(reduced model) == 25 and rank(X = initial model) == 26

% iii) Compute the t-statistic
Px0   = Px - Ph;        % projection onto the space of the reduced model
Ychat = Px0 * Yhat;     % Y predicted constrained 
echat = Y - Ychat;      % error predicted constrained = Y - Yhat

tstatc = (lambda' * bhat) / sqrt(lambda' * Sbetahat * lambda);
fprintf('Part 2: Tstat calculated with formula %.5f and from matlab function %.5f\n', tstatc, STATS2.tstat);  % it is


