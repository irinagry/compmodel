%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GRIGORESCU IRINA CMBI HOMEWORK 2 %
%             PART 2.1             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1
sample_size1 = 6;
sample_size2 = 8;
sample1 = 1.0 + 0.25*randn(sample_size1,1);
sample2 = 1.5 + 0.25*randn(sample_size2,1);

mean1 = mean(sample1);
mean2 = mean(sample2);
std1  = std(sample1);
std2  = std(sample2);

title1 = sprintf('Sample 1 (%.0f): mean = %f std = %f \n', sample_size1, mean1, std1);
title2 = sprintf('Sample 2 (%.0f): mean = %f std = %f \n', sample_size2, mean2, std2);

%% 1a T-statistic on the samples
[H,P,CI,STATS] = ttest2(sample1, sample2);
fprintf('T-statistic on the samples: H = %.5f, P = %.5f, T-stat = %.5f\n', H, P, STATS.tstat);

%% 1b
% i)  Construct the 1-dimensional D array
D = [sample1 ; sample2]';
size(D); % = 14 = 6 + 8

% ii) Construct all permutations of D
Dperm  = combnk((1:14),6);
permNo = length(Dperm);

Dperm = [Dperm , zeros(permNo, 8)];

for i = 1:permNo
    Dperm(i,7:14) = setdiff((1:14), Dperm(i,:));
    Dperm(i,:) = D(1,Dperm(i,:));
end


%% iii) compute the t-statistics for all the membership 
%       permutations to construct the empirical distribution of the t-statistic.
tstatDperm = zeros(permNo,1);
for i = 1:permNo
    [Hh,Pp,CIci,STATSs] = ttest2(Dperm(i,1:6), Dperm(i,7:14));
    tstatDperm(i) = STATSs.tstat;
end

% figure, histfit((tstatDperm),100), title('Histogram of t-statistic values on all membership permutations')
% xlabel('t-statistic');ylabel('frequency')

p = 0;
for i = 1:permNo
    if (abs(tstatDperm(i)) - abs(STATS.tstat)) > 1e-20
        p = p + 1;
    end
end

fprintf('Calculated p-value = %.5f n(p) = %3d permNo = %d\n', p/permNo, p, permNo);

%% 1c) Repeat b) but rather than using the t-statistic, use the difference between the means as the test statistic
tstatDpermAVG = zeros(permNo,1);
for i = 1:permNo
    tstatDpermAVG(i) = mean(Dperm(i,1:6)) - mean(Dperm(i,7:14));
end

pavg = 0; 
meanSamples = mean1 - mean2;
for i = 1:permNo
    if (abs(tstatDpermAVG(i)) - abs(meanSamples)) > 1e-20
        pavg = pavg + 1;
    end
end
fprintf('Calculated p-value = %.5f n(p) = %3d permNo = %d (difference between the means)\n', pavg/permNo, pavg, permNo);
return
%% 1d) 
N = 1000;
tstatPermN = zeros(N, 1);
pN = 0;
for i = 1:N
    indexPerm = randperm(14);
    Dpermuted = D(indexPerm);
    [Hhp,Ppp,CIcip,STATSsp] = ttest2(Dpermuted(1:6), Dpermuted(7:14));
    tstatPermN(i) = STATSsp.tstat;
    
    if (abs(tstatPermN(i)) - abs(STATS.tstat)) > 1e-20
        pN = pN + 1;
    end
    
end
fprintf('Calculated p-value = %.5f n(p) = %3d permNo = %d (N permutations)\n', pN/N, pN, N);
sum(length(unique(tstatPermN,'rows'))); % not all are unique


