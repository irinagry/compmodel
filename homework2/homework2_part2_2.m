%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GRIGORESCU IRINA CMBI HOMEWORK 2 %
%             PART 2.2             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Reading Data
% WM Mask
fid  = fopen('glm/wm_mask.img', 'r', 'l'); % little-endian
data = fread(fid, 'float'); % 16-bit floating point
data = reshape(data, [40 40 40]); % dimension 40x40x40
wmmask = data;

% Group 1
group1 = zeros(8, 40, 40, 40);
for i = 1:8
    filename = sprintf('glm/CPA%d_diffeo_fa.img', (i+3));
    fid  = fopen(filename, 'r', 'l'); % little-endian
    data = fread(fid, 'float'); % 16-bit floating point
    data = reshape(data, [40 40 40]); % dimension 40x40x40
    group1(i,:,:,:) = data.*wmmask;
end

% Group 2
group2 = zeros(8, 40, 40, 40);
indexg2 = [3 6 9 10 13 14 15 16]; 
for i = 1:8
    filename = sprintf('glm/PPA%d_diffeo_fa.img', indexg2(i));
    fid  = fopen(filename, 'r', 'l'); % little-endian
    data = fread(fid, 'float'); % 16-bit floating point
    data = reshape(data, [40 40 40]); % dimension 40x40x40
    group2(i,:,:,:) = data.*wmmask;
end



%% a) Compute t-statistics for each voxel in the ROI
X = [ ones(8,1) , zeros(8,1)  ; ...
     zeros(8,1) ,  ones(8,1)] ;
Px = X*(pinv(X'*X))*X';
Rx = eye(length(Px)) - Px;
lambda = [1 -1]';
pinvXX = pinv(X'*X);

tstatROI = zeros(40,40,40);
tic
for i = 1:40
    for j = 1:40
        for k = 1:40
            
            if wmmask(i,j,k) == 0
                continue
            end
                
            Y = [group1(:,i,j,k) ; group2(:,i,j,k)]; %16x1
            Yhat = Px*Y;
            bhat = X \ Yhat;
            ehat = Rx*Y;
            varehat = (ehat'*ehat) / 14; %(length(Y) - rank(X));            
            Sbetahat = varehat * pinvXX; % pinv(X'*X);
            
            tstatROI(i,j,k) = (lambda' * bhat) / sqrt(lambda' * Sbetahat * lambda);
            
        end
    end
end
toc

tstatVoxels = max(max(max(tstatROI)));
fprintf('The maximum t-statistic among all the voxels: %.5f\n', tstatVoxels);


%% b) Use the same strategy as in 1-(b) to determine all the possible permutations of group labels
% Constructing the indices
Dperm  = combnk((1:16), 8);
permNo = length(Dperm);

Dperm = [Dperm , zeros(permNo, 8)];
for i = 1:permNo
    Dperm(i,9:16) = setdiff((1:16), Dperm(i,:));
end

tstatROIPerm = zeros(permNo/2,1);


for x = 1:permNo/2
    tic
    for i = 1:40
        for j = 1:40
            for k = 1:40

                if wmmask(i,j,k) == 0
                    continue
                end

                % Permutation
                Y = [group1(:,i,j,k) ; group2(:,i,j,k)]; %16x1
                Y = Y(Dperm(x,:)');
                
                Yhat = Px*Y;
                bhat = X \ Yhat;
                ehat = Rx*Y;
                varehat = (ehat'*ehat) / 14; % (length(Y) - rank(X));
                Sbetahat = varehat * pinvXX; % pinvXX = pinv(X'*X);

                tstatROItemp = (lambda' * bhat) / sqrt(lambda' * Sbetahat * lambda);
                if abs(tstatROItemp) > abs(tstatROIPerm(x))
                    tstatROIPerm(x) = tstatROItemp;
                end

            end
        end
    end
    toc
    
end

% savefile = 'TstatisticPerms';
% % % % save(savefile, 'tstatROIPerm');
% load(savefile, 'tstatROIPerm');

%% c) Determine the multiple-comparisons-corrected p-value
p = 0;
for i = 1:permNo/2
    if (abs(tstatROIPerm(i)) - abs(tstatVoxels)) > 1e-20
        p = p + 1;
    end
end
p/(permNo/2)

%% d) Determine the maximum t-statistic threshold corresponding to p-value of 5%
prct = prctile(tstatROIPerm, 95);


%% Plotting
member = 1;
% Group1 plotting
figure, 
plotWithMontage(squeeze(group1(member,:,:,:)),40,40,40), 
title(sprintf('Plotting CPA data for member %d', member));
% Group2 plotting
figure, 
plotWithMontage(squeeze(group2(member,:,:,:)),40,40,40), 
title(sprintf('Plotting PPA data for member %d', member));
% WM mask plotting
figure, 
plotWithMontage(wmmask,40,40,40), 
title('Plotting WM Mask');
% TSTAT plotting
figure, 
plotWithMontage(tstatROI,40,40,40), 
title('The 2-sample t-statistic between the groups');
% TSTAT plotting

plot1x = repmat(tstatVoxels, 100); plot1y = linspace(1,350,100);
plot2x = repmat(prct, 100); plot2y = linspace(1,350,100);

tstatROIPerm = abs(sort(tstatROIPerm));


figure, 
hist(tstatROIPerm,100),
hold on
plot(plot1x, plot1y, 'r--')
hold on
plot(plot2x, plot2y, 'g--')
legend('maximum t-statistic for different permutations',...
       'maximum t-statistic among all voxels',...
       'maximum t-statistic threshold corresponding to p-value of 5%',...
       'Location', 'northeast')
xlabel('maximum t-statistic')
ylabel('frequency')
title('The maximum 2-sample t-statistic between the groups with permutation');







