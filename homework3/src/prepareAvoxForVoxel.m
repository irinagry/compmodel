function Avox = prepareAvoxForVoxel(perfImagePerVoxel)

    [m n] = size(perfImagePerVoxel);
    Avox = zeros(m*n,1);

    k = 1;
    for i = 1:12
        Avox(k) = perfImagePerVoxel(i);
        k = k + 1;
    end

end