function displayData(images, noSubj, noEcho, noTagPost, gradOn, echoTimes)
    
    for i = 1:noSubj
        figure
        
        for j = 1:noEcho
           
            if strcmp(gradOn,'on') == 1
                imageTemp = ...
                    squeeze(images(i).on(:,:,j,1,2:2:noTagPost)) - ...
                    squeeze(images(i).on(:,:,j,1,1:2:noTagPost)) ;
            else
                imageTemp = ...
                    squeeze(images(i).off(:,:,j,1,2:2:noTagPost)) - ...
                    squeeze(images(i).off(:,:,j,1,1:2:noTagPost)) ;
            end
            
            
            subplot(noEcho, 1, j);
            [m n] = size(imageTemp);
            imagesc(imageTemp(5:45,:));
            if j == 1
                title(sprintf('Perfusion Weighted Images (grad %s) for subject %d\nEcho time %.1fms', ...
                               gradOn, i, echoTimes(j)));
            else
                title(sprintf('Echo time %.1fms', echoTimes(j)));
            end
            
            
        end
        
    end

end