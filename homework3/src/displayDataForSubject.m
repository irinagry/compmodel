function displayDataForSubject(perfImages, subject)

    plotWithMontage(perfImages(subject).on,64,64,84,12);
    title(sprintf('Perfusion Weighted Images (grad on) for subject %d (all 12 echo times for each row)', subject));

    plotWithMontage(perfImages(subject).off,64,64,84,12);
    title(sprintf('Perfusion Weighted Images (grad off) for subject %d (all 12 echo times for each row)', subject));


end