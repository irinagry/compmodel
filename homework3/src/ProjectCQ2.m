%% ProjectCQ2
% 2. Taking the mean signal for a cortical ROI

% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();


% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

Nboot = 50;

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2, Nboot); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2, Nboot);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2, Nboot); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2, Nboot);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
           for boot = 1:Nboot
            
                bootstrapIndex = randsample(10,10,true);
               
                % Perfusion Weighted
                imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k));
                voxelsMeanASLON = imgTmpOn(roiCurrent);
                meanCorticalASL.on(i,j,k,boot) = sum(voxelsMeanASLON(bootstrapIndex)) / noPixels;

                imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
                voxelsMeanASLOFF = imgTmpOff(roiCurrent);
                meanCorticalASL.off(i,j,k,boot) = sum(voxelsMeanASLOFF(bootstrapIndex)) / noPixels;

                % Control images
                imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
                voxelsMeanCtrlON = imgTmpControlOn(roiCurrent);
                meanControlSignal.on(i,j,k,boot) = sum(voxelsMeanCtrlON(bootstrapIndex)) / noPixels;

                imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
                voxelsMeanCtrlOFF = imgTmpControlOff(roiCurrent);
                meanControlSignal.off(i,j,k,boot) = sum(voxelsMeanCtrlOFF(bootstrapIndex)) / noPixels;
            
           end
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1,1);
    
end


subject = 1;
colorspec = {[1 0 0]; [0 1 0]; [0 0 1]; ...
  [0.5 0.5 0]; [0 0.5 0.5]; [0.5 0 0.5] ; [0.2 0.5 0.8]};

meanCorticalASLON   = meanCorticalASL.on;
meanCorticalASLOFF  = meanCorticalASL.off;
meanCorticalCTRLON  = meanControlSignal.on;
meanCorticalCTRLOFF = meanControlSignal.off;
%save('q2bootstrap', 'meanCorticalASLON', 'meanCorticalASLOFF', 'meanCorticalCTRLON', 'meanCorticalCTRLOFF');


for i = 2:9
    meanCorticalASL.on(1,:,:,:) = meanCorticalASL.on(1,:,:,:) + meanCorticalASL.on(i,:,:,:);
    meanCorticalASL.off(1,:,:,:) = meanCorticalASL.off(1,:,:,:) + meanCorticalASL.off(i,:,:,:);
    meanControlSignal.on(1,:,:,:) = meanControlSignal.on(1,:,:,:) + meanControlSignal.on(i,:,:,:);
    meanControlSignal.off(1,:,:,:) = meanControlSignal.off(1,:,:,:) + meanControlSignal.off(i,:,:,:);
end
meanCorticalASL.on(1,:,:,:) = meanCorticalASL.on(1,:,:,:)./9;
meanCorticalASL.off(1,:,:,:) = meanCorticalASL.off(1,:,:,:)./9;
meanControlSignal.on(1,:,:,:) = meanControlSignal.on(1,:,:,:)./9;
meanControlSignal.off(1,:,:,:) = meanControlSignal.off(1,:,:,:)./9;

% Plot  ASL ON
figure
subplot(2,2,1)
for k = 1:noTagPost/2
    
    meanCortical = mean(squeeze(meanCorticalASL.on(subject,:,k,:)), 2);
    stdCortical  = std(squeeze(meanCorticalASL.on(subject,:,k,:)), 1, 2);
        
%     plot(echoTimes, log(meanCortical./scalingFactorASL.on(subject)), 's-', 'Color', colorspec{k});%semilogy
    %hold on
    errorbar(echoTimes, (meanCortical./scalingFactorASL.on(subject)), (stdCortical))
    
    meanCorticalASL.on(subject,:,k,:) = meanCorticalASL.on(subject,:,k,:)./scalingFactorASL.on(subject);
    hold on
end
title('Mean perfusion weighted signal for all subjects (VCG on)');
xlabel('TE [ms]');
ylabel('dM(VCGs_{on}) [arbitrary units]');
legend('50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000');


% Plot  ASL OFF
subplot(2,2,2)
for k = 1:noTagPost/2
    meanCortical = mean(squeeze(meanCorticalASL.off(subject,:,k,:)), 2);
    stdCortical  = std(squeeze(meanCorticalASL.off(subject,:,k,:)), 1, 2);
    
    %semilogy(echoTimes, meanCorticalASL.off(subject,:,k)./scalingFactorASL.off(subject), 's-', 'Color', colorspec{k});
    errorbar(echoTimes, (meanCortical./scalingFactorASL.off(subject)), (stdCortical))
    
    meanCorticalASL.off(subject,:,k,:) = meanCorticalASL.off(subject,:,k,:)./scalingFactorASL.off(subject);
    hold on
end
title('Mean perfusion weighted signal for all subjects (VCG off)');
xlabel('TE [ms]');
ylabel('dM(VCGs_{off}) [arbitrary units]');
legend('50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000');

% Plot  Control ON
subplot(2,2,3)
for k = 1:noTagPost/2
    meanCortical = mean(squeeze(meanControlSignal.on(subject,:,k,:)), 2);
    stdCortical  = std(squeeze(meanControlSignal.on(subject,:,k,:)), 1, 2);
    
    %semilogy(echoTimes, meanControlSignal.on(subject,:,k)./scalingFactorControl.on(subject), 's-', 'Color', colorspec{k});
    errorbar(echoTimes, (meanCortical./scalingFactorControl.on(subject)), (stdCortical))
    
    meanControlSignal.on(subject,:,k,:) = meanControlSignal.on(subject,:,k,:)./scalingFactorControl.on(subject);
    hold on
end
title('Control signal for all subjects (VCG on)');
xlabel('TE [ms]');
ylabel('dM(VCGs_{on}) [arbitrary units]');
legend('50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000');

% Plot  Control OFF
subplot(2,2,4)
for k = 1:noTagPost/2
    meanCortical = mean(squeeze(meanControlSignal.off(subject,:,k,:)), 2);
    stdCortical  = std(squeeze(meanControlSignal.off(subject,:,k,:)), 1, 2);
    
    %semilogy(echoTimes, meanControlSignal.off(subject,:,k)./scalingFactorControl.off(subject), 's-', 'Color', colorspec{k});
    errorbar(echoTimes, (meanCortical./scalingFactorControl.off(subject)), (stdCortical))
    
    meanControlSignal.off(subject,:,k,:) = meanControlSignal.off(subject,:,k,:)./scalingFactorControl.off(subject);
    hold on
end
title('Control signal for all subjects (VCG off)');
xlabel('TE [ms]');
ylabel('dM(VCGs_{off}) [arbitrary units]');
legend('50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000');
