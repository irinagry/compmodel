load('meanResMonoexp', 'meanRes');
meanResMono = mean(meanRes);

load('meanResBiexp', 'meanRes');
meanResBi = mean(meanRes);

D = [meanResMono meanResBi];
Dperm  = combnk((1:14),7);
permNo = length(Dperm);

Dperm = [Dperm , zeros(permNo, 7)];


for i = 1:permNo
    Dperm(i,8:14) = setdiff((1:14), Dperm(i,:));
    Dperm(i,:) = D(1,Dperm(i,:));
end


tstatDperm = zeros(permNo,1);
for i = 1:permNo
    [Hh,Pp,CIci,STATS] = ttest2(Dperm(i,1:6), Dperm(i,7:14));
    tstatDperm(i) = STATS.tstat;
end


p = 0;
for i = 1:permNo
    if (abs(tstatDperm(i)) - abs(STATS.tstat)) > 1e-20
        p = p + 1;
    end
end


fprintf('Calculated p-value = %.5f n(p) = %3d permNo = %d\n', p/permNo, p, permNo);

