function [S] = biExponentialT2Estimated(x, TE)
  
% Extract parameters
S0IC = x(1);
S0EC = x(2);
T2IC = x(3);
T2EC = x(4);

S = S0IC .* exp(-TE./T2IC) + S0EC .* exp(-TE./T2EC);

end