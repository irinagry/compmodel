function displayAtShortestEchoTime(perfImages, echoTimes, etime)
    
    perfImagesToPlot.on = zeros(64,64,7,1);
    perfImagesToPlot.off = zeros(64,64,7,1);

    for i = 1:1
        perfImagesToPlot.on(:,:,:,i) = squeeze(perfImages(i).on(:,:,etime,1,:));
        perfImagesToPlot.off(:,:,:,i) = squeeze(perfImages(i).off(:,:,etime,1,:));
    end
    
    plotWithMontage(perfImagesToPlot.on,64,64,7,7);
    title(sprintf('Perfusion Weighted Images (grad on) for TE = %.1fms', echoTimes(etime)));
%     saveas(gcf,sprintf('../fig/forgif/1SubjectMontageOn%d.png', etime));
    pause(1);
    
    
    plotWithMontage(perfImagesToPlot.off,64,64,7,7);
    title(sprintf('Perfusion Weighted Images (grad off) for TE = %.1fms', echoTimes(etime)));
%     saveas(gcf,sprintf('../fig/forgif/1SubjectMontageOff%d.png', etime));
    pause(1);

end