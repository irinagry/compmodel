%%% Open data file

%% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();


%% 1. Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Display data for a particular subject
% displayDataForSubject(perfImages, 1);

% Display data for diffusion gradient on for all 9 subjects
% displayData(images, noSubj, noEcho, noTagPost, 'on', echoTimes);

% Display data with image play
% subject = 1; echoTime = 1;
% figure, implay(squeeze(perfImages(subject).on(5:45,:,echoTime,1,:)));

% Display each of the 7 images on the same intensity scale with the
% appropriate pld/tau combinations
% displayAtShortestEchoTime(perfImages, echoTimes);



%% 2. Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
            % Perfusion Weighted
            imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k));
            meanCorticalASL.on(i,j,k) = sum(imgTmpOn(roiCurrent)) / noPixels;
        
            imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
            meanCorticalASL.off(i,j,k) = sum(imgTmpOff(roiCurrent)) / noPixels;
            
            % Control images
            imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
            meanControlSignal.on(i,j,k) = sum(imgTmpControlOn(roiCurrent)) / noPixels;
            
            imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
            meanControlSignal.off(i,j,k) = sum(imgTmpControlOff(roiCurrent)) / noPixels;
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1);
    
end


subject = 1;
colorspec = {[1 0 0]; [0 1 0]; [0 0 1]; ...
  [0.5 0.5 0]; [0 0.5 0.5]; [0.5 0 0.5] ; [0.2 0.5 0.8]};

% Plot  ASL ON
figure
for k = 1:noTagPost/2
    semilogy(echoTimes, meanCorticalASL.on(subject,:,k)./scalingFactorASL.on(subject), 's-', 'Color', colorspec{k});
    meanCorticalASL.on(subject,:,k) = meanCorticalASL.on(subject,:,k)./scalingFactorASL.on(subject);
    hold on
end
title(sprintf('Mean perfusion weighted signal Subject %d', subject));
xlabel('TE [ms]');
ylabel('log(dM(VCGs_{on})) [arbitrary units]');


% Plot  ASL OFF
figure
for k = 1:noTagPost/2
    semilogy(echoTimes, meanCorticalASL.off(subject,:,k)./scalingFactorASL.off(subject), 's-', 'Color', colorspec{k});
    meanCorticalASL.off(subject,:,k) = meanCorticalASL.off(subject,:,k)./scalingFactorASL.off(subject);
    hold on
end
title(sprintf('Mean perfusion weighted signal Subject %d', subject));
xlabel('TE [ms]');
ylabel('log(dM(VCGs_{off})) [arbitrary units]');


% Plot  Control ON
figure
for k = 1:noTagPost/2
    semilogy(echoTimes, meanControlSignal.on(subject,:,k)./scalingFactorControl.on(subject), 's-', 'Color', colorspec{k});
    meanControlSignal.on(subject,:,k) = meanControlSignal.on(subject,:,k)./scalingFactorControl.on(subject);
    hold on
end
title(sprintf('Control signal Subject %d', subject));
xlabel('TE [ms]');
ylabel('log(dM(VCGs_{on})) [arbitrary units]');


% Plot  Control OFF
figure
for k = 1:noTagPost/2
    semilogy(echoTimes, meanControlSignal.off(subject,:,k)./scalingFactorControl.off(subject), 's-', 'Color', colorspec{k});
    meanControlSignal.off(subject,:,k) = meanControlSignal.off(subject,:,k)./scalingFactorControl.off(subject);
    hold on
end
title(sprintf('Control signal Subject %d', subject));
xlabel('TE [ms]');
ylabel('log(dM(VCGs_{off})) [arbitrary units]');


%% 3.1 Mono-exponential fitting of the transverse decay (signal vs echo time)

% Create my data
AvoxASL.on      = zeros(noSubj*noEcho*(noTagPost/2),1);
AvoxASL.off     = zeros(noSubj*noEcho*(noTagPost/2),1);
AvoxControl.on  = zeros(noSubj*noEcho*(noTagPost/2),1);
AvoxControl.off = zeros(noSubj*noEcho*(noTagPost/2),1);
index = 1;

for i = 1:noSubj
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
            % Perfusion Weighted
            AvoxASL.on(index) = meanCorticalASL.on(i,j,k);
            AvoxASL.off(index) = meanCorticalASL.off(i,j,k);
            
            % Control images
            AvoxControl.on(index) = meanControlSignal.on(i,j,k);
            AvoxControl.off(index) = meanControlSignal.off(i,j,k);
            
            index = index+1;
            
        end
        
    end
    
end

% Fit Data
[PLD, TAG, TE] = prepareParamsForFit(length(AvoxASL.on), durations, echoTimes);

fittype = 'ASL';
grads = 'on';

N = 15; % number of initial runs

switch fittype
    case 'ASL'
        % Initial Params
        S0ini =   2.5 + 5.*randn(N,1);
        T2ini = 250 + 250.*randn(N,1);
        lb = [  -5,    0];
        ub = [   5, 1000];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ini =  50 +  50.*randn(N,1);
        T2ini = 250 + 250.*randn(N,1);
        lb = [   0,    0];
        ub = [ 100, 1000];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end


% Define multiple starting points
initialPoints = [S0ini T2ini];
finalPoints   = zeros(1,2);

predictedS = zeros(9,84);

for ratNo = 1:1
    
    RESNORMMIN = 1e+30; % high value
    RESNORMCounter = zeros(N,1);
    
    switch fittype
        
        case 'ASL'
            
            switch grads
                case 'on'
                % Fmincon parameters
                evalFunc = @(x) monoExponentialSSD(x, ...
                    AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :), TE(((ratNo-1)*84+1):(ratNo*84), :));
        
                case 'off'
                % Fmincon parameters
                evalFunc = @(x) monoExponentialSSD(x, ...
                    AvoxASL.off(((ratNo-1)*84+1):(ratNo*84), :), TE(((ratNo-1)*84+1):(ratNo*84), :));
            end
            
        case 'Control'
            
            switch grads
                case 'on'
                % Fmincon parameters
                evalFunc = @(x) monoExponentialSSD(x, ...
                    AvoxControl.on(((ratNo-1)*84+1):(ratNo*84), :), TE(((ratNo-1)*84+1):(ratNo*84), :));
        
                case 'off'
                % Fmincon parameters
                evalFunc = @(x) monoExponentialSSD(x, ...
                    AvoxControl.off(((ratNo-1)*84+1):(ratNo*84), :), TE(((ratNo-1)*84+1):(ratNo*84), :));
            end
            
    end
    
    tic
    for i = 1:N
        % Define various options for the non-linear fitting algorithm
        h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                     'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

        try
            % Define a starting point for the non-linear fit
            startx = initialPoints(i,:);

            % Run the fitting
            [parameter_hat, RESNORM, EXITFLAG, OUTPUT, LAMBDA, GRAD, HESSIAN] = ...
                fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);

            RESNORMCounter(i) = RESNORM;

        catch ME
            warning(ME.message);
            disp(ME.message);

            RESNORM = 1e+30; % high value
            RESNORMMIN = 1e+30;
            RESNORMCounter(i) = RESNORM;
        end

        if RESNORM < RESNORMMIN
            finalPoints = parameter_hat;
            RESNORMMIN = RESNORM;
            
            S0estimate = parameter_hat(1);
            T2estimate = parameter_hat(2);
            
            sigma = 1;
            HESSIAN2 = HESSIAN./(2*sigma^2);
            invHESSIAN = pinv(HESSIAN2);

            stdS0 = sqrt(invHESSIAN(1,1));
            stdT2 = sqrt(invHESSIAN(2,2));
            
        end
    end
    toc

    % What I got
    fprintf('=====>\n%s mono-exponential, grads %s Rat %d RESNORMIN = %f \n', fittype, grads, ratNo, RESNORMMIN);
    fprintf('Final Points\n'); finalPoints
    fprintf('\n<=====\n\n');
    
    titleFigure = sprintf(['Measured vs Predicted Values', ...
                        '(%s mono-exponential)\n grads %s for Rat %d'],...
                        fittype, grads, ratNo);

    % Compare predictions with actual measurements:
    switch fittype
        case 'ASL'
            [predictedS(ratNo,:)] = monoExponential(finalPoints, TE(((ratNo-1)*84+1):(ratNo*84), :));
            figure
            switch grads
                case 'on'
                    plot(TE(((ratNo-1)*84+1):(ratNo*84)),AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
                    
                case 'off'
                    plot(TE(((ratNo-1)*84+1):(ratNo*84)),AvoxASL.off(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
            end

        case 'Control'
            [predictedS(ratNo,:)] = monoExponential(finalPoints, TE(((ratNo-1)*84+1):(ratNo*84), :));
            figure
            switch grads
                case 'on'
                    plot(TE(((ratNo-1)*84+1):(ratNo*84)),AvoxControl.on(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
                case 'off'
                    plot(TE(((ratNo-1)*84+1):(ratNo*84)),AvoxControl.off(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
            end
    end
    
    hold on
    plot(TE(((ratNo-1)*84+1):(ratNo*84)), predictedS(ratNo,:),'rx'); % predictions
    title(titleFigure);
    legend('Data','Model');
    
    
    
    % Plot confidence interval
    plotConfidenceInterval = 'yes';
    if strcmp(plotConfidenceInterval,'yes')
        % Confidence interval
        figure
        % S0 confidence interval
        subplot(1,2,1)
        plot( [S0estimate-4*stdS0:stdS0/100:S0estimate+4*stdS0], ...
              normpdf([S0estimate-4*stdS0:stdS0/100:S0estimate+4*stdS0],S0estimate,stdS0));
        hold on
        % 2 sigma
        scatter(linspace(S0estimate-2*stdS0 , S0estimate+2*stdS0, 50), repmat(5e-1, [1 50]), ...
                                '.', 'MarkerEdgeColor', [1 0 0]);
        titleS0 = sprintf('S0\n2 sigma: [%f, %f]',S0estimate-2*stdS0,S0estimate+2*stdS0);
        title(titleS0)

        % T2 confidence interval
        subplot(1,2,2)
        plot( [T2estimate-4*stdT2:stdT2/100:T2estimate+4*stdT2], ...
              normpdf([T2estimate-4*stdT2:stdT2/100:T2estimate+4*stdT2],T2estimate,stdT2));
        hold on
        % 2 sigma
        scatter(linspace(T2estimate-2*stdT2 , T2estimate+2*stdT2, 50), repmat(15e-3, [1 50]), ...
                                '.', 'MarkerEdgeColor', [1 0 0]);
        titleT2 = sprintf('T2\n2 sigma: [%f, %f]',T2estimate-2*stdT2,T2estimate+2*stdT2);
        title(titleT2)
    end

    
end




%% 4 Bi-exponential fitting of the transverse decay
fittype = 'ASL';
grads = 'on'; % always on
T2Constant = 'on';

N = 15; % number of initial runs

switch fittype
    case 'ASL'
        % Initial Params
        S0ICini =   2.5 + 5.*randn(N,1);
        S0ECini =   2.5 + 5.*randn(N,1);
        T2ICini = 100 + 100.*randn(N,1);
        T2ECini = 100 + 100.*randn(N,1);
        lb = [  -5, -5,    0,    0];
        ub = [   5,  5,  200,  200];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ICini =  50 +  50.*randn(N,1);
        S0ECini =  50 +  50.*randn(N,1);
        T2ICini = 100 + 100.*randn(N,1);
        T2ECini = 100 + 100.*randn(N,1);
        lb = [   0,   0,    0,    0];
        ub = [ 100, 100,  200,  200];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end

% Define multiple starting points
switch T2Constant
    case 'on'
        initialPoints = [S0ICini S0ECini];
        finalPointsBi   = zeros(1,14); % 2params * 7 pld tau combinations
        lb = lb(1:2);
        ub = ub(1:2);
    case 'off'
        initialPoints = [S0ICini S0ECini T2ICini T2ECini];
        finalPointsBi   = zeros(1,28); % 4params * 7 pld tau combinations
end

predictedSBi = zeros(9,84);

for ratNo = 1:1
    
    RESNORMMIN = 1e+30; % high value
    RESNORMCounter = zeros(N,1);
    
    inSubjLB = ((ratNo-1)*84+1);
    inSubjUB = (ratNo*84);
    
    switch fittype
        
        case 'ASL'
            
            switch T2Constant
                case 'on'
                % Fmincon parameters
                evalFunc = @(x) biExponentialSSDT2Constant(x, ...
                    AvoxASL.on(inSubjLB:inSubjUB, :), TE(inSubjLB:inSubjUB, :));
        
                case 'off'
                % Fmincon parameters
                evalFunc = @(x) biExponentialSSDT2Estimated(x, ...
                    AvoxASL.off(inSubjLB:inSubjUB, :), TE(inSubjLB:inSubjUB, :));
            end
            
        case 'Control'
            
            switch T2Constant
                case 'on'
                % Fmincon parameters
                evalFunc = @(x) biExponentialSSDT2Constant(x, ...
                    AvoxControl.on(inSubjLB:inSubjUB, :), TE(inSubjLB:inSubjUB, :));
        
                case 'off'
                % Fmincon parameters
                evalFunc = @(x) biExponentialSSDT2Estimated(x, ...
                    AvoxControl.off(inSubjLB:inSubjUB, :), TE(inSubjLB:inSubjUB, :));
            end
            
    end
    
    tic
    for i = 1:N
        % Define various options for the non-linear fitting algorithm
        h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                     'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');


        try
            % Define a starting point for the non-linear fit
            startx = initialPoints(i,:);

            % Run the fitting
            [parameter_hat, RESNORM, EXITFLAG, OUTPUT, LAMBDA, GRAD, HESSIAN] = ...
                fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);

            RESNORMCounter(i) = RESNORM;

        catch ME
            warning(ME.message);
            disp(ME.message);

            RESNORM = 1e+30; % high value
            RESNORMMIN = 1e+30;
            RESNORMCounter(i) = RESNORM;
        end

        if RESNORM < RESNORMMIN
            finalPointsBi = parameter_hat;
            RESNORMMIN = RESNORM;

%             S0ICestimate = parameter_hat(1);
%             S0ECestimate = parameter_hat(2);
%             T2ICestimate = parameter_hat(3);
%             T2ECestimate = parameter_hat(4);

        end
        
    end
    toc

    % What I got
    if strcmp(T2Constant, 'on')
        t2cst = 'given';
    else
        t2cst = 'estimated';
    end
    fprintf('=====>\n%s bi-exponential, grads %s, T2 %s, Rat %d RESNORMIN = %f \n', fittype, grads, t2cst, ratNo, RESNORMMIN);
    fprintf('Final Points\n'); finalPointsBi
    fprintf('\n<=====\n\n');

    % Compare predictions with actual measurements:
    switch fittype
        case 'ASL'
            
            switch T2Constant
                case 'on'
                    [predictedSBi(ratNo,:)] = biExponentialT2Constant(finalPointsBi, TE(((ratNo-1)*84+1):(ratNo*84), :));
                    figure
                    plot(TE(inSubjLB:inSubjUB), AvoxASL.on(inSubjLB:inSubjUB, :),'bs'); % measurements
                    
                    titleFigure = sprintf('Measured vs Predicted Values (%s bi-exponential with T2 given)\n grads %s for Rat %d',...
                        fittype, grads, ratNo);
                case 'off'
                    [predictedSBi(ratNo,:)] = biExponentialT2Estimated(finalPointsBi, TE(((ratNo-1)*84+1):(ratNo*84), :));
                    figure
                    plot(TE(inSubjLB:inSubjUB), AvoxASL.off(inSubjLB:inSubjUB, :),'bs'); % measurements
                    
                    titleFigure = sprintf('Measured vs Predicted Values (%s bi-exponential with T2 estimated)\n grads %s for Rat %d',...
                        fittype, grads, ratNo);
            end

        case 'Control'
            
            switch T2Constant
                case 'on'
                    [predictedSBi(ratNo,:)] = biExponentialT2Constant(finalPointsBi, TE(((ratNo-1)*84+1):(ratNo*84), :));
                    figure
                    plot(TE(inSubjLB:inSubjUB), AvoxControl.on(inSubjLB:inSubjUB, :),'bs'); % measurements
                    
                    titleFigure = sprintf('Measured vs Predicted Values (%s bi-exponential with T2 given)\n grads %s for Rat %d',...
                        fittype, grads, ratNo);
                case 'off'
                    [predictedSBi(ratNo,:)] = biExponentialT2Estimated(finalPointsBi, TE(inSubjLB:inSubjUB, :));
                    figure
                    plot(TE(inSubjLB:inSubjUB), AvoxControl.off(inSubjLB:inSubjUB, :),'bs'); % measurements
                    
                    titleFigure = sprintf('Measured vs Predicted Values (%s bi-exponential with T2 estimated)\n grads %s for Rat %d',...
                        fittype, grads, ratNo);
            end
    end
    
    hold on
    plot(TE(inSubjLB:inSubjUB), predictedSBi(ratNo,:),'rx'); % predictions
    title(titleFigure);
    legend('Data','Model');
    
end










%% 3.2 Spatial map
grads = 'on';
N = 5;
ratNo = 4; % just for a single subject
% for i = 4:4 
%     figure, imagesc(squeeze(perfImages(i).on(10:35,15:45,1,1,4)))
% end
XMAX = 25; %10:35
YMAX = 30; %15:45
% Maps of:
mS0 = zeros(XMAX,YMAX);
mT2 = zeros(XMAX,YMAX);
mResnorm = zeros(XMAX,YMAX);

RESNORMCounterMap = zeros(N,XMAX,YMAX);

% for fmincon
lb = [  -1,    0];
ub = [   5, 1000];
A = [];
b = [];
Aeq = [];
beq = [];

runagain = 'no';
if strcmp(runagain, 'no')
    tic
    for x = 1:XMAX
        for y = 1:YMAX

            switch grads
                case 'on'
                % Fmincon parameters
                Avox = prepareAvoxForVoxel(squeeze(perfImages(ratNo).on(x+9,y+14,:,1,:)));
                evalFunc = @(x) monoExponentialSSD(x, ...
                    Avox, TE(((ratNo-1)*84+1):(ratNo*84), :));

                case 'off'
                % Fmincon parameters
                Avox = prepareAvoxForVoxel(squeeze(perfImages(ratNo).off(x+9,y+14,:,1,:)));
                evalFunc = @(x) monoExponentialSSD(x, ...
                    Avox, TE(((ratNo-1)*84+1):(ratNo*84), :));
            end


            % Add gaussians around initial points
            S0ini =   2.5 + 5.*randn(N,1);
            T2ini = 250 + 250.*randn(N,1);

            % Initial points matrix
            initialPoints = [S0ini T2ini];

            finalPoints = zeros(1,2);
            RESNORMMIN = 1e+30; % high value

            for i = 1:N
                % Define various options for the non-linear fitting algorithm
                h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

                % Run the fitting
                try
                    % Define a starting point for the non-linear fit
                    startx = initialPoints(i,:);

                    % Run the fitting
                    [parameter_hat, RESNORM, EXITFLAG, OUTPUT, LAMBDA, GRAD, HESSIAN] = ...
                        fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);
                    RESNORMCounterMap(i,x,y) = RESNORM;

                catch ME
                    warning(ME.message);
                    disp(ME.message);

                    RESNORM = 1e+30; % high value
                    RESNORMMIN = 1e+30;
                    RESNORMCounterMap(i,x,y) = RESNORM;
                end

                if RESNORM < RESNORMMIN

                    RESNORMMIN = RESNORM;
                    finalPoints = parameter_hat;

                    mS0(x,y) = parameter_hat(1);
                    mT2(x,y) = parameter_hat(2);
                    mResnorm(x,y) = RESNORM;

                end
            end

        end
        x
    end
    elapsedTime = toc
end

savefile = 'Q53SpatialMapOn';
% % % save(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');
load(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');

figure, imagesc(mS0), title('S0 map Grad On'), colorbar, colormap hot
T2mapCorrected = mT2; T2mapCorrected(T2mapCorrected>200) = 0;
figure, imagesc(T2mapCorrected), title('T2 map Grad On'), colorbar, colormap hot
figure, imagesc(mResnorm), title('RESNORM map Grad On'), colorbar, colormap hot


savefile = 'Q53SpatialMapOff';
% % % save(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');
load(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');

figure, imagesc(mS0), title('S0 map Grad off'), colorbar, colormap hot
T2mapCorrected = mT2; T2mapCorrected(T2mapCorrected>200) = 0;
figure, imagesc(T2mapCorrected), title('T2 map Grad off'), colorbar, colormap hot
figure, imagesc(mResnorm), title('RESNORM map Grad off'), colorbar, colormap hot







%% 3.2 Parametric Bootstrap
fittype = 'ASL';
grads = 'on';

switch fittype
    case 'ASL'
        % Initial Params
        S0ini =   2.5 + 5.*randn(N,1);
        T2ini = 250 + 250.*randn(N,1);
        lb = [  -5,    0];
        ub = [   5, 1000];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ini =  50 +  50.*randn(N,1);
        T2ini = 250 + 250.*randn(N,1);
        lb = [   0,    0];
        ub = [ 100, 1000];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end


% Define multiple starting points
initialPoints = [S0ini T2ini];
finalPoints   = zeros(1,2);

for ratNo = 1:1

    RESNORMMIN = 1e+30; % high value
    RESNORMCounter = zeros(N,1);
    
    inSubjLB = ((ratNo-1)*84+1);
    inSubjUB = (ratNo*84);
    
    % Parametric Bootstrap
    T = 100;
    K = 84; N = 2;
    % Values stored after parametric boostraping
    mS0 = zeros(1,T);
    mT2 = zeros(1,T);
    
    switch fittype
        
        case 'ASL'
            
            switch grads
                case 'on'
                residuals = sum((AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = (sqrt(noise)/10).*randn(T, 84);
                %Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
                Sperturbed = repmat(AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :)', [T,1]) + noiseModel;
        
                case 'off'
                residuals = sum((AvoxASL.off(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = (sqrt(noise)/10).*randn(T, 84);
                Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
            end
            
        case 'Control'
            
            switch grads
                case 'on'                
                residuals = sum((AvoxControl.on(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = sqrt(noise).*randn(T, 84);
                Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
        
                case 'off'
                residuals = sum((AvoxControl.off(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = sqrt(noise).*randn(T, 84);
                Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
            end
            
    end
    
    tic
    for paramBoot = 1:T
        
        % Prepare the function to be evaluated
        evalFunc = @(x) monoExponentialSSD(x, ...
                        Sperturbed(paramBoot,:)', TE(((ratNo-1)*84+1):(ratNo*84), :));
        
        % Evaluate function
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

            try
                % Define a starting point for the non-linear fit
                startx = initialPoints(i,:);

                % Run the fitting
                [parameter_hat_boot, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);

                RESNORMCounter(i) = RESNORM;

            catch ME
                warning(ME.message);
                disp(ME.message);

                RESNORM = 1e+30; % high value
                RESNORMMIN = 1e+30;
                RESNORMCounter(i) = RESNORM;
            end

            if RESNORM < RESNORMMIN
                finalPoints = parameter_hat_boot;
                RESNORMMIN = RESNORM;
                
                mS0(paramBoot) = parameter_hat_boot(1);
                mT2(paramBoot) = parameter_hat_boot(2);
            end
        end
    end
    toc
    
    % Show histograms
    figure
    % subplot 1 S0 histogram
    subplot(1,2,1)
    histfit(mS0, T/5)
    % subplot 2 T2 histogram
    subplot(1,2,2)
    histfit(mT2, T/5)

    mS0sorted = sort(mS0);
    mT2sorted = sort(mT2);

    %%%%%%%%%% RANGES
    % S0
    % 2 sigma range:
    S02sigmaL=mean(mS0)-2*std(mS0);
    S02sigmaU=mean(mS0)+2*std(mS0);
    % 95% range
    S095L = prctile(mS0sorted,2.5);
    S095U = prctile(mS0sorted,97.5);
    
    % T2
    % 2 sigma range:
    T22sigmaL=mean(mT2)-2*std(mT2);
    T22sigmaU=mean(mT2)+2*std(mT2);
    % 95% range
    T295L = prctile(mT2sorted,2.5); 
    T295U = prctile(mT2sorted,97.5);
    
    fprintf('\n%s grads %s for Rat %d S0\nstd: %f\n2 sigma: [%f, %f]\n95%%: [%f, %f]\n',...
        fittype, grads, ratNo, std(mS0), S02sigmaL, S02sigmaU, S095L, S095U);
    
    fprintf('\n%s grads %s for Rat %d T2\nstd: %f\n2 sigma: [%f, %f]\n95%%: [%f, %f]\n',...
        fittype, grads, ratNo, std(mT2), T22sigmaL, T22sigmaU, T295L, T295U);
    
    
    
    
    % Compare predictions with actual measurements:
    figure
%     for i = 2:T
%         plot(Sperturbed(i,:),'rs'); % perturbed data
%         hold on
%     end
    switch fittype
        case 'ASL'
            [predictedS] = monoExponential(finalPoints, TE(inSubjLB:inSubjUB, :));
            switch grads
                case 'on'
                    plot(TE(inSubjLB:inSubjUB), AvoxASL.on(inSubjLB:inSubjUB, :),'bs'); % measurements
                case 'off'
                    plot(TE(inSubjLB:inSubjUB), AvoxASL.off(inSubjLB:inSubjUB, :),'bs'); % measurements
            end

        case 'Control'
            [predictedS] = monoExponential(finalPoints, TE(inSubjLB:inSubjUB, :));
            switch grads
                case 'on'
                    plot(TE(inSubjLB:inSubjUB), AvoxControl.on(inSubjLB:inSubjUB, :),'bs'); % measurements
                case 'off'
                    plot(TE(inSubjLB:inSubjUB), AvoxControl.off(inSubjLB:inSubjUB, :),'bs'); % measurements
            end
    end
    hold on
    plot(TE(inSubjLB:inSubjUB), predictedS, 'gx');
    hold on
    errorbar(TE(inSubjLB:inSubjUB), predictedS, predictedS-std(mS0), predictedS+std(mS0))
    
end



return





%%
% 
% class(new_images_off)
% size(new_images_off)
% 
% figure
% 
% oldim = []
% for i = 1:12
%       oldim =  [oldim,squeeze(new_images_off(:,:,i,1,1))];
% end
% figure(1)
% imagesc(oldim), colormap jet, colorbar
% 
% for i = 1:12
%     if (i <= 6)
%         subplot(6,2,i)
%         imagesc(squeeze(new_images_off(:,:,i,1,1)));
%     else
%         subplot(6,2,i)
%         imagesc(squeeze(new_images_off(:,:,i,1,1)));
%     end
% end
% subplot(2,2,1)
% imagesc(squeeze(new_images_off(:,:,1,1,1)));
% subplot(2,2,2)
% imagesc(squeeze(new_images_off(:,:,2,1,1)));
% subplot(2,2,3)
% imagesc(squeeze(new_images_off(:,:,3,1,1)));
% subplot(2,2,4)
% imagesc(squeeze(new_images_off(:,:,4,1,1)));
% 
% 
% figure
% subplot(2,2,1)
% imagesc(squeeze(new_images_off(:,:,1,1,1)));
% subplot(2,2,2)
% imagesc(squeeze(new_images_off(:,:,1,1,2)));
% subplot(2,2,3)
% imagesc(squeeze(new_images_off(:,:,1,1,3)));
% subplot(2,2,4)
% imagesc(squeeze(new_images_off(:,:,1,1,4)));