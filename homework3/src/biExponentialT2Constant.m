function [S] = biExponentialT2Constant(x, TE)
  
% Extract parameters
S0IC = x(1);
S0EC = x(2);
T2IC = 53;
T2EC = 133;

S = S0IC .* exp(-TE./T2IC) + S0EC .* exp(-TE./T2EC);

end