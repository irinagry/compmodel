%% Core Question 1
% Extract the perfusion weighted and control images


% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();


% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Display data for a particular subject
% displayDataForSubject(perfImages, 1);

% Display data for diffusion gradient on for all 9 subjects
% displayData(images, noSubj, noEcho, noTagPost, 'on', echoTimes);

% Display data with image play
% subject = 1; echoTime = 1;
% figure, implay(squeeze(perfImages(subject).on(5:45,:,echoTime,1,:)));

% Display each of the 7 images on the same intensity scale with the
% appropriate pld/tau combinations

imageCrusherOnAll = [];
imageCrusherOffAll = [];
for subject = 1:1
    echoTime = 1;
    imageCrusherOn = [];
    imageCrusherOff = [];
    
    for i = 1:7
        imageCrusherOn = [imageCrusherOn  ...
            squeeze(ctrlImages(subject).on(10:40,:,echoTime,1,i))];
        imageCrusherOff = [imageCrusherOff  ...
            squeeze(ctrlImages(subject).off(10:40,:,echoTime,1,i))];
    end

    imageCrusherOnAll = [imageCrusherOnAll ; imageCrusherOn];
    imageCrusherOffAll = [imageCrusherOffAll ; imageCrusherOff];
end

    figure
    subplot(2,1,1)
    imagesc(imageCrusherOnAll), colormap jet
    ax = gca;
    ax.YTick = [1:2:18] .* 30/2;
    ax.YTickLabel = {'subj 1','subj 2','subj 3','subj 4','subj 5','subj 6','subj 7','subj 8','subj 9'};
    ax.XTick = [1:2:14] .* 64/2;
    ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
    xlabel('PLD ms / \tau ms');
    title('Perfusion Weighted images for all subjects, TE = 29ms, crusher gradients on');
    
    
    subplot(2,1,2)
    imagesc(imageCrusherOffAll), colormap jet
    ax = gca;
    ax.YTick = [1:2:18] .* 30/2;
    ax.YTickLabel = {'subj 1','subj 2','subj 3','subj 4','subj 5','subj 6','subj 7','subj 8','subj 9'};
    ax.XTick = [1:2:14] .* 64/2;
    ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
    xlabel('PLD ms / \tau ms');
    title('Perfusion Weighted images for all subjects, TE = 29ms, crusher gradients off');
    
    
    figure
    imagesc([imageCrusherOnAll ; imageCrusherOffAll]), colormap jet
    ax = gca;
    ax.YTick = [1:2:36] .* 31/2;
    ax.YTickLabel = {'subj 1 VCG on','subj 2 VCG on','subj 3 VCG on','subj 4 VCG on','subj 5 VCG on',...
        'subj 6 VCG on','subj 7 VCG on','subj 8 VCG on','subj 9 VCG on', ...
        'subj 1 VCG off','subj 2 VCG off','subj 3 VCG off','subj 4 VCG off','subj 5 VCG off',...
        'subj 6 VCG off','subj 7 VCG off','subj 8 VCG off','subj 9 VCG off'};
    ax.XTick = [1:2:14] .* 64/2;
    ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
    xlabel('PLD ms / \tau ms');
    title('Perfusion Weighted images for all subjects, TE = 29ms');
    
    
%     saveas(gcf,'../fig/fig51ONOFFALL.png');

% Plot all 12 echo times for a subject
% for i = 1:12
%    displayAtShortestEchoTime(perfImages, echoTimes, i);
% end

% echoTime = 1;
% for subject = 1:9
%     scalingfact = max(max(max(squeeze(perfImages(subject).on(:,:,echoTime,1,:)))));
%     for i = 1:7 
%         figure
%         imagesc(squeeze(perfImages(subject).on(:,:,echoTime,1,i))./scalingfact);
%         colormap jet
%         saveas(gcf,sprintf('../fig/forgif/Subject%dimg%d.png', subject, i));
%     end
% end






