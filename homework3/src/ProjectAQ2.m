%% Advanced Question 2
% IV EC IC

% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();


% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

Nboot = 50;

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
                bootstrapIndex = 1:10;
               
                % Perfusion Weighted
                imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k)); 
                %1 - squeeze(perfImages(i).on(:,:,j,1,k))./squeeze(perfImages(i).off(:,:,j,1,k));
                voxelsMeanASLON = imgTmpOn(roiCurrent);
                meanCorticalASL.on(i,j,k) = sum(voxelsMeanASLON(bootstrapIndex)) / noPixels;

                imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
                voxelsMeanASLOFF = imgTmpOff(roiCurrent);
                meanCorticalASL.off(i,j,k) = sum(voxelsMeanASLOFF(bootstrapIndex)) / noPixels;

                % Control images
                imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
                voxelsMeanCtrlON = imgTmpControlOn(roiCurrent);
                meanControlSignal.on(i,j,k) = sum(voxelsMeanCtrlON(bootstrapIndex)) / noPixels;

                imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
                voxelsMeanCtrlOFF = imgTmpControlOff(roiCurrent);
                meanControlSignal.off(i,j,k) = sum(voxelsMeanCtrlOFF(bootstrapIndex)) / noPixels;
            
           
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1);
    
end


subject = 1;
colorspec = {[1 0 0]; [0 1 0]; [0 0 1]; ...
  [0.5 0.5 0]; [0 0.5 0.5]; [0.5 0 0.5] ; [0.2 0.5 0.8]};

meanCorticalASLON   = meanCorticalASL.on;
meanCorticalASLOFF  = meanCorticalASL.off;
meanCorticalCTRLON  = meanControlSignal.on;
meanCorticalCTRLOFF = meanControlSignal.off;
%save('q2bootstrap', 'meanCorticalASLON', 'meanCorticalASLOFF', 'meanCorticalCTRLON', 'meanCorticalCTRLOFF');


% for i = 2:9
%     if i == 1
%         meanCorticalASL.on(1,:,:) = meanCorticalASL.on(1,:,:)./scalingFactorASL.on(i);
%         meanCorticalASL.off(1,:,:) = meanCorticalASL.off(1,:,:)./scalingFactorASL.off(i);
%         meanControlSignal.on(1,:,:) = meanControlSignal.on(1,:,:)./scalingFactorControl.on(i);
%         meanControlSignal.off(1,:,:) = meanControlSignal.off(1,:,:)./scalingFactorControl.off(i);
%     else
%         meanCorticalASL.on(1,:,:) = meanCorticalASL.on(1,:,:) + meanCorticalASL.on(i,:,:)./scalingFactorASL.on(i);
%         meanCorticalASL.off(1,:,:) = meanCorticalASL.off(1,:,:) + meanCorticalASL.off(i,:,:)./scalingFactorASL.off(i);
%         meanControlSignal.on(1,:,:) = meanControlSignal.on(1,:,:) + meanControlSignal.on(i,:,:)./scalingFactorControl.on(i);
%         meanControlSignal.off(1,:,:) = meanControlSignal.off(1,:,:) + meanControlSignal.off(i,:,:)./scalingFactorControl.off(i);
%     end
% end
% meanCorticalASL.on(1,:,:) = meanCorticalASL.on(1,:,:)./9;
% meanCorticalASL.off(1,:,:) = meanCorticalASL.off(1,:,:)./9;
% meanControlSignal.on(1,:,:) = meanControlSignal.on(1,:,:)./9;
% meanControlSignal.off(1,:,:) = meanControlSignal.off(1,:,:)./9;



% IV
IV = 1 - squeeze(meanCorticalASL.on(:,1,:))./squeeze(meanCorticalASL.off(:,1,:));
IVctrl = 1 - squeeze(meanControlSignal.on(:,1,:))./squeeze(meanControlSignal.off(:,1,:));

% IC
icec = load('CQ4icec', 'meanICEC', 'stdICEC');
phiEC = (1 - IV) ./ (repmat(icec.meanICEC, [9 1]) + 1);
phiIC = repmat(icec.meanICEC, [9 1]) .* phiEC;

% EC
icecctrl = load('CQ4icecctrl', 'meanICEC', 'stdICEC');
phiECctrl = (1 - IV) ./ (repmat(icecctrl.meanICEC, [9 1]) + 1);
phiICctrl = repmat(icecctrl.meanICEC, [9 1]) .* phiECctrl;



figure
errorbar(mean(phiIC), std(phiIC), 'b')
hold on
errorbar(mean(phiICctrl), std(phiICctrl), 'b--')
hold on
errorbar(mean(phiEC), std(phiEC), 'k')
hold on
errorbar(mean(phiECctrl), std(phiECctrl), 'k--')
hold on
errorbar(mean(IV,1), std(IV), 'r')
hold on
errorbar(mean(IVctrl,1), std(IVctrl), 'r--')

legend('\phi_{IC}(\DeltaM)', '\phi_{IC}(Ctrl)', '\phi_{EC}(\DeltaM)', '\phi_{EC}(Ctrl)', '\phi_{IV}(\DeltaM)', '\phi_{IV}(Ctrl)',...
    'Location','northoutside','Orientation','horizontal');
ax = gca;
ax.XTick = (1:7);
ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
xlabel('PLD ms / \tau ms');
ylabel('Proportion of ASL and Control signal');

%errorbar(mean(IV,1), std(IV))


























