function [images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData()
    
    % Data Dir
    addpath ../data
    names = dir('../data');
    
    % Create structure of data from all 9 files
    images = repmat(struct, [9,1]); 
    
    for i = 3:length(names)
    
        % Get data from files:
        load(names(i).name);
    
        % Load data into structures:
        images(i-2).on  = new_images_on;
        images(i-2).off = new_images_off;

    end
    
    % Parameters
    [durations echoTimes] = params();
    noSubj     = length(images);
    sizeImages = size(images(1).on);
    noEcho     = sizeImages(3);
    noTagPost  = sizeImages(5);

end