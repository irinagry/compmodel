function [PLD, TAG, TE] = prepareParamsForFit(n, durations, echoTimes)
    PLD = zeros(n, 1);
    TAG = zeros(n, 1);
    for i = 1:n
        pos    = mod(i-1,7) + 1;
        PLD(i) = durations(pos).pld;
        TAG(i) = durations(pos).tag;
    end
    
%     size(PLD)
%     figure, scatter((1:12*7*2), PLD(1:12*7*2))
%     figure, scatter((1:12*7*2), TAG(1:12*7*2))
    
    TE = zeros(n/9, 1);
    for i = 1:7:n/9
        TE(i:(i+6)) = echoTimes((i+6)/7);
    end
    TE = repmat(TE, [9 1]);
%     figure, scatter((1:12*7*2), TE(1:12*7*2));
end