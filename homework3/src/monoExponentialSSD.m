function [sumRes] = monoExponentialSSD(x, Avox, TE)
  
% Extract parameters
S0 = x(1);
T2 = x(2);

S = S0 .* exp(-TE./T2);

% Compute the sum of the square differences
sumRes = sum((Avox - S).^2);

end