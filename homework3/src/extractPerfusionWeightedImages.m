function perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost)
    
    % The perfusion weighted images
    perfImages = repmat(struct, [9,1]);

    for i = 1:noSubj
        
        for j = 1:noEcho

            for k = 2:2:noTagPost
                
                % The gradient on images
                perfImages(i).on(:,:,j,1,k/2) = ...
                    squeeze(images(i).on(:,:,j,1,k)) - ...
                    squeeze(images(i).on(:,:,j,1,k-1)) ;

                % The gradient off images
                perfImages(i).off(:,:,j,1,k/2) = ...
                    squeeze(images(i).off(:,:,j,1,k)) - ...
                    squeeze(images(i).off(:,:,j,1,k-1)) ;
                
            end
            
        end
        
    end

end