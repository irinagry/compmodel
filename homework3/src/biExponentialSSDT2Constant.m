function [sumRes] = biExponentialSSDT2Constant(x, Avox, TE)
  
% Extract parameters
S0IC = x(1);
S0EC = x(2);
T2IC = 53;
T2EC = 133;

S = S0IC .* exp(-TE./T2IC) + S0EC .* exp(-TE./T2EC);

% Compute the sum of the square differences
sumRes = sum((Avox - S).^2);

end