function [rois] = createROIs(perfImages, tau, toPlot)
    
    roi1 = roipoly(perfImages(1).on(:,:,1,1,tau), [33 38 38 33 33], [16 18 20 18 16]); % Higher?
    roi2 = roipoly(perfImages(2).on(:,:,1,1,tau), [33 38 38 33 33], [14 16 18 16 14]);
    roi3 = roipoly(perfImages(3).on(:,:,1,1,tau), [35 40 40 35 35], [16 18 20 18 16]); % More to the right?
    roi4 = roipoly(perfImages(4).on(:,:,1,1,tau), [33 38 38 33 33], [14 16 18 16 14]);
    roi5 = roipoly(perfImages(5).on(:,:,1,1,tau), [32 37 37 32 32], [12 14 16 14 12]);
    roi6 = roipoly(perfImages(6).on(:,:,1,1,tau), [32 37 37 32 32], [11 13 15 13 11]);
    roi7 = roipoly(perfImages(7).on(:,:,1,1,tau), [32 37 37 32 32], [14 16 18 16 14]);
    roi8 = roipoly(perfImages(8).on(:,:,1,1,tau), [32 37 37 32 32], [10 12 14 12 10]);
    roi9 = roipoly(perfImages(9).on(:,:,1,1,tau), [33 38 38 33 33], [11 13 15 13 11]);
    
    rois = [roi1 roi2 roi3 roi4 roi5 roi6 roi7 roi8 roi9];
    
    if strcmp(toPlot, 'yes') == 1
        % 1
        figure
        imgtmp = squeeze(perfImages(1).on(:,:,1,1,tau));
        imgtmp(roi1) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([33 38; 38 38 ; 38 33; 33 33], [16 18; 18 20; 20 18; 18 16]);
        title('Subject 1 ROI');

        % 2
        figure
        imgtmp = squeeze(perfImages(2).on(:,:,1,1,tau));
        imgtmp(roi2) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([33 38; 38 38 ; 38 33; 33 33], [14 16; 16 18; 18 16; 16 14]);
        title('Subject 2 ROI');

        % 3
        figure
        imgtmp = squeeze(perfImages(3).on(:,:,1,1,tau));
        imgtmp(roi3) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([35 40; 40 40 ; 40 35; 35 35], [16 18; 18 20; 20 18; 18 16]);
        title('Subject 3 ROI');

        % 4
        figure
        imgtmp = squeeze(perfImages(4).on(:,:,1,1,tau));
        imgtmp(roi4) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([33 38; 38 38 ; 38 33; 33 33], [14 16; 16 18; 18 16; 16 14]);
        title('Subject 4 ROI');

        % 5
        figure
        imgtmp = squeeze(perfImages(5).on(:,:,1,1,tau));
        imgtmp(roi5) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([32 37; 37 37 ; 37 32; 32 32], [12 14; 14 16; 16 14; 14 12]);
        title('Subject 5 ROI');

        % 6
        figure
        imgtmp = squeeze(perfImages(6).on(:,:,1,1,tau));
        imgtmp(roi6) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([32 37; 37 37 ; 37 32; 32 32], [11 13; 13 15; 15 13; 13 11]);
        title('Subject 6 ROI');

        % 7
        figure
        imgtmp = squeeze(perfImages(7).on(:,:,1,1,tau));
        imgtmp(roi7) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([32 37; 37 37 ; 37 32; 32 32], [14 16; 16 18; 18 16; 16 14]);
        title('Subject 7 ROI');

        % 8
        figure
        imgtmp = squeeze(perfImages(8).on(:,:,1,1,tau));
        imgtmp(roi8) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([32 37; 37 37 ; 37 32; 32 32], [10 12; 12 14; 14 12; 12 10]);
        title('Subject 8 ROI');

        % 9
        figure
        imgtmp = squeeze(perfImages(9).on(:,:,1,1,tau));
        imgtmp(roi9) = 0;
        imagesc(imgtmp), colormap gray 
        hold on
        plot([33 38; 38 38 ; 38 33; 33 33], [11 13; 13 15; 15 13; 13 11]);
        title('Subject 9 ROI');
    end
    
end