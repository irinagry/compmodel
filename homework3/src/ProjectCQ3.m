%% ProjectCQ3
% Mono-exponential fitting of the transverse decay (signal vs echo time)

% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();

% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
            % Perfusion Weighted
            imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k));
            meanCorticalASL.on(i,j,k) = sum(imgTmpOn(roiCurrent)) / noPixels;
        
            imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
            meanCorticalASL.off(i,j,k) = sum(imgTmpOff(roiCurrent)) / noPixels;
            
            % Control images
            imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
            meanControlSignal.on(i,j,k) = sum(imgTmpControlOn(roiCurrent)) / noPixels;
            
            imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
            meanControlSignal.off(i,j,k) = sum(imgTmpControlOff(roiCurrent)) / noPixels;
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1);
    
    % Scaling
    for k = 1:noTagPost/2
        meanCorticalASL.on(i,:,k) = meanCorticalASL.on(i,:,k)./scalingFactorASL.on(i);
        meanCorticalASL.off(i,:,k) = meanCorticalASL.off(i,:,k)./scalingFactorASL.off(i);
        meanControlSignal.on(i,:,k) = meanControlSignal.on(i,:,k)./scalingFactorControl.on(i);
        meanControlSignal.off(i,:,k) = meanControlSignal.off(i,:,k)./scalingFactorControl.off(i);
    end
    
end


fittype = 'ASL';
grads = 'on';
caseVal = strcat(fittype,grads);

N = 15; % number of initial runs

switch fittype
    case 'ASL'
        % Initial Params
        S0ini =   2.5 + 5.*randn(N,1);
        T2ini =  90 +  20.*randn(N,1);
        lb = [  -7,   0];
        ub = [   8, 500];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ini =  50 +  50.*randn(N,1);
        T2ini =  90 +  20.*randn(N,1);
        lb = [   0,   0];
        ub = [ 100, 500];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end


% Define multiple starting points
initialPoints = [S0ini T2ini];
finalPoints   = zeros(9,7,2);

% Parametric Bootstrap
T = 50;
K = 12; N = 2;
% Values stored after parametric boostraping
mS0 = zeros(9,T,7);
mT2 = zeros(9,T,7);
RESNORMCounter = zeros(9,T,7);
    
predictedS = zeros(9,7,12);

for ratNo = 1:9

    
    % For each rat compute for each pldtau combination
    fprintf('=====>\n%s mono-exponential, grads %s, Rat %d\n\n', fittype, grads, ratNo);

    for pldtau = 1:7
        
        switch caseVal
            
            case 'ASLon'    
                Avox = squeeze(meanCorticalASL.on(ratNo, :, pldtau));
            case 'ASLoff'
                Avox = squeeze(meanCorticalASL.off(ratNo, :, pldtau));
            case 'Controlon'
                Avox = squeeze(meanControlSignal.on(ratNo, :, pldtau));
            case 'Controloff'
                Avox = squeeze(meanControlSignal.off(ratNo, :, pldtau));
                
        end
                
        evalFuncAvox = @(x) monoExponentialSSD(x, Avox, echoTimes);
        
        RESNORMMIN = 1e+30; % high value
        %tic
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

            try
                % Define a starting point for the non-linear fit
                startx = initialPoints(i,:);

                % Run the fitting
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFuncAvox, startx, A, b, Aeq, beq, lb, ub, [], h);

                RESNORMCounter(ratNo, 1, pldtau) = RESNORM;


            catch ME
                warning(ME.message);
                disp(ME.message);

                RESNORM = 1e+30; % high value
                RESNORMMIN = 1e+30;
                RESNORMCounter(ratNo, 1, pldtau) = RESNORM;
            end

            if RESNORM < RESNORMMIN
                finalPoints(ratNo, pldtau, :) = parameter_hat;
                RESNORMMIN = RESNORM;
                goodInitialPoints = startx;

            end
        end
        %toc
        
        [predictedSini] = monoExponential(squeeze(finalPoints(ratNo, pldtau, :)), echoTimes);

        residuals = sum((Avox - predictedSini).^2);
        noise = (1/(K - N)) * residuals;
        noiseModel = sqrt(noise).*randn(T, K);
        
        % Parametric Bootstrap
        Sperturbed = repmat(predictedSini, [T, 1]) + noiseModel;
        
        % Plot Avox for the current pld tau combination
%         figure
%         plot(echoTimes, Avox, 'rs');
%         hold on
        
        for t = 1 : T
            
            evalFunc = @(x) monoExponentialSSD(x, Sperturbed(t,:), echoTimes);
            
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

            try
                % Define a starting point for the non-linear fit
                startx = goodInitialPoints;

                % Run the fitting
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFuncAvox, startx, A, b, Aeq, beq, lb, ub, [], h);

                RESNORMCounter(ratNo, t, pldtau) = RESNORM;


            catch ME
                warning(ME.message);
                disp(ME.message);

                RESNORM = 1e+30; % high value
                RESNORMMIN = 1e+30;
                RESNORMCounter(ratNo, t, pldtau) = RESNORM;
            end

            if RESNORM < RESNORMMIN
                finalPoints(ratNo, pldtau, :) = parameter_hat;
                RESNORMMIN = RESNORM;
            end
            
            % For the parametric bootstrap I keep all the values for S0 and
            % T2 found for all T perturbed signals
            % The last one is without perturbing the signal
            mS0(ratNo, t, pldtau) = finalPoints(ratNo, pldtau, 1);
            mT2(ratNo, t, pldtau) = finalPoints(ratNo, pldtau, 2);
            
        end
        
        % What I got
        fprintf('PLD %d / TAU %d, RESNORMIN = %f \n', ...
                durations(pldtau).pld, durations(pldtau).tag, mean(RESNORMCounter(ratNo, :, pldtau)));
        fprintf('Final Points: S0 = %.3f T2 = %.3f\n\n', mean(mS0(ratNo, :, pldtau)), mean(mT2(ratNo, :, pldtau)));
                        
%         titleFigure = sprintf(['Rat %d\nPLD = %d, Tau = %d\n',...
%                         'Final Points: M0 = %.3f T2 = %.3f\n', ...
%                         'Measured vs Predicted Values', ...
%                         '(%s mono-exponential)\n grads %s'],...
%                         ratNo, durations(pldtau).pld, durations(pldtau).tag, ...
%                         mean(mS0(ratNo, :, pldtau)), mean(mT2(ratNo, :, pldtau)),...
%                         fittype, grads);
%                         
%         
%         [predictedS(ratNo,pldtau,:)] = monoExponential(squeeze(finalPoints(ratNo, pldtau, :)), echoTimes);
%         plot(echoTimes, squeeze(predictedS(ratNo,pldtau,:)), 'b*');
%         xlabel('TE'); ylabel('Signal');
%         legend('Data', 'Predicted Signal');
%         title(titleFigure);
        
    end
    fprintf('\n<=====\n\n');
        
end


%%
% % For each pldtau combination you can print out the histogram
% for pldtau = 1:7
%     figure
%     subplot(1,2,1)
%     histfit(mS0(ratNo,1:T,pldtau));
%     title(sprintf('Histogram for S0 for PLD = %d TAU = %d', durations(pldtau).pld, durations(pldtau).tag));
%     
%     subplot(1,2,2)
%     histfit(mT2(ratNo,1:T,pldtau));
%     title(sprintf('Histogram for T2 for PLD = %d TAU = %d', durations(pldtau).pld, durations(pldtau).tag));
% end


%% Print out mean of S0 and mean of T2 and confidence levels for each pldtau 
meanS0 = zeros(9,7); stdS0 = zeros(9,7); 
meanT2 = zeros(9,7); stdT2 = zeros(9,7); 
meanRes = zeros(9,7); stdRes = zeros(9,7); 
figure
for ratNo = 1:9
    
    mS0Rat = squeeze(mS0(ratNo, 1:T, :)); % lines = T, columns = pldtau combs
    mT2Rat = squeeze(mT2(ratNo, 1:T, :)); 
    mResnormRat = squeeze(RESNORMCounter(ratNo, 1:T, :)); 
    
    for pldtau = 1:7
    
        meanS0(ratNo, pldtau) = mean(mS0Rat(:, pldtau));
        stdS0( ratNo, pldtau) =  std(mS0Rat(:, pldtau));

        meanT2(ratNo, pldtau) = mean(mT2Rat(:, pldtau));
        stdT2( ratNo, pldtau) =  std(mT2Rat(:, pldtau));
        
        meanRes(ratNo, pldtau) = mean(mResnormRat(:, pldtau));
        stdRes( ratNo, pldtau) =  std(mResnormRat(:, pldtau));

        fprintf('RAT %d, pld = %d, tau = %d, \n---->meanS0 = %.3f, stdS0 = %.3f, meanT2 = %.3f, stdT2 = %.3f\n\n', ...
            ratNo, durations(pldtau).pld, durations(pldtau).tag, ...
            meanS0(ratNo, pldtau), stdS0(ratNo, pldtau), ...
            meanT2(ratNo, pldtau), stdT2(ratNo, pldtau));

    end
    
    fprintf('\n\n\n');
    
    % Plot error bars
%     errorbar(1:7, meanT2(ratNo, :), stdT2(ratNo, :)); % T2
%     errorbar(1:7, meanS0(ratNo, :), stdS0(ratNo, :)); % S0
%     errorbar(1:7, meanRes(ratNo, :), stdRes(ratNo, :)); % Resnorm

    hold on
    pause(0.5)
    
end
save('meanResMonoexp', 'meanRes')


%%
figure
boxplot((meanRes(:, :)))%, mean(stdRes(:, :))); % Resnorm

plotThing = 'S0';

xlabel('PLD/TAU combination'); ylabel(plotThing);
ax = gca;
ax.XTick = (1:7);
ax.XTickLabel = {[num2str(durations(1).pld), '/', num2str(durations(1).tag)],...
                 [num2str(durations(2).pld), '/', num2str(durations(2).tag)],...
                 [num2str(durations(3).pld), '/', num2str(durations(3).tag)],...
                 [num2str(durations(4).pld), '/', num2str(durations(4).tag)],...
                 [num2str(durations(5).pld), '/', num2str(durations(5).tag)],...
                 [num2str(durations(6).pld), '/', num2str(durations(6).tag)],...
                 [num2str(durations(7).pld), '/', num2str(durations(7).tag)]};
ax.XTickLabelRotation = 45;
title(sprintf(['Mono-exponential Fitting of the Transverse Decay\n',...
               '%s for all PLD/TAU combinations'], plotThing));

% legend('Rat 1', 'Rat 2', 'Rat 3', 'Rat 4', 'Rat 5', ...
%     'Rat 6', 'Rat 7', 'Rat 8', 'Rat 9', 'Location','northwest');
legend('Subjects');
return
saveas(gcf,sprintf('../fig/CQ3_Mono%s_%sAvg.png', fittype, plotThing));


%%
% 
% for pldtau = 1:7
%     meanByPld(pldtau) = mean(meanRes(:, pldtau));
%     stdByPld(pldtau) = mean(stdRes(:, pldtau));
%     boxplot(meanRes(:, pldtau), stdRes(:, pldtau)); % Resnorm
% end
% 
% figure
% boxplot(meanByPld, stdByPld)





