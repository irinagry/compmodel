function parametricBootstrapTest()

%% 3.2 Parametric Bootstrap

switch fittype
    case 'ASL'
        % Initial Params
        S0ini =   2.5 + 5.*randn(N,1);
        T2ini = 250 + 250.*randn(N,1);
        lb = [  -1,    0];
        ub = [   5, 1000];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ini =  50 +  50.*randn(N,1);
        T2ini = 250 + 250.*randn(N,1);
        lb = [   0,    0];
        ub = [ 100, 1000];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end


% Define multiple starting points
initialPoints = [S0ini T2ini];
finalPoints   = zeros(1,2);

for ratNo = 1:1

    RESNORMMIN = 1e+30; % high value
    RESNORMCounter = zeros(N,1);
    
    % Parametric Bootstrap
    T = 100;
    K = 84; N = 2;
    % Values stored after parametric boostraping
    mS0 = zeros(1,T);
    mT2 = zeros(1,T);
    
    switch fittype
        
        case 'ASL'
            
            switch grads
                case 'on'
                residuals = sum((AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = sqrt(noise).*randn(T, 84);
                %Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
                Sperturbed = repmat(AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :)', [T,1]) + noiseModel;
        
                case 'off'
                residuals = sum((AvoxASL.off(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = sqrt(noise).*randn(T, 84);
                Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
            end
            
        case 'Control'
            
            switch grads
                case 'on'                
                residuals = sum((AvoxControl.on(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = sqrt(noise).*randn(T, 84);
                Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
        
                case 'off'
                residuals = sum((AvoxControl.off(((ratNo-1)*84+1):(ratNo*84), :) - predictedS(ratNo,:)').^2);
                noise = (1/(K - N)) * residuals;
                noiseModel = sqrt(noise).*randn(T, 84);
                Sperturbed = repmat(predictedS(ratNo,:),[T,1]) + noiseModel;
            end
            
    end
    
    tic
    for paramBoot = 1:T
        
        % Prepare the function to be evaluated
        evalFunc = @(x) monoExponentialSSD(x, ...
                        Sperturbed(paramBoot,:)', TE(((ratNo-1)*84+1):(ratNo*84), :));
        
        % Evaluate function
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

            try
                % Define a starting point for the non-linear fit
                startx = initialPoints(i,:);

                % Run the fitting
                [parameter_hat_boot, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);

                RESNORMCounter(i) = RESNORM;

            catch ME
                warning(ME.message);
                disp(ME.message);

                RESNORM = 1e+30; % high value
                RESNORMMIN = 1e+30;
                RESNORMCounter(i) = RESNORM;
            end

            if RESNORM < RESNORMMIN
                finalPoints = parameter_hat_boot;
                RESNORMMIN = RESNORM;
                
                mS0(paramBoot) = parameter_hat_boot(1);
                mT2(paramBoot) = parameter_hat_boot(2);
            end
        end
    end
    toc
    
%     % Show histograms
%     figure
%     % subplot 1 S0 histogram
%     subplot(1,2,1)
%     histfit(mS0, T/5)
%     % subplot 2 T2 histogram
%     subplot(1,2,2)
%     histfit(mT2, T/5)

    mS0sorted = sort(mS0);
    mT2sorted = sort(mT2);

    %%%%%%%%%% RANGES
    % S0
    % 2 sigma range:
    S02sigmaL=mean(mS0)-2*std(mS0);
    S02sigmaU=mean(mS0)+2*std(mS0);
    % 95% range
    S095L = prctile(mS0sorted,2.5);
    S095U = prctile(mS0sorted,97.5);
    
    % T2
    % 2 sigma range:
    T22sigmaL=mean(mT2)-2*std(mT2);
    T22sigmaU=mean(mT2)+2*std(mT2);
    % 95% range
    T295L = prctile(mT2sorted,2.5); 
    T295U = prctile(mT2sorted,97.5);
    
    fprintf('\n%s grads %s for Rat %d S0\n2 sigma: [%f, %f]\n95%%: [%f, %f]\n',...
        fittype, grads, ratNo, S02sigmaL, S02sigmaU, S095L, S095U);
    
    fprintf('\n%s grads %s for Rat %d T2\n2 sigma: [%f, %f]\n95%%: [%f, %f]\n',...
        fittype, grads, ratNo, T22sigmaL, T22sigmaU, T295L, T295U);
    
%     % Compare predictions with actual measurements:
%     figure
%     for i = 2:T
%         plot(Sperturbed(i,:),'rs'); % perturbed data
%         hold on
%     end
%     switch fittype
%         case 'ASL'
%             [predictedS] = monoExponential(finalPoints, TE(((ratNo-1)*84+1):(ratNo*84), :));
%             switch grads
%                 case 'on'
%                     plot(AvoxASL.on(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
%                 case 'off'
%                     plot(AvoxASL.off(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
%             end
% 
%         case 'Control'
%             [predictedS] = monoExponential(finalPoints, TE(((ratNo-1)*84+1):(ratNo*84), :));
%             switch grads
%                 case 'on'
%                     plot(AvoxControl.on(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
%                 case 'off'
%                     plot(AvoxControl.off(((ratNo-1)*84+1):(ratNo*84), :),'bs'); % measurements
%             end
%     end
%     hold on
%     plot(predictedS,'gx');
    
end


end
