%% ProjectCQ4
% Bi-exponential fitting of the transverse decay

% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();

% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
            % Perfusion Weighted
            imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k));
            meanCorticalASL.on(i,j,k) = sum(imgTmpOn(roiCurrent)) / noPixels;
        
            imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
            meanCorticalASL.off(i,j,k) = sum(imgTmpOff(roiCurrent)) / noPixels;
            
            % Control images
            imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
            meanControlSignal.on(i,j,k) = sum(imgTmpControlOn(roiCurrent)) / noPixels;
            
            imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
            meanControlSignal.off(i,j,k) = sum(imgTmpControlOff(roiCurrent)) / noPixels;
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1);
    
    
%     % Scaling
%     for k = 1:noTagPost/2
%         meanCorticalASL.on(i,:,k) = meanCorticalASL.on(i,:,k)./scalingFactorASL.on(i);
%         meanCorticalASL.off(i,:,k) = meanCorticalASL.off(i,:,k)./scalingFactorASL.off(i);
%         meanControlSignal.on(i,:,k) = meanControlSignal.on(i,:,k)./scalingFactorControl.on(i);
%         meanControlSignal.off(i,:,k) = meanControlSignal.off(i,:,k)./scalingFactorControl.off(i);
%     end
    
end


fittype = 'ASL';
grads = 'on'; % always on
T2Constant = 'on'; % on == T2 given, off == T2 estimated
caseVal = strcat(fittype,T2Constant);

N = 15; % number of initial runs

switch fittype
    case 'ASL'
        % Initial Params
        S0ICini =   2.5 + 5.*randn(N,1);
        S0ECini =   2.5 + 5.*randn(N,1);
        T2ICini = 100 + 100.*randn(N,1);
        T2ECini = 100 + 100.*randn(N,1);
        lb = [  -5, -5,    0,    0];
        ub = [   5,  5,  200,  200];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ICini =  50 +  50.*randn(N,1);
        S0ECini =  50 +  50.*randn(N,1);
        T2ICini = 100 + 100.*randn(N,1);
        T2ECini = 100 + 100.*randn(N,1);
        lb = [   0,   0,    0,    0];
        ub = [ 100, 100,  200,  200];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end

if strcmp(T2Constant, 'on')
    lb = lb(1:2);
    ub = ub(1:2);
    % Define multiple starting points
    initialPoints = [S0ICini S0ECini];
    finalPoints   = zeros(9,7,2);
else
    % Define multiple starting points
    initialPoints = [S0ICini S0ECini T2ICini T2ECini];
    finalPoints   = zeros(9,7,4);    
end

% Parametric Bootstrap
T = 25;
K = 12; N = 5;
% Values stored after parametric boostraping
mS0IC = zeros(9,T,7);
mS0EC = zeros(9,T,7);
mT2IC = zeros(9,T,7);
mT2EC = zeros(9,T,7);

predictedS = zeros(9,7,12,T);

for ratNo = 1:1
    
    RESNORMCounter = zeros(7,1);
    
    if strcmp(T2Constant, 'on')
        residuals = 0.241632^2;
    else
        residuals = 0.194650^2;
    end
    noise = (1/(K - N)) * residuals;
    noiseModel = sqrt(noise).*randn(T, K);
    
    figure
    for pldtau = 1:7
        
        RESNORMMIN = 1e+30; % high value
        
        switch caseVal
            
            case 'ASLon'    
                Avox = squeeze(meanCorticalASL.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Constant(x, AvoxP, echoTimes);
            case 'ASLoff'
                Avox = squeeze(meanCorticalASL.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Estimated(x, AvoxP, echoTimes);
            case 'Controlon'
                Avox = squeeze(meanControlSignal.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Constant(x, AvoxP, echoTimes);
            case 'Controloff'
                Avox = squeeze(meanControlSignal.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Estimated(x, AvoxP, echoTimes);
                
        end
        
        % Parametric Bootstrap
        Sperturbed = repmat(Avox, [T,1]) + noiseModel;
        
        for t = 1 : T
        
            evalFunc = @(x) evalFunc0(x, Sperturbed(t,:));
            
            %tic
            for i = 1:N
                % Define various options for the non-linear fitting algorithm
                h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                             'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

                try
                    % Define a starting point for the non-linear fit
                    startx = initialPoints(i,:);

                    % Run the fitting
                    [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                        fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);

                    RESNORMCounter(pldtau) = RESNORM;

                catch ME
                    warning(ME.message);
                    disp(ME.message);

                    RESNORM = 1e+30; % high value
                    RESNORMMIN = 1e+30;
                    RESNORMCounter(pldtau) = RESNORM;
                end

                if RESNORM < RESNORMMIN
                    finalPoints(ratNo, pldtau, :) = parameter_hat;
                    RESNORMMIN = RESNORM;

                end
            end
            %toc
        
            mS0IC(ratNo, t, pldtau) = parameter_hat(1);
            mS0EC(ratNo, t, pldtau) = parameter_hat(2);
            if strcmp(T2Constant, 'off')
                mT2IC(ratNo, t, pldtau) = parameter_hat(3);
                mT2EC(ratNo, t, pldtau) = parameter_hat(4);
            end
        
            % The predicted signal
            if strcmp(T2Constant, 'on')
                [predictedS(ratNo,pldtau,:,t)] = biExponentialT2Constant(finalPoints(ratNo,pldtau,:), echoTimes);
            else
                [predictedS(ratNo,pldtau,:,t)] = biExponentialT2Estimated(finalPoints(ratNo,pldtau,:), echoTimes);
            end
        
%         plot(echoTimes, Avox, 'bs'); % measurements
%         hold on
%         plot(echoTimes, squeeze(predictedS(ratNo,pldtau,:)), 'rx'); % predictions
%         hold on
        
        end
        
    end
    
    if strcmp(T2Constant,'on')
        t2cst = 'given';
    else
        t2cst = 'estimated';
    end
    
    % What I got
    fprintf('=====>\n%s bi-exponential, T2 %s, grads %s Rat %d RESNORMIN = %f \n', fittype, t2cst, grads, ratNo, min(RESNORMCounter));
    fprintf('Final Points\n'); 
    if strcmp(T2Constant,'on')
        %S0IC = mean(squeeze(finalPoints(ratNo,:,1)))
        %S0EC = mean(squeeze(finalPoints(ratNo,:,2)))
        S0IC = mean(mean(mS0IC(ratNo, :, :)))
        S0EC = mean(mean(mS0EC(ratNo, :, :)))
    else
        %S0IC = mean(squeeze(finalPoints(ratNo,:,1)))
        %S0EC = mean(squeeze(finalPoints(ratNo,:,2)))
        %T2IC = mean(squeeze(finalPoints(ratNo,:,3)))
        %T2EC = mean(squeeze(finalPoints(ratNo,:,4)))
        S0IC = mean(mean(mS0IC(ratNo, :, :)))
        S0EC = mean(mean(mS0EC(ratNo, :, :)))
        T2IC = mean(mean(mT2IC(ratNo, :, :)))
        T2EC = mean(mean(mT2EC(ratNo, :, :)))
    end
    fprintf('\n<=====\n\n');
    
    titleFigure = sprintf(['Measured vs Predicted Values', ...
                        '(%s bi-exponential T2 %s)\n grads %s for Rat %d'],...
                        fittype, t2cst, grads, ratNo);
    
    
    % Plot mean signal for each echo time
    for i = 1:12
        signalPerEcho = squeeze(predictedS(ratNo, :, i, :));
        plot(echoTimes(i), mean(mean(signalPerEcho)), 'gs');
        if strcmp(T2Constant, 'off')
            estimatedSignalCQ4off(i) = mean(mean(signalPerEcho));
        else
            estimatedSignalCQ4on(i) = mean(mean(signalPerEcho));
        end
        errorbar(echoTimes(i), mean(mean(signalPerEcho)), std(std(signalPerEcho)));
        hold on
    end
    legend('Confidence Interval', 'Mean Predicted Signal');
    title(titleFigure);
    xlabel('TE [ms]');
    ylabel('Signal');
    
end

%figure, histfit(mS0IC(1,:,1))

%% 
% 1. ASL mono-exponential, grads on Rat 1 RESNORMIN = 0.245591 
% 2. ASL bi-exponential, T2 given, grads on Rat 1 RESNORMIN = 0.241632 
% 3. ASL bi-exponential, T2 estimated, grads on Rat 1 RESNORMIN = 0.194650 

% figure
% for i = 1:12
%     plot(echoTimes(i), estimatedSignalCQ3(i), 'rs');
%     hold on
%     plot(echoTimes(i), estimatedSignalCQ4off(i), 'gs');
%     hold on
%     plot(echoTimes(i), estimatedSignalCQ4on(i), 'bs');
% end














