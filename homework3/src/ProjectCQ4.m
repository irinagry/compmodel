%% ProjectCQ4
% Bi-exponential fitting of the transverse decay (signal vs echo time)

% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();

% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
            % Perfusion Weighted
            imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k));
            meanCorticalASL.on(i,j,k) = sum(imgTmpOn(roiCurrent)) / noPixels;
        
            imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
            meanCorticalASL.off(i,j,k) = sum(imgTmpOff(roiCurrent)) / noPixels;
            
            % Control images
            imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
            meanControlSignal.on(i,j,k) = sum(imgTmpControlOn(roiCurrent)) / noPixels;
            
            imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
            meanControlSignal.off(i,j,k) = sum(imgTmpControlOff(roiCurrent)) / noPixels;
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1);
    
    % Scaling
    for k = 1:noTagPost/2
        meanCorticalASL.on(i,:,k) = meanCorticalASL.on(i,:,k)./scalingFactorASL.on(i);
        meanCorticalASL.off(i,:,k) = meanCorticalASL.off(i,:,k)./scalingFactorASL.off(i);
        meanControlSignal.on(i,:,k) = meanControlSignal.on(i,:,k)./scalingFactorControl.on(i);
        meanControlSignal.off(i,:,k) = meanControlSignal.off(i,:,k)./scalingFactorControl.off(i);
    end
    
end


fittype = 'Control';
grads = 'on';
T2Constant = 'on'; % on == T2 given, off == T2 estimated
caseVal = strcat(fittype,T2Constant);

N = 15; % number of initial runs

switch fittype
    case 'ASL'
        % Initial Params
        S0ICini =   2.5 + 5.*randn(N,1);
        S0ECini =   2.5 + 5.*randn(N,1);
        T2ICini =  90 +  20.*randn(N,1);
        T2ECini =  90 +  20.*randn(N,1);
        lb = [  -5, -5,    0,    0];
        ub = [   5,  5,  500,  500];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ICini =  50 +  50.*randn(N,1);
        S0ECini =  50 +  50.*randn(N,1);
        T2ICini =  90 +  20.*randn(N,1);
        T2ECini =  90 +  20.*randn(N,1);
        lb = [   0,   0,    0,    0];
        ub = [ 100, 100,  500,  500];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end


% Define multiple starting points
if strcmp(T2Constant, 'on')
    lb = lb(1:2);
    ub = ub(1:2);
    % Define multiple starting points
    initialPoints = [S0ICini S0ECini];
    finalPoints   = zeros(9,7,2);
else
    % Define multiple starting points
    initialPoints = [S0ICini S0ECini T2ICini T2ECini];
    finalPoints   = zeros(9,7,4);    
end

% Parametric Bootstrap
T = 50;
K = 12; N = 5;
% Values stored after parametric boostraping
mS0IC = zeros(9,T,7);
mS0EC = zeros(9,T,7);
mT2IC = zeros(9,T,7);
mT2EC = zeros(9,T,7);

predictedS = zeros(9,7,12);
RESNORMCounter = zeros(9,T,7);

for ratNo = 1:9
    
    if strcmp(T2Constant, 'on')
        fprintf('=====>\n%s Bi-exponential, grads %s, T2 given, Rat %d\n\n', ...
            fittype, grads, ratNo);
    else
        fprintf('=====>\n%s Bi-exponential, grads %s, T2 estimated, Rat %d\n\n', ...
            fittype, grads, ratNo);
    end
    
    % For each rat compute for each pldtau combination
    for pldtau = 1:7
        
        switch caseVal
            
            case 'ASLon'    
                Avox = squeeze(meanCorticalASL.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Constant(x, AvoxP, echoTimes);
            case 'ASLoff'
                Avox = squeeze(meanCorticalASL.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Estimated(x, AvoxP, echoTimes);
            case 'Controlon'
                Avox = squeeze(meanControlSignal.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Constant(x, AvoxP, echoTimes);
            case 'Controloff'
                Avox = squeeze(meanControlSignal.on(ratNo, :, pldtau));
                evalFunc0 = @(x, AvoxP) biExponentialSSDT2Estimated(x, AvoxP, echoTimes);
                
        end
              
        evalFuncAvox = @(x) evalFunc0(x, Avox);
        
        RESNORMMIN = 1e+30; % high value
        
        %tic
        for i = 1:N
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

            try
                % Define a starting point for the non-linear fit
                startx = initialPoints(i,:);

                % Run the fitting
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFuncAvox, startx, A, b, Aeq, beq, lb, ub, [], h);

                RESNORMCounter(ratNo, 1, pldtau) = RESNORM;

            catch ME
                warning(ME.message);
                disp(ME.message);

                RESNORM = 1e+30; % high value
                RESNORMMIN = 1e+30;
                RESNORMCounter(ratNo, 1, pldtau) = RESNORM;
            end

            if RESNORM < RESNORMMIN
                finalPoints(ratNo, pldtau, :) = parameter_hat;
                RESNORMMIN = RESNORM;
                goodInitialPoints = startx;
            end
        end
        %toc
        
        if strcmp(T2Constant, 'on')
            [predictedSini] = biExponentialT2Constant(squeeze(finalPoints(ratNo, pldtau, :)), echoTimes);
        else
            [predictedSini] = biExponentialT2Estimated(squeeze(finalPoints(ratNo, pldtau, :)), echoTimes);
        end
        
        residuals = sum((Avox - predictedSini).^2);
        noise = (1/(K - N)) * residuals;
        noiseModel = sqrt(noise).*randn(T, K);
        
        % Parametric Bootstrap
        Sperturbed = repmat(predictedSini, [T, 1]) + noiseModel;
        
        % Plot Avox for the current pld tau combination
%         figure
%         plot(echoTimes, Avox, 'rs');
%         hold on
        
        for t = 1 : T
            
            evalFunc = @(x) evalFunc0(x, Sperturbed(t,:));
        
            % Define various options for the non-linear fitting algorithm
            h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                         'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

            try
                % Define a starting point for the non-linear fit
                startx = goodInitialPoints;

                % Run the fitting
                [parameter_hat, RESNORM, EXITFLAG, OUTPUT] = ...
                    fmincon(evalFuncAvox, startx, A, b, Aeq, beq, lb, ub, [], h);

                RESNORMCounter(ratNo, t, pldtau) = RESNORM;

            catch ME
                warning(ME.message);
                disp(ME.message);

                RESNORM = 1e+30; % high value
                RESNORMMIN = 1e+30;
                RESNORMCounter(ratNo, t, pldtau) = RESNORM;
            end

            if RESNORM < RESNORMMIN
                finalPoints(ratNo, pldtau, :) = parameter_hat;
                RESNORMMIN = RESNORM;
            end

            % For the parametric bootstrap I keep all the values for S0 and
            % T2 found for all T perturbed signals
            % The last one is without perturbing the signal
            mS0IC(ratNo, t, pldtau) = finalPoints(ratNo, pldtau, 1);
            mS0EC(ratNo, t, pldtau) = finalPoints(ratNo, pldtau, 2);
            if strcmp(T2Constant, 'off')
                mT2IC(ratNo, t, pldtau) = parameter_hat(3);
                mT2EC(ratNo, t, pldtau) = parameter_hat(4);
            end
            
        end
        
        % What I got
        fprintf('PLD %d / TAU %d, RESNORMIN = %f \n', ...
                durations(pldtau).pld, durations(pldtau).tag, mean(RESNORMCounter(ratNo, :, pldtau)));
        fprintf('Final Points: S0IC = %.3f S0EC = %.3f', mean(mS0IC(ratNo, :, pldtau)), mean(mS0EC(ratNo, :, pldtau)));
        if strcmp(T2Constant, 'off')
            fprintf(' T2IC = %.3f T2EC = %.3f', mean(mT2IC(ratNo, :, pldtau)), mean(mT2EC(ratNo, :, pldtau)));
        end
        fprintf('\n\n');
        
        
        if strcmp(T2Constant, 'off')
            titleFigure = sprintf(['Rat %d\nPLD = %d, Tau = %d\n',...
                            'Final Points: S0IC = %.3f S0EC = %.3f T2IC = %.1f T2EC = %.1f\n', ...
                            'Measured vs Predicted Values', ...
                            '(%s bi-exponential T2 estimated)\n grads %s'],...
                            ratNo, durations(pldtau).pld, durations(pldtau).tag, ...
                            mean(mS0IC(ratNo, :, pldtau)), mean(mS0EC(ratNo, :, pldtau)),...
                            mean(mT2IC(ratNo, :, pldtau)), mean(mT2EC(ratNo, :, pldtau)),...
                            fittype, grads);
        else
            titleFigure = sprintf(['Rat %d\nPLD = %d, Tau = %d\n',...
                            'Final Points: S0IC = %.3f S0EC = %.3f T2IC = %.3f T2EC = %.3f\n', ...
                            'Measured vs Predicted Values', ...
                            '(%s bi-exponential T2 given)\n grads %s'],...
                            ratNo, durations(pldtau).pld, durations(pldtau).tag, ...
                            mean(mS0IC(ratNo, :, pldtau)), mean(mS0EC(ratNo, :, pldtau)),...
                            53, 133,...
                            fittype, grads);
        end
                        
        
        % The predicted signal
%         if strcmp(T2Constant, 'on')
%             [predictedS(ratNo,pldtau,:)] = biExponentialT2Constant(squeeze(finalPoints(ratNo, pldtau, :)), echoTimes);
%         else
%             [predictedS(ratNo,pldtau,:)] = biExponentialT2Estimated(squeeze(finalPoints(ratNo, pldtau, :)), echoTimes);
%         end
%         plot(echoTimes, squeeze(predictedS(ratNo,pldtau,:)), 'b*');
%         xlabel('TE'); ylabel('Signal');
%         legend('Data', 'Predicted Signal');
%         title(titleFigure);
        
    end
    fprintf('\n<=====\n\n');
    pause(0.5);
        
end


%% Print out mean of S0 and mean of T2 and confidence levels for each pldtau 
meanS0IC = zeros(9,7); stdS0IC = zeros(9,7); 
meanS0EC = zeros(9,7); stdS0EC = zeros(9,7); 
meanT2IC = zeros(9,7); stdT2IC = zeros(9,7); 
meanT2EC = zeros(9,7); stdT2EC = zeros(9,7); 
meanRes = zeros(9,7); stdRes = zeros(9,7); 

for ratNo = 1:9
    if ratNo == 1 || ratNo == 8 || ratNo == 3
        continue
    end
    
    mS0ICRat = squeeze(mS0IC(ratNo, 1:T, :)); % lines = T, columns = pldtau combs
    mS0ECRat = squeeze(mS0EC(ratNo, 1:T, :)); % lines = T, columns = pldtau combs
    if strcmp(T2Constant, 'off')
        mT2ICRat = squeeze(mT2IC(ratNo, 1:T, :)); 
        mT2ECRat = squeeze(mT2EC(ratNo, 1:T, :)); 
    end
    mResnormRat = squeeze(RESNORMCounter(ratNo, 1:T, :)); 
    
    for pldtau = 1:7
    
        meanS0IC(ratNo, pldtau) = mean(mS0ICRat(:, pldtau));
        stdS0IC( ratNo, pldtau) =  std(mS0ICRat(:, pldtau));

        meanS0EC(ratNo, pldtau) = mean(mS0ECRat(:, pldtau));
        stdS0EC( ratNo, pldtau) =  std(mS0ECRat(:, pldtau));
        
        if strcmp(T2Constant, 'off')
            meanT2IC(ratNo, pldtau) = mean(mT2ICRat(:, pldtau));
            stdT2IC( ratNo, pldtau) =  std(mT2ICRat(:, pldtau));
            
            meanT2EC(ratNo, pldtau) = mean(mT2ECRat(:, pldtau));
            stdT2EC( ratNo, pldtau) =  std(mT2ECRat(:, pldtau));
        end
        
        meanRes(ratNo, pldtau) = mean(mResnormRat(:, pldtau));
        stdRes( ratNo, pldtau) =  std(mResnormRat(:, pldtau));

        fprintf(['RAT %d, pld = %d, tau = %d, \n',...
            '---->meanS0IC = %.3f, stdS0IC = %.3f, meanS0EC = %.3f, stdS0EC = %.3f'], ...
            ratNo, durations(pldtau).pld, durations(pldtau).tag, ...
            meanS0IC(ratNo, pldtau), stdS0IC(ratNo, pldtau), ...
            meanS0EC(ratNo, pldtau), stdS0EC(ratNo, pldtau));
        if strcmp(T2Constant, 'off')
            fprintf('\n---->meanT2IC = %.3f, stdT2IC = %.3f, meanT2EC = %.3f, stdT2EC = %.3f',...
                meanT2IC(ratNo, pldtau), stdT2IC(ratNo, pldtau), ...
                meanT2EC(ratNo, pldtau), stdT2EC(ratNo, pldtau));
        end
        fprintf('\n\n');

    end
    
    fprintf('\n\n\n');
    
    % Plot error bars        
%     errorbar(1:7, meanT2IC(ratNo,  :), stdT2IC(ratNo, :)); % T2ic
%     errorbar(1:7, meanT2EC(ratNo,  :), stdT2EC(ratNo, :)); % T2ec
%     errorbar(1:7, meanS0IC(ratNo,  :), stdS0IC(ratNo, :)); % S0ic
%     errorbar(1:7, meanS0EC(ratNo,  :), stdS0EC(ratNo, :)); % S0ec
%     errorbar(1:7, meanRes(ratNo,  :), stdRes(ratNo, :)); % Resnorm

%     plot(1:7, meanS0IC(ratNo,  :)./meanS0EC(ratNo,  :));

%     boxplot();

%     plot(1:7, squeeze(meanS0IC(ratNo, :)./meanS0EC(ratNo, :)));

    hold on
    pause(0.5)
    
end

% save('meanResBiexp', 'meanRes')


%%
figure
boxplot(meanS0EC(:, :))

plotThing = 'S0EC';

xlabel('PLD/TAU combination'); ylabel(plotThing);
ax = gca;
ax.XTick = (1:7);
ax.XTickLabel = {[num2str(durations(1).pld), '/', num2str(durations(1).tag)],...
                 [num2str(durations(2).pld), '/', num2str(durations(2).tag)],...
                 [num2str(durations(3).pld), '/', num2str(durations(3).tag)],...
                 [num2str(durations(4).pld), '/', num2str(durations(4).tag)],...
                 [num2str(durations(5).pld), '/', num2str(durations(5).tag)],...
                 [num2str(durations(6).pld), '/', num2str(durations(6).tag)],...
                 [num2str(durations(7).pld), '/', num2str(durations(7).tag)]};
ax.XTickLabelRotation = 45;
title(sprintf(['Bi-exponential Fitting of the Transverse Decay\n',...
               '%s for all PLD/TAU combinations'], plotThing));

legend('Rat 1', 'Rat 2', 'Rat 3', 'Rat 4', 'Rat 5', ...
    'Rat 6', 'Rat 7', 'Rat 8', 'Rat 9', 'Location', 'northwest');

%return
% saveas(gcf,sprintf('../fig/CQ4_Bi%s_%sAvg.png', fittype, plotThing));


%% Plot the estimated delta Mic / delta Mec as a function of pld/tau

% Plot delta Mic / delta Mec as a function of pld/tau
figure
indices = [2 4 5 6 7 9];
errorbar(1:7, mean(squeeze(meanS0IC(indices, :)./meanS0EC(indices, :))), std(squeeze(meanS0IC(indices, :)./meanS0EC(indices, :))));

meanICEC = mean(squeeze(meanS0IC(indices, :)./meanS0EC(indices, :)));
stdICEC  = std(squeeze(meanS0IC(indices, :)./meanS0EC(indices, :)));
%save('CQ4icec', 'meanICEC', 'stdICEC');
save('CQ4icecctrl', 'meanICEC', 'stdICEC');

xlabel('PLD/TAU combination'); ylabel('S0IC/S0EC (arbitrary units)');
ax = gca;
ax.XTick = (1:7);
ax.XTickLabel = {[num2str(durations(1).pld), '/', num2str(durations(1).tag)],...
                 [num2str(durations(2).pld), '/', num2str(durations(2).tag)],...
                 [num2str(durations(3).pld), '/', num2str(durations(3).tag)],...
                 [num2str(durations(4).pld), '/', num2str(durations(4).tag)],...
                 [num2str(durations(5).pld), '/', num2str(durations(5).tag)],...
                 [num2str(durations(6).pld), '/', num2str(durations(6).tag)],...
                 [num2str(durations(7).pld), '/', num2str(durations(7).tag)]};
ax.XTickLabelRotation = 45;
title(sprintf(['Bi-exponential Fitting of the Transverse Decay\n',...
               'S0IC/S0EC for all PLD/TAU combinations, averaged over all 9 subjects']));


           
%saveas(gcf,sprintf('../fig/CQ4_BiT2set%s_S0ICS0EC_mean.png', fittype));





return
%%
% % For each pldtau combination you can print out the histogram
% for pldtau = 1:7
%     figure
%     subplot(1,2,1)
%     histfit(mS0(ratNo,1:T,pldtau));
%     title(sprintf('Histogram for S0 for PLD = %d TAU = %d', durations(pldtau).pld, durations(pldtau).tag));
%     
%     subplot(1,2,2)
%     histfit(mT2(ratNo,1:T,pldtau));
%     title(sprintf('Histogram for T2 for PLD = %d TAU = %d', durations(pldtau).pld, durations(pldtau).tag));
% end















