function [durations echoTimes] = params()

    % Durations
    durations = repmat(struct, [7,1]);
    durations(1).tag = 500;
    durations(1).pld = 50;
    durations(2).tag = 750;
    durations(2).pld = 50;
    durations(3).tag = 1000;
    durations(3).pld = 50;
    durations(4).tag = 3000;
    durations(4).pld = 50;
    durations(5).tag = 3000;
    durations(5).pld = 300;
    durations(6).tag = 3000;
    durations(6).pld = 700;
    durations(7).tag = 3000;
    durations(7).pld = 1200;
    
    % Echo times
    echoTimes = [29 41 53 68.2 80.2 92.2 107.4 119.4 131.4 146.6 158.6 170.6];
    
end