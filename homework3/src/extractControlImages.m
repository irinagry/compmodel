function ctrlImages = extractControlImages(images, noSubj, noEcho, noTagPost)

    % The control images
    ctrlImages = repmat(struct, [9,1]);

    for i = 1:noSubj
        
        for j = 1:noEcho

            for k = 2:2:noTagPost
                
                % The gradient on images
                ctrlImages(i).on(:,:,j,1,k/2) = squeeze(images(i).on(:,:,j,1,k));

                % The gradient off images
                ctrlImages(i).off(:,:,j,1,k/2) = squeeze(images(i).off(:,:,j,1,k));
                
            end
            
        end
        
    end

end
