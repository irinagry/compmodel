%% ProjectCQ3map
% Mono-exponential fitting of the transverse decay (signal vs echo time)

% Load Data
[images ...
    durations echoTimes noSubj ...
    sizeImages noEcho noTagPost] = loadData();

% Extract the perfusion weighted and control images
% Perfusion Weighted
perfImages = extractPerfusionWeightedImages(...
    images, noSubj, noEcho, noTagPost);

% Control Images
ctrlImages = extractControlImages(...
    images, noSubj, noEcho, noTagPost);

% Taking the mean signal for a cortical ROI
rois = createROIs(perfImages, 4, 'no');

size(rois)

meanCorticalASL.on    = zeros(noSubj, noEcho, noTagPost/2); % perfusion weighted
meanCorticalASL.off   = zeros(noSubj, noEcho, noTagPost/2);
meanControlSignal.on  = zeros(noSubj, noEcho, noTagPost/2); % controls
meanControlSignal.off = zeros(noSubj, noEcho, noTagPost/2);

scalingFactorASL.on = zeros(9,1);
scalingFactorASL.off = zeros(9,1);
scalingFactorControl.on = zeros(9,1);
scalingFactorControl.off = zeros(9,1);

for i = 1:noSubj
    
    % Calculate current roi and its number of pixels
    roiCurrent = rois(1:64, ((i-1)*64+1):(i*64));
    noPixels = sum(roiCurrent(roiCurrent==1));
    
    for j = 1:noEcho
       
        for k = 1:noTagPost/2
            
            % Perfusion Weighted
            imgTmpOn = squeeze(perfImages(i).on(:,:,j,1,k));
            meanCorticalASL.on(i,j,k) = sum(imgTmpOn(roiCurrent)) / noPixels;
        
            imgTmpOff = squeeze(perfImages(i).off(:,:,j,1,k));
            meanCorticalASL.off(i,j,k) = sum(imgTmpOff(roiCurrent)) / noPixels;
            
            % Control images
            imgTmpControlOn = squeeze(ctrlImages(i).on(:,:,j,1,k));
            meanControlSignal.on(i,j,k) = sum(imgTmpControlOn(roiCurrent)) / noPixels;
            
            imgTmpControlOff = squeeze(ctrlImages(i).off(:,:,j,1,k));
            meanControlSignal.off(i,j,k) = sum(imgTmpControlOff(roiCurrent)) / noPixels;
            
        end
        
    end
    
    scalingFactorASL.on(i) = meanCorticalASL.on(i,1,1); % max(max(meanCorticalASL.on(i,:,:)));
    scalingFactorASL.off(i) = meanCorticalASL.off(i,1,1);
    scalingFactorControl.on(i) = meanControlSignal.on(i,1,1);
    scalingFactorControl.off(i) = meanControlSignal.off(i,1,1);
    
    % Scaling
    for k = 1:noTagPost/2
        meanCorticalASL.on(i,:,k) = meanCorticalASL.on(i,:,k)./scalingFactorASL.on(i);
        meanCorticalASL.off(i,:,k) = meanCorticalASL.off(i,:,k)./scalingFactorASL.off(i);
        meanControlSignal.on(i,:,k) = meanControlSignal.on(i,:,k)./scalingFactorControl.on(i);
        meanControlSignal.off(i,:,k) = meanControlSignal.off(i,:,k)./scalingFactorControl.off(i);
    end
    
end


fittype = 'ASL';
grads = 'off'; % and then off
caseVal = strcat(fittype,grads);

N = 5;
ratNo = 4; % just for a single subject
for i = 4:4 
    figure, imagesc(squeeze(perfImages(i).on(10:35,15:45,1,1,4)))
end
XMAX = 25; %10:35
YMAX = 30; %15:45

% Maps of:
mS0 = zeros(XMAX,YMAX);
mT2 = zeros(XMAX,YMAX);
mResnorm = zeros(XMAX,YMAX);

RESNORMCounterMap = zeros(N,XMAX,YMAX);

switch fittype
    case 'ASL'
        % Initial Params
        S0ini =   2.5 + 5.*randn(N,1);
        T2ini =  90 +  20.*randn(N,1);
        lb = [  -8,   0];
        ub = [   8, 500];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
    case 'Control'
        % Initial Params
        S0ini =  50 +  50.*randn(N,1);
        T2ini =  90 +  20.*randn(N,1);
        lb = [   0,   0];
        ub = [ 100, 500];
        A = [];
        b = [];
        Aeq = [];
        beq = [];
end


% Define multiple starting points
initialPoints = [S0ini T2ini];
finalPoints   = zeros(9,7,2);


runagain = 'yes';

gradOffT2 = [];
%gradOnT2  = [];
for pldtau = 1:7 % out of 7 combinations
    
    % Maps of:
    mS0 = zeros(XMAX,YMAX);
    mT2 = zeros(XMAX,YMAX);
    mResnorm = zeros(XMAX,YMAX);

    RESNORMCounterMap = zeros(N,XMAX,YMAX);
    
    if strcmp(runagain, 'yes')
        tic
        for x = 1:XMAX
            for y = 1:YMAX

                switch caseVal
                    case 'ASLon'
                        Avox = prepareAvoxForVoxel(squeeze(perfImages(ratNo).on(x+9,y+14,:,1,pldtau)));
                    case 'ASLoff'
                        Avox = prepareAvoxForVoxel(squeeze(perfImages(ratNo).off(x+9,y+14,:,1,pldtau)));
                    case 'Controlon'
                        Avox = prepareAvoxForVoxel(squeeze(ctrlImages(ratNo).on(x+9,y+14,:,1,pldtau)));
                    case 'Controloff'
                        Avox = prepareAvoxForVoxel(squeeze(ctrlImages(ratNo).off(x+9,y+14,:,1,pldtau)));
                end

                evalFunc = @(x) monoExponentialSSD(x, Avox', echoTimes);            

                finalPoints = zeros(1,2);
                RESNORMMIN = 1e+30; % high value

                for i = 1:N
                    % Define various options for the non-linear fitting algorithm
                    h = optimset('MaxFunEvals', 20000, 'Algorithm', 'interior-point',...
                             'TolX', 1e-10, 'TolFun', 1e-10, 'Display', 'off');

                    % Run the fitting
                    try
                        % Define a starting point for the non-linear fit
                        startx = initialPoints(i,:);

                        % Run the fitting
                        [parameter_hat, RESNORM, EXITFLAG, OUTPUT, LAMBDA, GRAD, HESSIAN] = ...
                            fmincon(evalFunc, startx, A, b, Aeq, beq, lb, ub, [], h);
                        RESNORMCounterMap(i,x,y) = RESNORM;

                    catch ME
                        warning(ME.message);
                        disp(ME.message);

                        RESNORM = 1e+30; % high value
                        RESNORMMIN = 1e+30;
                        RESNORMCounterMap(i,x,y) = RESNORM;
                    end

                    if RESNORM < RESNORMMIN

                        RESNORMMIN = RESNORM;
                        finalPoints = parameter_hat;

                        mS0(x,y) = parameter_hat(1);
                        mT2(x,y) = parameter_hat(2);
                        mResnorm(x,y) = RESNORM;

                    end
                end

            end
            x
            pldtau
        end
        elapsedTime = toc
    end

    % Save map of T2
    if strcmp(grads, 'off') == 1
        gradOffT2 = [gradOffT2 mT2];
    else
        gradOnT2 = [gradOnT2 mT2];
    end
    
    
    pldtaucomb = sprintf('%s/%s', num2str(durations(pldtau).pld), num2str(durations(pldtau).tag));
    S0title = @(gradT) sprintf('S0 map Grad %s, PLD/TAU %s', gradT, pldtaucomb);
    T2title = @(gradT) sprintf('T2 map Grad %s, PLD/TAU %s', gradT, pldtaucomb);
    Restitle = @(gradT) sprintf('RESNORM map Grad %s, PLD/TAU %s', gradT, pldtaucomb);

    savefile = sprintf('Q53SpatialMap_Grad%s_Monoexp_pldtau%d', grads, pldtau);
%     save(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');
    load(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');

    figure, imagesc(mS0), title(S0title(grads)), colorbar, colormap hot
    saveas(gcf, ['../fig/maps/', savefile, 'S0.png']);
    T2mapCorrected = mT2; T2mapCorrected(T2mapCorrected>350) = 0;
    figure, imagesc(T2mapCorrected), title(T2title(grads)), colorbar, colormap hot
    saveas(gcf, ['../fig/maps/', savefile, 'T2.png']);
    % figure, imagesc(mResnorm), title(Restitle('On')), colorbar, colormap hot
    figure, imagesc(mResnorm./(max(max(mResnorm)))), title(Restitle(grads)), colorbar, colormap hot
    saveas(gcf, ['../fig/maps/', savefile, 'RESNORM.png']);

end

if strcmp(grads, 'off') == 1
    figure, 
    gradOffT2t = gradOffT2; gradOffT2t(gradOffT2t > 150) = 0;
    imagesc(gradOffT2t), colormap jet
    ax = gca;
    ax.XTick = [1:2:14] .* 30/2;
    ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
    xlabel('PLD ms / \tau ms');
    title('T2 Spatial Maps (grad off)');
    saveas(gcf, ['../fig/maps/', 'T2GradOff.png']);
else
    figure, 
    gradOnT2t = gradOnT2; gradOnT2t(gradOnT2t > 150) = 0;
    imagesc(gradOnT2t), colormap jet
    ax = gca;
    ax.XTick = [1:2:14] .* 30/2;
    ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
    xlabel('PLD ms / \tau ms');
    title('T2 Spatial Maps (grad on)');
    saveas(gcf, ['../fig/maps/', 'T2GradOn.png']);
end

% Put them together
% savefile = sprintf('Q53SpatialMapOffpldtau%s', pldtau);
% load(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');

figure,

subplot(2,1,1)
imagesc(gradOnT2t), colormap jet
ax = gca;
ax.XTick = [1:2:14] .* 30/2;
ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
xlabel('PLD ms / \tau ms');
title('T2 Spatial Maps (grad on)');

subplot(2,1,2)
imagesc(gradOffT2t), colormap jet
ax = gca;
ax.XTick = [1:2:14] .* 30/2;
ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
xlabel('PLD ms / \tau ms');
title('T2 Spatial Maps (grad off)');

% save('cq3maps', 'gradOnT2', 'gradOffT2', 'gradOnT2t', 'gradOffT2t');

figure
coalesced = [gradOnT2t ; gradOffT2t];
imagesc(coalesced), colormap jet
ax = gca;
ax.YTick = [15, 40];
ax.YTickLabel = {'VCG On', 'VCG Off'};
ax.XTick = [1:2:14] .* 30/2;
ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
xlabel('PLD ms / \tau ms');
title('T2 Spatial Maps');

% saveas(gcf, ['../fig/maps/', 'T2GradOnOff.png']);

% savefile = sprintf('Q53SpatialMapOffpldtau%s', pldtau);
% save(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');
% % % load(savefile, 'mS0', 'mT2', 'mResnorm', 'elapsedTime');
% 
% figure, imagesc(mS0), title(S0title('Off')), colorbar, colormap hot
% T2mapCorrected = mT2; T2mapCorrected(T2mapCorrected>200) = 0;
% figure, imagesc(T2mapCorrected), title(T2title('Off')), colorbar, colormap hot
% figure, imagesc(mResnorm), title(Restitle('Off')), colorbar, colormap hot



return
% Don't run this 

gradOffT2 = [];
gradOnT2  = [];

for pldtau = 1:7

    grads = 'off';
    savedfile = sprintf('Q53SpatialMap_Grad%s_Monoexp_pldtau%d', grads, pldtau);
    mT2Off = load(savefile, 'mT2');
    mean(mean(mT2Off.mT2))
    gradOffT2 = [gradOffT2 mT2Off.mT2];
    
    grads = 'on';
    savedfile = sprintf('Q53SpatialMap_Grad%s_Monoexp_pldtau%d', grads, pldtau);
    mT2On = load(savefile, 'mT2');
    mean(mean(mT2On.mT2))
    gradOnT2 = [gradOnT2 mT2On.mT2];
    
end

figure, 
gradOffT2t = gradOffT2; gradOffT2t(gradOffT2t > 150) = 0;
imagesc(gradOffT2t), colormap jet
ax = gca;
ax.XTick = [1:2:14] .* 30/2;
ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
xlabel('PLD ms / \tau ms');
% saveas(gcf, ['../fig/maps/', 'T2GradOff.png']);

figure, 
gradOnT2t = gradOnT2; gradOnT2t(gradOnT2t > 150) = 0;
imagesc(gradOnT2t), colormap jet
ax = gca;
ax.XTick = [1:2:14] .* 30/2;
ax.XTickLabel = {'50/500','50/750','50/1000','50/3000','300/3000','700/3000','1200/3000'};
xlabel('PLD ms / \tau ms');
% saveas(gcf, ['../fig/maps/', 'T2GradOn.png']);






