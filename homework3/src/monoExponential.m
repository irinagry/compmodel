function [S] = monoExponential(x, TE)
  
% Extract parameters
S0 = x(1);
T2 = x(2);

S = S0 .* exp(-TE./T2);

end