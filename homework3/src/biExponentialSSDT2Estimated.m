function [sumRes] = biExponentialSSDT2Estimated(x, Avox, TE)
  
% Extract parameters
S0IC = x(1);
S0EC = x(2);
T2IC = x(3);
T2EC = x(4);

S = S0IC .* exp(-TE./T2IC) + S0EC .* exp(-TE./T2EC);

% Compute the sum of the square differences
sumRes = sum((Avox - S).^2);

end